<?php
/**
 * The template for displaying archive Testimonoial pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 *
 */

get_header();
?>
<style>
.dmformsubmit { float:left !important;}
.dmform-error {
    color: #f00909b0 !important;;
    float: left !important;;
}
</style>
<!-- Google Fonts Include -->
<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic|Josefin+Sans:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic|Oswald:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic|Open+Sans:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic&amp;subset=latin-ext&amp;display=swap"  />
<!-- RT CSS Include d-css-runtime-desktop-one-package-new-->
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri()?>/mnlt/production/1917/_dm/s/rt/dist/css/d-css-runtime-desktop-one-package-new.min.css" />
<!-- End of RT CSS Include -->
       <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/widget_css/production_1917/004b5bdd245110b6c6336267aa0e53b8.css" id="widgetCSS" />
       <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/51716a39/files/51716a39_1.min.css" id="siteGlobalCss" />
 <style id="pagestyle" type="text/css">
    *#dm *.dmBody div.u_1422486861{
background-color:rgba(0,0,0,0) !important}
*#dm *.dmBody section.u_1897767439{
background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-2880w.jpg) !important;
background-color:rgba(181,49,44,1) !important;
background-repeat:no-repeat !important;
background-size:cover !important}
*#dm *.dmBody *.u_1897767439:before{
opacity:0.5 !important;
background-color:rgb(255,255,255) !important}
*#dm *.dmBody *.u_1897767439.before{
opacity:0.5 !important;
background-color:rgb(255,255,255) !important}
*#dm *.dmBody *.u_1897767439>.bgExtraLayerOverlay{
opacity:0.5 !important;
background-color:rgb(255,255,255) !important}
*#dm *.dmBody section.u_1897767439:before{
background-color:rgba(181,49,44,1) !important;
opacity:0.92 !important}
*#dm *.dmBody section.u_1897767439.before{
background-color:rgba(181,49,44,1) !important;
opacity:0.92 !important}
*#dm *.dmBody section.u_1897767439>.bgExtraLayerOverlay{
background-color:rgba(181,49,44,1) !important;
opacity:0.92 !important}
*#dm *.dmBody div.u_1505275905 input:not([type="submit"]){
border-style:solid !important;
border-width:0 !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1505275905 textarea{
border-style:solid !important;
border-width:0 !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1505275905 select{
border-style:solid !important;
border-width:0 !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody *.u_1505275905 .dmformsubmit{
float:LEFT !important}
*#dm *.dmBody div.u_1505275905 .dmform-success{
color:rgba(102,102,102,1) !important}
*#dm *.dmBody div.dmform-error{
color:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1505275905 .dmform-success .rteBlock{
color:rgba(102,102,102,1) !important}
*#dm *.dmBody div.dmform-error .rteBlock{
color:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1505275905 .dmwidget-title{
color:rgba(102,102,102,1) !important}

*#dm *.dmBody div.u_1505275905 .m-recaptcha{
color:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1711146099 .svg{
width:50% !important;
color:rgba(102,102,102,1) !important;
fill:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1745519653{
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1786227030{
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1711146099{
border-radius:50% !important;
border-color:rgba(102,102,102,1) !important;
border-width:2px !important;
border-style:solid !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1930206344 .svg{
width:50% !important;
color:rgba(102,102,102,1) !important;
fill:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1930206344{
border-radius:50% !important;
border-color:rgba(102,102,102,1) !important;
border-width:2px !important;
border-style:solid !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1379604917{
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1647253524 .svg{
width:50% !important;
color:rgba(102,102,102,1) !important;
fill:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1647253524{
border-radius:50% !important;
border-color:rgba(102,102,102,1) !important;
border-width:2px !important;
border-style:solid !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1051238054 .svg{
width:50% !important;
color:rgba(102,102,102,1) !important;
fill:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1051238054{
border-radius:50% !important;
border-color:rgba(102,102,102,1) !important;
border-width:2px !important;
border-style:solid !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1317639325{
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1939218721 .svg{
width:50% !important;
color:rgba(102,102,102,1) !important;
fill:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1939218721{
border-radius:50% !important;
border-color:rgba(102,102,102,1) !important;
border-width:2px !important;
border-style:solid !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1544959150{
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1029858441 .svg{
width:50% !important;
color:rgba(102,102,102,1) !important;
fill:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1029858441{
border-radius:50% !important;
border-color:rgba(102,102,102,1) !important;
border-width:2px !important;
border-style:solid !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1332697789{
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1125238651 .svg{
width:50% !important;
color:rgba(102,102,102,1) !important;
fill:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1125238651{
border-radius:50% !important;
border-color:rgba(102,102,102,1) !important;
border-width:2px !important;
border-style:solid !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1406525810{
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1122957372 .svg{
width:50% !important;
color:rgba(102,102,102,1) !important;
fill:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1122957372{
border-radius:50% !important;
border-color:rgba(102,102,102,1) !important;
border-width:2px !important;
border-style:solid !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1301119704{
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1026842319 .svg{
width:50% !important;
color:rgba(102,102,102,1) !important;
fill:rgba(102,102,102,1) !important}
*#dm *.dmBody div.u_1026842319{
border-radius:50% !important;
border-color:rgba(102,102,102,1) !important;
border-width:2px !important;
border-style:solid !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1521080361{
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody div.u_1023368897{
color:rgba(0,0,0,1) !important}
</style>
<style id="pagestyleDevice" type="text/css">
*#dm *.dmBody section.u_1897767439{
background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-1920w.jpg) !important;
background-color:rgba(181,49,44,1) !important;
background-repeat:no-repeat !important;
background-size:cover !important}@media all and (min-width:1920px), all and (-webkit-min-device-pixel-ratio: 1.5), all and (min--moz-device-pixel-ratio: 1.5), all and (min-device-pixel-ratio: 1.5) {
*#dm *.dmBody section.u_1897767439{
background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-2880w.jpg) !important;
background-color:rgba(181,49,44,1) !important;
background-repeat:no-repeat !important;
background-size:cover !important}
}
*#dm *.dmBody h1.u_1453001980
{
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	float:none !important;
	margin-right:157.5px !important;
	margin-left:157.5px !important;
	top:0 !important;
	max-width:calc(100% - 157px) !important;
	left:0 !important;
	width:645px !important;
	margin-top:66px !important;
	position:relative !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	height:auto !important;
	display:block !important;
}
*#dm *.dmBody h3.u_1453001980
{
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:auto !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody section.u_1897767439
{
	padding-top:13.5px !important;
	padding-left:40px !important;
	padding-bottom:13.5px !important;
	float:none !important;
	top:0 !important;
	max-width:100% !important;
	left:0 !important;
	width:auto !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	background-attachment:initial !important;
	background-position:0 50% !important;
}
*#dm *.dmBody div.u_1505275905 .dmwidget-title
{
	font-size:18px !important;
}
*#dm *.dmBody nav.u_1179213709
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:12px !important;
	padding-left:15px !important;
	padding-bottom:12px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:-8px !important;
	justify-content:flex-end !important;
	align-items:stretch !important;
	margin-bottom:0 !important;
	padding-right:15px !important;
	min-width:25px !important;
	text-align:start !important;
}
*#dm *.dmBody div.u_1745519653
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:20px !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1930206344
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1930206344
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1711146099
{
	width:50px !important;
	height:50px !important;
	padding-top:0 !important;
	margin-bottom:10px !important;
	padding-bottom:0 !important;
}
*#dm *.dmBody div.u_1711146099
{
	width:50px !important;
	height:50px !important;
	padding-top:0 !important;
	margin-bottom:10px !important;
	padding-bottom:0 !important;
}
*#dm *.dmBody div.u_1786227030
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:25px !important;
	text-align:left !important;
	margin-top:8px !important;
}
*#dm *.dmBody div.u_1786227030
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:25px !important;
	text-align:left !important;
	margin-top:8px !important;
}
*#dm *.dmBody div.u_1647253524
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1647253524
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1379604917
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1379604917
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1051238054
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1051238054
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1317639325
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1317639325
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1939218721
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1939218721
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1544959150
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1544959150
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1029858441
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1029858441
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1332697789
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1332697789
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1125238651
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1125238651
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1406525810
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1406525810
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1122957372
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1122957372
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1301119704
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1301119704
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1026842319
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1026842319
{
	width:50px !important;
	height:50px !important;
}
*#dm *.dmBody div.u_1521080361
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1521080361
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:705.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:20px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:20px !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1422486861
{
	padding-top:36px !important;
	padding-left:40px !important;
	padding-bottom:36px !important;
	float:none !important;
	top:0 !important;
	max-width:100% !important;
	left:0 !important;
	width:auto !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1023368897
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:207px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1023368897
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:207px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1330770612
{
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:166px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0px !important;
	padding-bottom:2px !important;
	margin-right:auto !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0px !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody div.u_1505275905
{
	padding-top:0px !important;
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-left:0px !important;
	padding-bottom:0px !important;
	margin-right:0px !important;
	margin-left:0px !important;
	max-width:100% !important;
	margin-top:0px !important;
	margin-bottom:0px !important;
	padding-right:0px !important;
	min-width:25px !important;
	text-align:center !important;
}
.u_1084594921.dmRespCol , .u_1371840841.dmRespCol{
    padding-top: 36.5px !important;
    padding-left: 40px !important;
    padding-bottom: 36.5px !important;
    position: relative !important;
    padding-right: 40px !important;
}
</style>
<link rel="preload" as="style" importance="low" onload="loadCSS(this)" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/css/media.css' ?>" />
 <div id="desktopBodyBox"> <div id="iscrollBody"> <div id="site_content"> <div class="dmRespRow dmRespRowStable dmRespRowNoPadding dmPageTitleRow dmInnerBarRow"> <div class="dmRespColsWrapper"> <div class="large-12 dmRespCol"> <div id="innerBar" class="innerBar lineInnerBar dmDisplay_None"> <div class="titleLine display_None"><hr/></div> 
<!-- Page title is hidden in css for new responsive sites. It is left here only so we don't break old sites. Don't copy it to new layouts --> <div id="pageTitleText"> <div class="innerPageTitle"><?php the_title(); ?></div> 
</div> 
 <div class="titleLine display_None"><hr/></div> 
</div> 
</div> 
</div> 
</div> 
<div dmwrapped="true" id="1275460582" class="dmBody u_dmStyle_template_testimonials">
   <div id="allWrapper" class="allWrapper">
      <!-- navigation placeholders --> 
      <div id="dm_content" class="dmContent">
         <div dm:templateorder="112" class="dmDefaultRespTmpl mainBorder" id="1479330477">
            <div class="innerPageTmplBox dmDefaultPage dmAboutListPage dmRespRowsWrapper" id="1736103396">
               <section class="u_1897767439 dmRespRow dmHomeSection1 dmHomeSectionNoBtn dmSectionNoParallax hasBackgroundOverlay" id="1897767439">
                  <div class="dmRespColsWrapper" id="1010790795">
                     <div class="dmRespCol large-12 medium-12 small-12" id="1139052171">
                        <h3 class="u_1453001980 dmNewParagraph" id="1453001980" data-element-type="paragraph" data-uialign="left" style="top: 0px; padding: 2px 0px; text-align: center!important; display: block; margin: 8px auto 8px 0px; float: none; width: 960px; height: auto;"><font color="#ffffff"><b>TESTIMONIALS</b></font></h3>
                     </div>
                  </div>
               </section>
               <div class="u_1422486861 dmRespRow" style="text-align: center;" id="1422486861">
                  <div class="dmRespColsWrapper" id="1640410774">
                     <div class="u_1084594921 dmRespCol small-12 large-9 medium-9 p-0-mobile" id="1084594921">
                     	<?php $i=0;
		                      $args = array(  
		                          'post_type' => 'testimonials',
		                          'post_status' => 'publish',
		                          'posts_per_page' => -1, 
		                          'orderby' => 'date', 
		                          'order' => 'ASC', 
		                      );

		                      $loop = new WP_Query( $args ); 
		                          
		                      while ( $loop->have_posts() ) : $loop->the_post(); 
		                     ?>
          	              <div class="graphicWidget u_1711146099" editablewidget="true" data-element-type="graphic" data-widget-type="graphic" id="1711146099" data-layout="graphic-style-2" title="Quotation mark">
                           <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 1664 1792" id="1304174606" class="svg u_1304174606" data-icon-name="fa-quote-right" alt="Quotation mark">
                              <path fill="inherit" d="M768 320v704q0 104-40.5 198.5t-109.5 163.5-163.5 109.5-198.5 40.5h-64q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h64q106 0 181-75t75-181v-32q0-40-28-68t-68-28h-224q-80 0-136-56t-56-136v-384q0-80 56-136t136-56h384q80 0 136 56t56 136zM1664 320v704q0 104-40.5 198.5t-109.5 163.5-163.5 109.5-198.5 40.5h-64q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h64q106 0 181-75t75-181v-32q0-40-28-68t-68-28h-224q-80 0-136-56t-56-136v-384q0-80 56-136t136-56h384q80 0 136 56t56 136z"></path>
                           </svg>
                        </div>
                        <div class="dmNewParagraph u_1745519653" data-dmtmpl="true" data-element-type="paragraph" id="1745519653" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px 0px; float: none; width: 705.609px; height: auto; position: relative;" data-uialign="left" data-editor-state="closed">
                           <div><font style="color: rgb(0, 0, 0);"><br/></font></div>
                           <font style="color: rgb(0, 0, 0);"><?php the_content(); ?></font> 
                           <div><font style="color: rgb(0, 0, 0);">&nbsp;</font></div>
                        </div>
                        <?php endwhile; ?>
						<?php get_template_part('partials/contact/contact'); ?>
         </div>
         <div class="u_1371840841 dmRespCol small-12 large-3 medium-3 p-0-mobile" id="1371840841">
            <div class="u_1330770612 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1330770612" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px 0px; float: none; width: 166px; height: auto; position: relative;" data-uialign="right"><font style="color: rgb(102, 102, 102);" class="font-size-18 lh-1">Contact Us Today!</font></div>
            <div class="u_1023368897 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1023368897" data-uialign="left" data-styletopreserve="{" background-image":""}"="" data-editor-state="closed"> 
            <div><span style="" class="lh-1 font-size-16"><font style=""><span style=""><?php  echo get_theme_mod('contact_details');?></span></font> 
               </span> 
            </div>
            <div>
               <div>
                  <span style="" class="lh-1 font-size-16"><font style="color: rgb(181, 49, 44);"><span style=""><br/></span></font> 
                  </span> 
                  <div><font style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><a href="tel:<?php  echo get_theme_mod('contact_phone_no');?>" runtime_url="tel:(561) 731-3155" style="color: rgb(181, 49, 44);" class="lh-1 font-size-16"><?php  echo get_theme_mod('contact_phone_no');?></a></font></div>
                  <div><span style="" class="font-size-14 lh-1"><br/></span></div>
                  <div><a href="mailto:<?php  echo get_theme_mod('contact_email');?>" runtime_url="mailto:admin@veritasnursecare.com" style="color: rgb(46, 176, 246);"><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_email');?></font></a></div>
                  <div><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><br/></font></div>
                  <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('office_hours');?></font></div>
                  <div><br></br></div>
                  <div><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_address1');?></span></div>
                  <div>
                     <span style="" class="font-size-14 lh-1">
                        <span style="color: rgb(0, 0, 0);">
                        </span>
                        <span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_address2');?></span><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><br/></span>
                     </span>
                  </div>
                  <div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div> 
</div> 
</div> 
</div> 
</div> 
<?php
get_footer();
?>
<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
<script src="<?php echo get_stylesheet_directory_uri()?>/js/jquery.validate.js"></script>  
<script>
	$(document).ready(function(){
		$("#commentForm").validate();
		$('body').on('submit','#commentForm',function(e){
			var base_url = "<?php echo home_url();?>";
			var formData= $('#commentForm').serialize();
			console.log(formData);
			e.preventDefault();
			 jQuery.ajax({ 
				type:"post",
				dataType : 'json',
				url: base_url+"/wp-admin/admin-ajax.php",
				data: formData,
				success:function(response){ 
					$(".spinner-border").hide(); 
					if(response.status==true){
						$(".dmform-error").hide();
						$(".dmform-success").show();
						$("#commentForm")[0].reset();
					}else if(response.status=='recaptcha_error' ){	
						$(".dmform-error").show();
						$(".dmform-error").html(response.msg);
					}else{
						$(".dmform-error").show();						
						$("#commentForm")[0].reset();
						$(".dmform-error").html(response.msg);
						grecaptcha.reset();
					}
					
				},
				beforeSend:function(response){
					$(".spinner-border").show();
				}						
			});
		});	
	});
</script> 