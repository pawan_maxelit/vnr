<?php
/**
 * The template for displaying single services pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 *
 */

get_header();
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri()?>/mnlt/production/1903/_dm/s/rt/dist/css/d-css-runtime-desktop-one-package-new.min.css" />
<!-- End of RT CSS Include -->
       <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/widget_css/production_1903/004b5bdd245110b6c6336267aa0e53b8.css" id="widgetCSS" />
<!-- Support `img` size attributes -->
<style>img[width][height] {height: auto;}</style>
<!-- This is populated in Ajax navigation -->
<style id="pageAdditionalWidgetsCss" type="text/css">
</style>
<!-- Site CSS -->
<link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/51716a39/files/51716a39_1.min.css" id="siteGlobalCss" />
<style id="customWidgetStyle" type="text/css">

.dmformsubmit { float:left !important;}
.dmform-error {
    color: #f00909b0 !important;;
    float: left !important;;
}
    .widget-85b0db .text, .widget-85b0db .image{
    display:inline;
}
.widget-85b0db .image{
    max-width:100%;
    width:200px;
}
.widget-85b0db .left{
    clear:left;
    float:left;
    margin-right: 10px;
}
.widget-85b0db .right{
    clear:right;
    float:right;
    margin-left: 10px;
}
.widget-85b0db .spacer-left{
    float:left;
}
.widget-85b0db .spacer-right{
    float:right;
}
#dm .widget-85b0db .wrapper, #dm .widget-85b0db .rteBlock {
    text-align: justify;
}
#dm .widget-85b0db ul, #dm .widget-85b0db ol {
    padding: 0;
    list-style-position: inside;
}
</style>
<style id="innerPagesStyle" type="text/css">
</style>
<style id="additionalGlobalCss" type="text/css">
</style>
<style id="pagestyle" type="text/css">
    *#dm *.dmBody section.u_1897767439
{
	background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-2880w.jpg) !important;
	background-color:rgba(181,49,44,1) !important;
	background-repeat:no-repeat !important;
	background-size:cover !important;
}
*#dm *.dmBody *.u_1897767439:before
{
	opacity:0.5 !important;
	background-color:rgb(255,255,255) !important;
}
*#dm *.dmBody *.u_1897767439.before
{
	opacity:0.5 !important;
	background-color:rgb(255,255,255) !important;
}
*#dm *.dmBody *.u_1897767439>.bgExtraLayerOverlay
{
	opacity:0.5 !important;
	background-color:rgb(255,255,255) !important;
}
*#dm *.dmBody section.u_1897767439:before
{
	background-color:rgba(181,49,44,1) !important;
	opacity:0.92 !important;
}
*#dm *.dmBody section.u_1897767439.before
{
	background-color:rgba(181,49,44,1) !important;
	opacity:0.92 !important;
}
*#dm *.dmBody section.u_1897767439>.bgExtraLayerOverlay
{
	background-color:rgba(181,49,44,1) !important;
	opacity:0.92 !important;
}
*#dm *.dmBody div.dmform-error
{
	color:rgba(102,102,102,1) !important;
}
*#dm *.dmBody div.dmform-error .rteBlock
{
	color:rgba(102,102,102,1) !important;
}
div.u_1194927944
{
	background-color:rgba(0,0,0,0) !important;
}
div.u_1246820261 input:not([type="submit"])
{
	border-style:solid !important;
	border-width:0 !important;
	background-color:rgba(227,227,227,1) !important;
}
div.u_1246820261 textarea
{
	border-style:solid !important;
	border-width:0 !important;
	background-color:rgba(227,227,227,1) !important;
}
div.u_1246820261 select
{
	border-style:solid !important;
	border-width:0 !important;
	background-color:rgba(227,227,227,1) !important;
}
*#dm *.dmBody *.u_1246820261 .dmformsubmit
{
	float:LEFT !important;
}
div.u_1246820261 .dmform-success
{
	color:rgba(102,102,102,1) !important;
}
div.u_1246820261 .dmform-success .rteBlock
{
	color:rgba(102,102,102,1) !important;
}
div.u_1246820261 .dmwidget-title
{
	color:rgba(102,102,102,1) !important;
}
div.u_1246820261 .dmforminput label:not(.for-checkable):not(.custom-contact-checkable)
{
	color:rgba(102,102,102,1) !important;
}
div.u_1246820261 .m-recaptcha
{
	color:rgba(102,102,102,1) !important;
}
*#dm *.dmBody *.u_1143172575 .wrapper
{
	text-align:LEFT !important;
}
*#dm *.dmBody *.u_1143172575 .rteBlock
{
	text-align:LEFT !important;
}
*#dm *.dmBody div.u_1507611713
{
	color:rgba(0,0,0,1) !important;
}
*#dm *.dmBody div.u_1140670964
{
	border-style:solid !important;
	border-width:0 !important;
	background-color:rgba(255,255,255,1) !important;
	border-color:rgba(181,49,44,1) !important;
}
*#dm *.dmBody nav.u_1783616978
{
	color:black !important;
}
*#dm *.dmBody nav.u_1783616978.main-navigation.unifiednav .unifiednav__container:not([data-depth])>.unifiednav__item-wrap>.unifiednav__item
{
	color:rgba(181,49,44,1) !important;
}
*#dm *.dmBody *.u_1783616978.main-navigation.unifiednav:not([data-nav-structure='VERTICAL']) .unifiednav__container:not([data-depth])>.unifiednav__item-wrap:not(:last-child)::before
{
	font-size:12px !important;
}
*#dm *.dmBody *.u_1783616978.main-navigation.unifiednav:not([data-nav-structure='VERTICAL']) .unifiednav__container:not([data-depth])>.unifiednav__item-wrap:not(:last-child)::after
{
	font-size:12px !important;
}
</style>
<style id="pagestyleDevice" type="text/css">
    *#dm *.dmBody section.u_1897767439
{
	background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-1920w.jpg) !important;
	background-color:rgba(181,49,44,1) !important;
	background-repeat:no-repeat !important;
	background-size:cover !important;
}@media all and (min-width:1920px), all and (-webkit-min-device-pixel-ratio: 1.5), all and (min--moz-device-pixel-ratio: 1.5), all and (min-device-pixel-ratio: 1.5) {
*#dm *.dmBody section.u_1897767439
{
	background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-2880w.jpg) !important;
	background-color:rgba(181,49,44,1) !important;
	background-repeat:no-repeat !important;
	background-size:cover !important;
}
}
*#dm *.dmBody h1.u_1453001980
{
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	float:none !important;
	margin-right:157.5px !important;
	margin-left:157.5px !important;
	top:0 !important;
	max-width:calc(100% - 157px) !important;
	left:0 !important;
	width:645px !important;
	margin-top:66px !important;
	position:relative !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	height:auto !important;
	display:block !important;
}
*#dm *.dmBody section.u_1897767439
{
	padding-top:13.5px !important;
	padding-left:40px !important;
	padding-bottom:13.5px !important;
	float:none !important;
	top:0 !important;
	max-width:100% !important;
	left:0 !important;
	width:auto !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	background-attachment:initial !important;
	background-position:0 50% !important;
}
*#dm *.dmBody nav.u_1179213709
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:12px !important;
	padding-left:15px !important;
	padding-bottom:12px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:415px !important;
	margin-top:-8px !important;
	justify-content:flex-end !important;
	align-items:stretch !important;
	margin-bottom:0 !important;
	padding-right:15px !important;
	min-width:25px !important;
	text-align:start !important;
}
*#dm *.dmBody h3.u_1453001980
{
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
div.u_1194927944
{
	padding-top:36.5px !important;
	padding-left:40px !important;
	padding-bottom:36.5px !important;
	float:none !important;
	top:auto !important;
	max-width:none !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
}
div.u_1246820261 .dmwidget-title
{
	font-size:18px !important;
}
div.u_1246820261
{
	padding-top:0 !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:16px !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
}
div.u_1101791350
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 13px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:13px !important;
	max-width:166px !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
div.u_1143172575 .wrapper
{
	font-size:14px !important;
}
div.u_1143172575 .rteBlock
{
	font-size:14px !important;
}
div.u_1143172575 .image
{
	width:251px;
}
div.u_1194927944
{
	padding-top:36.5px !important;
	padding-left:40px !important;
	padding-bottom:36.5px !important;
	float:none !important;
	top:auto !important;
	max-width:none !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
}
div.u_1246820261 .dmwidget-title
{
	font-size:18px !important;
}
div.u_1246820261
{
	padding-top:0 !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:16px !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
}
div.u_1101791350
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 13px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:13px !important;
	max-width:166px !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
div.u_1143172575 .wrapper
{
	font-size:14px !important;
}
div.u_1143172575 .rteBlock
{
	font-size:14px !important;
}
div.u_1143172575 .image
{
	width:251px;
}
*#dm *.dmBody div.u_1044283377
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:303.422px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:0 !important;
}
*#dm *.dmBody div.u_1374036318
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:16px !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:25px !important;
}
*#dm *.dmBody div.u_1933771769
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	margin-right:auto !important;
	margin-left:auto !important;
	max-width:100% !important;
	margin-top:39px !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
}
*#dm *.dmBody div.u_1507611713
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:207px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1507611713
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:207px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1101791350
{
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:166px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:auto !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1246820261
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
}
*#dm *.dmBody div.u_1635326565
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:303.422px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:0 !important;
}
*#dm *.dmBody div.u_1558444602
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:303.422px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:0 !important;
}
*#dm *.dmBody div.u_1977169067
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:-15px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
}
*#dm *.dmBody div.u_1140670964
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:auto !important;
	position:relative !important;
	height:auto !important;
	padding-top:0 !important;
	padding-left:40px !important;
	padding-bottom:0 !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
}
*#dm *.dmBody nav.u_1783616978.main-navigation.unifiednav .unifiednav__container:not([data-depth])>.unifiednav__item-wrap>.unifiednav__item
{
	font-size:12px !important;
}
*#dm *.dmBody div.u_1154840544
{
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:225.609px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0px !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	padding-right:0px !important;
	min-width:0 !important;
}
*#dm *.dmBody nav.u_1783616978
{
	margin-left:0 !important;
	padding-top:0px !important;
	padding-left:0px !important;
	padding-bottom:0px !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	margin-right:0 !important;
	padding-right:0px !important;
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:225.609px !important;
	position:relative !important;
	height:auto !important;
	max-width:100% !important;
	justify-content:flex-start !important;
	align-items:stretch !important;
	min-width:0 !important;
	text-align:start !important;
}
</style>
<!--[if IE 7]><style>.fw-head.fw-logo img{max-width: 290px;}.dm_header .logo-div img{max-width: 290px;}</style><![endif]-->
<!--[if IE 8]><style>.fw-head .fw-logo img{max-width: 290px;}.dm_header .logo-div img{max-width: 290px;}*#dm div.dmHeader{_height:90px;min-height:0px;}</style><![endif]-->
    <style id="globalFontSizeStyle" type="text/css">
        .font-size-26, .size-26, .size-26 > font { font-size: 26px !important; }.font-size-36, .size-36, .size-36 > font { font-size: 36px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-13, .size-13, .size-13 > font { font-size: 13px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-13, .size-13, .size-13 > font { font-size: 13px !important; }
    </style>
	<link rel="preload" as="style" importance="low" onload="loadCSS(this)" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/css/media.css' ?>" />
	
 <div id="desktopBodyBox"> <div id="iscrollBody"> <div id="site_content"> <div class="dmRespRow dmRespRowStable dmRespRowNoPadding dmPageTitleRow dmInnerBarRow"> <div class="dmRespColsWrapper"> <div class="large-12 dmRespCol"> <div id="innerBar" class="innerBar lineInnerBar dmDisplay_None"> <div class="titleLine display_None"><hr/></div> 
<!-- Page title is hidden in css for new responsive sites. It is left here only so we don't break old sites. Don't copy it to new layouts --> <div id="pageTitleText"> <div class="innerPageTitle">Skilled Nursing & Therapy Services at Home</div> 
</div> 
 <div class="titleLine display_None"><hr/></div> 
</div> 
</div> 
</div> 
</div> 
 <div dmwrapped="true" id="1275460582" class="dmBody u_dmStyle_template_skilled-nursing">
   <div id="allWrapper" class="allWrapper">
      <!-- navigation placeholders --> 
      <div id="dm_content" class="dmContent">
         <div dm:templateorder="112" class="dmDefaultRespTmpl mainBorder" id="1479330477">
            <div class="innerPageTmplBox dmDefaultPage dmAboutListPage dmRespRowsWrapper" id="1736103396">
               <section class="u_1897767439 dmRespRow dmHomeSection1 dmHomeSectionNoBtn dmSectionNoParallax hasBackgroundOverlay" id="1897767439">
                  <div class="dmRespColsWrapper" id="1010790795">
                     <div class="dmRespCol large-12 medium-12 small-12" id="1139052171">
                        <h3 class="u_1453001980 dmNewParagraph" id="1453001980" data-element-type="paragraph" data-uialign="center" style="display: block; transition: none 0s ease 0s;">
                           <div style="text-align: center;"><font color="#ffffff"><b style="" class="font--36 lh-1"><?php echo get_the_title(); ?></b></font></div>
                        </h3>
                     </div>
                  </div>
               </section>
               <div class="u_1194927944 dmRespRow" style="text-align: center;" id="1194927944">
                  <div class="dmRespColsWrapper" id="1349345585">
                     <div class="u_1272477507 dmRespCol small-12 large-9 medium-9" id="1272477507">
                        <div class="widget-85b0db u_1143172575 dmCustomWidget" data-lazy-load="" id="1143172575" dmle_extension="custom_extension" data-element-type="custom_extension" icon="false" surround="false" data-widget-id="85b0db3860664a28b3031e6cd70abf8c" data-widget-version="37" data-widget-config="eyJkaXJlY3Rpb24iOiJyaWdodCIsImltYWdlIjoiaHR0cHM6Ly9pcnAtY2RuLm11bHRpc2NyZWVuc2l0ZS5jb20vNTE3MTZhMzkvZG1zM3JlcC9tdWx0aS9pbWFnZTIuanBnIiwidGV4dCI6IjxwIGNsYXNzPVwicnRlQmxvY2tcIj48c3Ryb25nPjxzcGFuIHN0eWxlPVwiY29sb3I6cmdiKDAsIDAsIDApXCI+WW91ciBQYXJlbnQgb3IgRWxkZXJseSBMb3ZlZCBPbmUgSGFzIEp1c3QgQmVlbiBEaXNjaGFyZ2VkIEZyb20gQSBIb3NwaXRhbCBvciBBIFJlaGFiaWxpdGF0aW9uIENlbnRlcjwvc3Bhbj48L3N0cm9uZz48L3A+PGJyPjxwIGNsYXNzPVwicnRlQmxvY2tcIj48c3BhbiBzdHlsZT1cImNvbG9yOnJnYigxMDIsIDEwMiwgMTAyKVwiPjxzcGFuIHN0eWxlPVwiY29sb3I6cmdiKDAsIDAsIDApXCI+UGF0aWVudHMgZGlzY2hhcmdlZCBmcm9tIGEgaG9zcGl0YWwgb3IgYSByZWhhYmlsaXRhdGlvbiBjZW50ZXIgd291bGQgZ2VuZXJhbGx5IHJlcXVpcmUgZm9sbG93LXVwIGNhcmUgYXQgaG9tZS4gwqBUaGUga2luZCBvZiBmb2xsb3ctdXAgY2FyZSBvcmRlcmVkIGJ5IHRoZSBwaHlzaWNpYW4gd2lsbCBkZXBlbmQgbGFyZ2VseSBvbiB0aGUgcmVhc29uIGZvciB0aGUgcGF0aWVudOKAmXMgaG9zcGl0YWwgYW5kL29yIHJlaGFiaWxpdGF0aW9uIGNvbmZpbmVtZW50LiDCoEZvciBleGFtcGxlLCBhIHBhdGllbnQgcmVjb3ZlcmluZyBmcm9tIGEgaGVhcnQgc3VyZ2VyeSBtYXkgcmVxdWlyZSBza2lsbGVkIG51cnNpbmcgY2FyZSBmb3IgdGhlIHN1cmdpY2FsIHdvdWxkLiDCoEFub3RoZXIgcGF0aWVudCByZWNvdmVyaW5nIGZyb20gYSBicm9rZW4gaGlwIG1heSByZXF1aXJlIHBoeXNpY2FsIHRoZXJhcHkuPC9zcGFuPjwvc3Bhbj48L3A+PGJyPjxwIGNsYXNzPVwicnRlQmxvY2tcIj48c3BhbiBzdHlsZT1cImNvbG9yOnJnYigxODEsIDQ5LCA0NClcIj5WZXJpdGFzPC9zcGFuPjxzcGFuIHN0eWxlPVwiY29sb3I6cmdiKDAsIDAsIDApXCI+wqBjYW4gcHJvdmlkZSB0aGUgU2tpbGxlZCBOdXJzZXMsIFBoeXNpY2FsIFRoZXJhcGlzdHMsIE9jY3VwYXRpb25hbCBUaGVyYXBpc3RzLCBTcGVlY2ggVGhlcmFwaXN0cyBhbmQgTWVkaWNhbCBTb2NpYWwgV29ya2VycyB0aGF0IHdpbGwgYmUgYWJsZSB0byBwcm92aWRlIHRoZSBzZXJ2aWNlcyB0aGF0IHdpbGwgYmUgcHJlc2NyaWJlZCBieSB0aGUgcGF0aWVudOKAmXMgcGh5c2ljaWFuLjwvc3Bhbj48L3A+PGJyPiIsImFsdCI6IkEgaG9tZSBjYXJlIHdvcmtlciBoZWxwaW5nIGFuIGVsZGVybHkgd29tYW4gd2l0aCBhIGNhbmUgZG93biB0aGUgc3RhaXJzLiJ9">
                           <div class="wrapper services-wrapper">
                              <div class="spacer spacer-right"></div>
                              <img alt="A home care worker helping an elderly woman with a cane down the stairs." class="image right" src="<?php echo get_the_post_thumbnail_url();?>" onerror="handleImageLoadError(this)"/> 
                              <div class="text"><?php echo get_the_content(); ?>
                              </div>
                              <div style="clear:both"></div>
                           </div>
                        </div>
                        <div class="u_1140670964 dmRespRow" id="1140670964">
                           <div class="dmRespColsWrapper" id="1263089187">
                              <div class="dmRespCol large-6 medium-6 small-12" id="1136160748">
                                 <div class="u_1044283377 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1044283377" style="" data-uialign="left" data-styletopreserve="{"background-image":""}"> 
                                 <h4><span style="display: initial; color: rgb(26, 26, 26);">Skilled Nursing</span></h4>
                              </div>
                              <div class="u_1374036318 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1374036318" style="transition: opacity 1s ease-in-out 0s;" data-uialign="left">
                                 <ul class="defaultList">
									<?php $rows = get_field('skilled_nursing'); //print_r($rows); 
									if( have_rows('skilled_nursing') ):

										// loop through the rows of data
										while ( have_rows('skilled_nursing') ) : the_row();
																		?>
                                    <li style="color: rgb(26, 26, 26);"><span style="color: rgb(26, 26, 26); display: initial;"><?php echo  the_sub_field('skilled_nursing'); ?> </span></li>
                                 <?php
										endwhile;
										endif; ?>
								</ul>
                                 <p><br/></p>
                              </div>
                           </div>
                           <div class="dmRespCol large-6 medium-6 small-12" id="1344161162">
                              <div class="u_1933771769 imageWidget align-center" editablewidget="true" data-element-type="image" data-widget-type="image" id="1933771769"> <a id="1376115995"><img src="<?php echo get_field('image'); ?>" alt="" id="1000048418" class="" data-dm-image-path=/></a> </div>
                           </div>
                        </div>
                     </div>
                     <div class="dmRespRow" id="1207860421">
                        <div class="dmRespColsWrapper" id="1875086501">
                           <div class="dmRespCol large-6 medium-6 small-12" id="1218542085">
                              <div class="u_1977169067 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1977169067" style="transition: opacity 1s ease-in-out 0s;" data-uialign="left">
                                 <p><span style="background-color: rgb(255, 255, 255); font-weight: bold; color: rgb(26, 26, 26); display: initial;">Physical Therapy</span><span style="background-color: rgb(26, 26, 26); font-weight: bold; color: rgb(26, 26, 26); display: initial;">﻿</span></p>
                                 <ul class="defaultList">
									<?php $rows = get_field('physical_therapy'); //print_r($rows); 
									if( have_rows('physical_therapy') ):
										while ( have_rows('physical_therapy') ) : the_row();
									?>
                                    <li style="color: rgb(26, 26, 26);"><span style="color: rgb(26, 26, 26); display: initial;"><?php echo  the_sub_field('physical_therapy'); ?></span></li>
									<?php
										endwhile;
										endif; ?>
                                 </ul>
                                 <p><span style="display: initial;"><span class="ql-cursor">﻿</span></span></p>
                              </div>
                              <div class="u_1635326565 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1635326565" style="transition: opacity 1s ease-in-out 0s;" data-uialign="right">
                                 <p><strong><font color="#1a1a1a">Occupational Therapy</font></strong></p>
                                 <ul class="defaultList">
									<?php $rows = get_field('occupational_therapy'); //print_r($rows); 
									if( have_rows('occupational_therapy') ):
										while ( have_rows('occupational_therapy') ) : the_row();
									?>
                                    <li style="color: rgb(26, 26, 26);"><span style="color: rgb(26, 26, 26); display: initial;"><?php echo  the_sub_field('occupational_therapy'); ?></span></li>
                                    
										<?php
										endwhile;
										endif; ?>
                                 </ul>
                                 <p><span style="display: initial;"><span class="ql-cursor">﻿</span></span></p>
                              </div>
                              <div class="dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1902650458" style="transition: opacity 1s ease-in-out 0s;" data-uialign="left">
                                 <p style="color: rgb(26, 26, 26);"><strong style="color: rgb(26, 26, 26); display: initial;">Speech Therapy</strong></p>
                                 <ul class="defaultList">
									<?php $rows = get_field('speech_therapy'); //print_r($rows); 
									if( have_rows('speech_therapy') ):
										while ( have_rows('speech_therapy') ) : the_row();
									?>
                                    <li style="color: rgb(26, 26, 26);"><span style="color: rgb(26, 26, 26); display: initial;"><?php echo  the_sub_field('speech_therapy'); ?></span></li>
                                    <?php
										endwhile;
										endif; ?>
                                 </ul>
                                 <p><br/></p>
                                 <p><br/></p>
                              </div>
                              <div class="u_1558444602 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1558444602" style="transition: opacity 1s ease-in-out 0s;" data-uialign="right">
                                 <p style="color: rgb(26, 26, 26);"><strong style="color: rgb(26, 26, 26); display: initial;">Medical Social Work</strong></p>
                                 <ul class="defaultList">
									<?php $rows = get_field('medical_social_work'); //print_r($rows); 
									if( have_rows('medical_social_work') ):
										while ( have_rows('medical_social_work') ) : the_row();
									?>
                                    <li style="color: rgb(26, 26, 26);"><span style="color: rgb(26, 26, 26); display: initial;"><?php echo  the_sub_field('medical_social_work'); ?></span></li>
                                    <?php
										endwhile;
										endif; ?>
                                 </ul>
                                 <p><span style="display: initial;"><span class="ql-cursor">﻿</span></span></p>
                              </div>
                           </div>
                           <div class="dmRespCol empty-column large-6 medium-6 small-12" id="1437645513"></div>
                        </div>
                     </div>
					  <?php get_template_part('partials/contact/contact'); ?>
					 </div>
					
                  <div class="u_1972778160 dmRespCol small-12 large-3 medium-3" id="1972778160">
                     <div class="u_1101791350 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1101791350" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px 0px; float: none; width: 166px; height: auto; position: relative;" data-uialign="right"><font style="color: rgb(102, 102, 102);" class="font-size-18 lh-1">Contact Us Today!</font></div>
                     <div class="u_1507611713 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1507611713" data-uialign="left" data-styletopreserve="{" background-image":""}"="" data-editor-state="closed"> 
                     <div><span style="" class="lh-1 font-size-16"><font style=""><span style=""><?php  echo get_theme_mod('contact_details');?></span></font> 
                        </span> 
                     </div>
                     <div>
                        <div>
                           <span style="" class="lh-1 font-size-16"><font style="color: rgb(181, 49, 44);"><span style=""><br/></span></font> 
                           </span> 
                           <div><font style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><a href="tel:<?php  echo get_theme_mod('contact_phone_no');?>" runtime_url="tel:(561) 731-3155" style="color: rgb(181, 49, 44);" class="lh-1 font-size-16"><?php  echo get_theme_mod('contact_phone_no');?></a></font></div>
                           <div><span style="" class="font-size-14 lh-1"><br/></span></div>
                           <div><a href="mailto:<?php  echo get_theme_mod('contact_email');?>" runtime_url="mailto:admin@veritasnursecare.com" style="color: rgb(46, 176, 246);"><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_email');?></font></a></div>
                           <div><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><br/></font></div>
                          <div><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><br/></font></div>
							  <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('office_hours');?></font></div>
							  <div><br></br></div>
                           <div><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_address1');?></span></div>
                           <div>
                              <span style="" class="font-size-14 lh-1">
                                 <span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_address2');?></span><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><br/></span>
                              </span>
                           </div>
                       </div>
                     </div>
                  </div>
                  <div class="u_1154840544 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1154840544" style="transition: none 0s ease 0s; text-align: left; display: block;" data-uialign="center">
                     <h4 class="m-size-13 size-18 m-text-align-center"><span class="font-size-18 m-font-size-13" style="color: rgb(181, 49, 44); display: initial;">Our Other Services</span></h4>
                  </div>
                  <nav class="u_1783616978 effect-bottom2 main-navigation unifiednav dmLinksMenu other_services" >
                     <ul class="unifiednav__container  " data-auto="navigation-pages">
						<?php 
							$args = array(  
										'post_type' 		=> 'services',
										'post_status' 		=> 'publish',
										'posts_per_page' 	=> 100, 
										'orderby' 			=> 'title', 
										'order' 			=> 'time'	
									);
									$service = new WP_Query($args);	
											
									if($service->have_posts()){
										while($service->have_posts()){
											$service->the_post();
											$post_id= $post->ID;
											
								?>

                        <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="<?php echo the_permalink($post->ID); ?>" class="unifiednav__item    " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                           Skilled Nursing & Therapy Services at Home
                           " data-auto="page-text-style"><?php echo get_the_title(); ?><span class="icon icon-angle-down"></span> 
                           </span> 
                           </a> 
                        </li>
						<?php }
								}
								?>
                     </ul>
                  </nav>
									
               </div>  
            </div>
         </div>
      </div>
   </div>
</div>
</div> 
</div>

<?php
get_footer();
?>
<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
<script src="<?php echo get_stylesheet_directory_uri()?>/js/jquery.validate.js"></script>  
<script>
	$(document).ready(function(){
		$("#commentForm").validate();
		$('body').on('submit','#commentForm',function(e){
			var base_url = "<?php echo home_url();?>";
			var formData= $('#commentForm').serialize();
			console.log(formData);
			e.preventDefault();
			 jQuery.ajax({ 
				type:"post",
				dataType : 'json',
				url: base_url+"/wp-admin/admin-ajax.php",
				data: formData,
				success:function(response){ 
					$(".spinner-border").hide(); 
					if(response.status==true){
						$(".dmform-error").hide();
						$(".dmform-success").show();
						$("#commentForm")[0].reset();
					}else if(response.status=='recaptcha_error' ){	
						$(".dmform-error").show();
						$(".dmform-error").html(response.msg);
					}else{
						$(".dmform-error").show();						
						$("#commentForm")[0].reset();
						$(".dmform-error").html(response.msg);
						grecaptcha.reset();
					}
					
				},
				beforeSend:function(response){
					$(".spinner-border").show();
				}						
			});
		});	
	});
</script> 