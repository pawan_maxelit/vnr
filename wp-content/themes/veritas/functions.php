<?php
/**
 * Veritas functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Veritas
 */
define('GOOGLE_RECAPTCH_KEY','6LdRo6kcAAAAACtE4kq6BeSrEBGxmQFXrbUnIqS3');
define('GOOGLE_RECAPTCH_SECRET','6LdRo6kcAAAAAKGG42HkM63SRdg6DTEXQQKp6WMI');
if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'veritas_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function veritas_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Veritas, use a find and replace
		 * to change 'veritas' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'veritas', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'veritas' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'veritas_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'veritas_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function veritas_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'veritas_content_width', 640 );
}
add_action( 'after_setup_theme', 'veritas_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function veritas_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'veritas' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'veritas' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'veritas_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function veritas_scripts() {
	wp_enqueue_style( 'veritas-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'veritas-style', 'rtl', 'replace' );

	wp_enqueue_script( 'veritas-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'veritas_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
function add_menu_link_class($atts, $item, $args)
{
    if($args->menu == 'Home') {
    $atts['class'] = 'unifiednav__item-wrap';
    return $atts;
}else{
	$atts['class'] = '';
    return $atts;
}

}
add_filter('nav_menu_css_class', 'add_menu_link_class', 1, 3);
add_filter( 'nav_menu_link_attributes', 'wp_menu_add_class', 10, 3 );

function wp_menu_add_class( $atts, $item, $args ) {
// or something based on $item
	if($args->menu == 'Home') {
    $atts['class'] = 'unifiednav__item   dmUDNavigationItem_00';
    return $atts;
}else{
	$atts['class'] = '';
    return $atts;
}
}
/*add_filter('nav_menu_submenu_css_class','some_function', 10, 3 );
function some_function( $classes, $args, $depth ){

    foreach ( $classes as $key => $class ) {
        if ( $class == 'sub-menu' && $depth == 0) {
            $classes[ $key ] = 'unifiednav__container unifiednav__container_sub-nav';
        }elseif($class == 'sub-menu' && $depth == 1){
            $classes[ $key ] = 'unifiednav__container unifiednav__container_sub-nav';
        }
    } 
    return $classes;
}*/
// Register Custom Post Type
function custom_post_type_testimonial() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Testimonials', 'text_domain' ),
		'name_admin_bar'        => __( 'Testimonials', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'text_domain' ),
		'description'           => __( 'Testimonial Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'trackbacks', 'revisions' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'Testimonials', $args );

}
add_action( 'init', 'custom_post_type_testimonial', 0 );
// Register Custom Post Type
function custom_post_type_services() {

	$labels = array(
		'name'                  => _x( 'Services', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Services', 'text_domain' ),
		'name_admin_bar'        => __( 'Services', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Service', 'text_domain' ),
		'description'           => __( 'Services Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'comments', 'trackbacks', 'revisions' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'Services', $args );

}
add_action( 'init', 'custom_post_type_services', 0 );

/** Ajax **/

/* This is new signup process*/	
add_action('wp_ajax_nopriv_job_ajax','job_ajax');
add_action('wp_ajax_job_ajax','job_ajax');	
function job_ajax(){
	global $wpdb;
	$postData= $_POST;
	$response=[];
	if(!empty($_POST['g-recaptcha-response']))
	 {
        $secret = GOOGLE_RECAPTCH_SECRET;
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success){
			$response['status']='true';
        } else {
            $message = "Some error in vrifying g-recaptcha";
			$response['status']='recaptcha_error';
			$response['msg']=$message;
			echo json_encode($response);
			exit();
		}
	}else{
		$message = "Please verify the recaptchax";
		$response['status']='recaptcha_error';
		$response['msg']=$message;
		echo json_encode($response);
		exit();
	}
	$headers = array('Content-Type: text/html; charset=UTF-8');
	//$to =get_option('admin_email');
	$to="madan@braintechnosys.com";
	$subject = 'Applied job request';
	$name= @$_POST['name'];
	$email= @$_POST['email'];
	$phone= @$_POST['phone'];
	$message= @$_POST['message'];
	$job_name= @$_POST['job_name'];
	
	$body = '<table border="0" width="600" align="center" style="padding:20px; margin-bottom:20px; border:1px solid #c3a932;" cellpadding="5" cellspacing="5" bgcolor="#333">
	<tr>
		<td colspan="2" align="center" style="color:#c3a932; font-size:18px; font-weight:bold; border-bottom:2px solid #c3a932">Veritas Applied Job</td>
		</tr>
		<tr>
			<th align="left" style="color:#c3a932;" valign="top">Name</th>
		<td align="left" style="color:#c3a932;" valign="top">'.$name.'</td>
		</tr>
		<tr>
		<th align="left" style="color:#c3a932;" valign="top">Email</th>
		<td align="left" style="color:#c3a932;" valign="top">'.$email.'</td>
		</tr>
		<tr>
		<th align="left" style="color:#c3a932;" valign="top">Phone Number</th>
		<td align="left" style="color:#c3a932;" valign="top">'.$phone.'</td>
		</tr>
		<tr> 
		<th align="left" style="color:#c3a932;" valign="top">Message</th>
		<td align="left" style="color:#c3a932;" valign="top">'.$message.'</td>
		</tr>
		<tr> 
		<th align="left" style="color:#c3a932;" valign="top">Job Name</th>
		<td align="left" style="color:#c3a932;" valign="top">'.$job_name.'</td>
		</tr>
		<tr>
		<td colspan="2" style="color:#c3a932; font-size:14px;font-style:italic;">Regards,<br>The veritas Team.</td>
		</tr>
			<tr>
		<th align="left" style="color:#c3a932;">&nbsp;</th>
		<td align="left" style="color:#c3a932;">&nbsp;</td>
		</tr>
	</table>';
	
	$reslt=wp_mail( $to, $subject, $body, $headers );
	if($reslt){
		$response['status']='true';
		$response['msg']="Your Data successfuly Submited.";
	}else{
		$response['status']='false';
		$response['msg']="Oops, there was an error sending your message. Please try again later.";
	}
	echo json_encode($response);
	exit();
}	

/* Inquery Ajax*/	
add_action('wp_ajax_nopriv_contact_form_inquery','contact_form_inquery');
add_action('wp_ajax_contact_form_inquery','contact_form_inquery');	
function contact_form_inquery(){
	global $wpdb;
	$postData= $_POST;
	$response=[];
	if(!empty($_POST['g-recaptcha-response']))
	 {
        $secret = GOOGLE_RECAPTCH_SECRET;
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
        $responseData = json_decode($verifyResponse);
        if($responseData->success){
			$response['status']='true';
        } else {
            $message = "Some error in vrifying g-recaptcha";
			$response['status']='recaptcha_error';
			$response['msg']=$message;
			echo json_encode($response);
			exit();
		}
	}else{
		$message = "Please verify the recaptcha";
		$response['status']='recaptcha_error';
		$response['msg']=$message;
		echo json_encode($response);
		exit();
	}
	$headers = array('Content-Type: text/html; charset=UTF-8');
	//$to =get_option('admin_email');
	$to ="madan@braintechnosys.com";
	$subject = 'Contact user';
	$name=$_POST['name'];
	$email=$_POST['email'];
	$phone=$_POST['phone'];
	$message=$_POST['message'];
	$body = '<table border="0" width="600" align="center" style="padding:20px; margin-bottom:20px; border:1px solid #c3a932;" cellpadding="5" cellspacing="5" bgcolor="#333">
	<tr>
		<td colspan="2" align="center" style="color:#c3a932; font-size:18px; font-weight:bold; border-bottom:2px solid #c3a932">Veritas Contact</td>
		</tr>
		<tr>
			<th align="left" style="color:#c3a932;" valign="top">Name</th>
		<td align="left" style="color:#c3a932;" valign="top">'.$name.'</td>
		</tr>
		<tr>
		<th align="left" style="color:#c3a932;" valign="top">Email</th>
		<td align="left" style="color:#c3a932;" valign="top">'.$email.'</td>
		</tr>
		<tr>
		<th align="left" style="color:#c3a932;" valign="top">Phone Number</th>
		<td align="left" style="color:#c3a932;" valign="top">'.$phone.'</td>
		</tr>
		<tr> 
		<th align="left" style="color:#c3a932;" valign="top">Message</th>
		<td align="left" style="color:#c3a932;" valign="top">'.$message.'</td>
		</tr>
		<tr>
		<td colspan="2" style="color:#c3a932; font-size:14px;font-style:italic;">Regards,<br>The veritas Team.</td>
		</tr>
			<tr>
		<th align="left" style="color:#c3a932;">&nbsp;</th>
		<td align="left" style="color:#c3a932;">&nbsp;</td>
		</tr>
	</table>';
	$reslts=wp_mail($to, $subject, $body, $headers );
	if($reslts){
		$response['status']='true';
		$response['msg']="Your data successfuly submited.";
	}else{
		$response['status']='false';
		$response['msg']="Oops, there was an error sending your message. Please try again later.";
	}
	echo json_encode($response);
	exit();
}	
add_filter('site_transient_update_plugins', 'my_remove_update_nag');
function my_remove_update_nag($value) {
 unset($value->response['advanced-custom-fields-pro/acf.php']);
 return $value;
}
?>
