<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Veritas
 */

get_header();
?>
<link rel="preload" href="<?php echo get_stylesheet_directory_uri()?>/css/51716a39_home_1.min.css" id="homeCssLink" as="style" importance="low" onload="loadCSS(this)" />
<link rel="preload" href="<?php echo get_stylesheet_directory_uri()?>/css/51716a39_1.min.css" id="siteGlobalCss" as="style" importance="low" onload="loadCSS(this)" />
 <link rel="preload" as="style" importance="low" onload="loadCSS(this)" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/css/media.css' ?>" />
	<main id="primary" class="site-main">

		<section class="error-404 not-found">
			<header class="page-header">
				<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'veritas' ); ?></h1>
			</header><!-- .page-header -->

			<div class="page-content">
				<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'veritas' ); ?></p>
			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
