<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Veritas
 */

?>
<style id="globalFontSizeStyle" type="text/css">
         .font-size-26, .size-26, .size-26 > font { font-size: 26px !important; }.font-size-36, .size-36, .size-36 > font { font-size: 36px !important; }.font-size-17, .size-17, .size-17 > font { font-size: 17px !important; }.font-size-24, .size-24, .size-24 > font { font-size: 24px !important; }.font-size-24, .size-24, .size-24 > font { font-size: 24px !important; }.font-size-17, .size-17, .size-17 > font { font-size: 17px !important; }.font-size-30, .size-30, .size-30 > font { font-size: 30px !important; }.font-size-36, .size-36, .size-36 > font { font-size: 36px !important; }.font-size-48, .size-48, .size-48 > font { font-size: 48px !important; }.font-size-24, .size-24, .size-24 > font { font-size: 24px !important; }.font-size-24, .size-24, .size-24 > font { font-size: 24px !important; }.font-size-22, .size-22, .size-22 > font { font-size: 22px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }
      </style>
<footer id="colophon" class="site-footer">
	<div class="dmFooterContainer">
   <div id="fcontainer" class="f_hcontainer dmFooter p_hfcontainer">
      <div dm:templateorder="14" class="dmFooterResp dmFullFooterResp" id="1602863267">
         <div class="dmRespRow dmDividerRow" id="1314492676">
            <div class="dmRespColsWrapper" id="1735367830">
               <div class="large-12 medium-12 small-12 dmRespCol" id="1209666878">
                  <div class="dmDividerWrapper clearfix" id="1776315408" data-element-type="dDividerId">
                     <hr class="dmDivider defaultDivider" id="1180913438"/>
                  </div>
               </div>
            </div>
         </div>
         <div class="u_1287975267 dmRespRow dmFooterContent" id="1287975267">
            <div class="dmRespColsWrapper" id="1562264047">
               <div class="u_1207772639 dmRespCol small-12 dmFooterCol large-9 medium-9" id="1207772639">
                  <h3 class="dmFooterDefaultH3" id="1021107606">Follow Us</h3>
                  <div class="u_1766957055 dmNewParagraph" data-dmtmpl="true" id="1766957055" data-uialign="left">
                     <div style="text-align: left;"><span style="font-weight: 400;"><font style=""><font style="color: rgb(255, 255, 255);"><?php echo get_theme_mod( 'copyright_text'); ?>|&nbsp;</font><font style="color: rgb(255, 255, 255); text-decoration-line: underline;"><a href="<?php echo get_theme_mod( 'private_policy_link'); ?>" runtime_url="/privacy-policy" style="">Privacy Policy</a></font></font></span></div>
                  </div>
                  <div class="dmRespRow u_1581118233" id="1581118233">
                     <div class="dmRespColsWrapper" id="1772450751">
                        <div class="dmRespCol small-12 large-4 medium-4" id="1581886145">
                           <div class="dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1557300118" style="transition: opacity 1s ease-in-out 0s;" data-uialign="left">
                              <?php echo get_theme_mod( 'footer_section_text1'); ?>
                           </div>
                        </div>
                        <div class="dmRespCol small-12 large-4 medium-4" id="1486425540">
                           <div class="dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1487523527" style="transition: opacity 1s ease-in-out 0s;" data-uialign="center">
                           	<?php echo get_theme_mod( 'footer_section_text2'); ?>
                           </div>
                        </div>
                        <div class="dmRespCol large-4 medium-4 small-12" id="1713601598">
                           <div class="dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1164720775" style="transition: opacity 1s ease-in-out 0s;" data-uialign="right">
                              <?php echo get_theme_mod( 'footer_section_text3'); ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="u_1955650092 dmRespCol small-12 large-3 medium-3" id="1955650092">
                  <div class="u_1199030155 default dmSocialHub" id="1199030155" dont_set_id="true" dmle_extension="social_hub" data-element-type="social_hub" wr="false" networks="" icon="false" surround="false">
                     <div class="socialHubWrapper">
                        <div class="socialHubInnerDiv vertical">
                        	<?php  if(get_theme_mod( 'facebook_url')) : ?> 
                        	<a href="<?php  echo get_theme_mod('facebook_url');?>" target="_blank" dmle_dont_remove="target" dm_dont_rewrite_url="true"> <span class="dmSocialFacebook dm-social-icons-facebook oneIcon socialHubIcon style3" data-hover-effect=""></span> 
                           </a>
                           <?php endif; ?>
                           <?php  if(get_theme_mod( 'twitter_url')) : ?>  
                           <a href="<?php  echo get_theme_mod('twitter_url');?>" target="_blank" dmle_dont_remove="target" dm_dont_rewrite_url="true"> <span class="dmSocialTwitter dm-social-icons-twitter oneIcon socialHubIcon style3" data-hover-effect=""></span> 
                           </a> 
                           <?php endif; ?>
                           <?php  if(get_theme_mod('instagram_url')) : ?>  
                           <a href="<?php  echo get_theme_mod('instagram_url');?>" target="_blank" dmle_dont_remove="target" dm_dont_rewrite_url="true"> <span class="dmSocialInstagram dm-social-icons-Instagram oneIcon socialHubIcon style3" data-hover-effect=""></span> 
                           </a> 
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="1482260810" dmle_extension="powered_by" data-element-type="powered_by" icon="true" surround="false" data-dmtmpl="true"></div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="<?php echo get_template_directory_uri();?>/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
/*$('.demo').slick({
    dots: false,
    arrows: true,
    speed: 500,
    autoplay: false,
    slidesToShow: 1,
  });*/
</script>
<!-- <div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'veritas' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'veritas' ), 'WordPress' );
				?>
			</a>
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'veritas' ), 'veritas', '<a href="http://underscores.me/">Underscores.me</a>' );
				?>
		</div> --><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script>
$(document).ready(function(){
	$(".icon-angle-down").show();
})

</script>
</body>
</html>
