<?php get_header(); ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(). '/css/about-inline.css'; ?>">

<div id="site_content">
	<div class="dmRespRow dmRespRowStable dmRespRowNoPadding dmPageTitleRow dmInnerBarRow">
		<div class="dmRespColsWrapper">
		  <div class="large-12 dmRespCol">
			 <div id="innerBar" class="innerBar lineInnerBar dmDisplay_None">
				<div class="titleLine display_None">
				   <hr/>
				</div>
				<!-- Page title is hidden in css for new responsive sites. It is left here only so we don't break old sites. Don't copy it to new layouts --> 
				<div id="pageTitleText">
				   <div class="innerPageTitle"><?php the_title(); ?></div>
				</div>
				<div class="titleLine display_None">
				   <hr/>
				</div>
			 </div>
		  </div>
		</div>
		</div>
		<div dmwrapped="true" id="1275460582" class="dmBody u_dmStyle_template_about">
		<div id="allWrapper" class="allWrapper">
		  <!-- navigation placeholders --> 
		  <div id="dm_content" class="dmContent">
			 <div dm:templateorder="112" class="dmDefaultRespTmpl mainBorder" id="1479330477">
				<div class="innerPageTmplBox dmDefaultPage dmAboutListPage dmRespRowsWrapper" id="1736103396">
				   <section class="u_1897767439 dmRespRow dmHomeSection1 dmHomeSectionNoBtn dmSectionNoParallax hasBackgroundOverlay" id="1897767439">
					  <div class="dmRespColsWrapper" id="1010790795">
						 <div class="dmRespCol large-12 medium-12 small-12" id="1139052171">
							<h3 class="u_1453001980 dmNewParagraph" id="1453001980" data-element-type="paragraph" data-uialign="left" style="top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px auto 8px 0px; float: none; width: 960px; height: auto;">
							   <div style="text-align: center;"><span style="color: rgb(255, 255, 255); font-weight: 600;"><?php the_title(); ?></span></div>
							</h3>
						 </div>
					  </div>
				   </section>
				   <div class="u_1422486861 dmRespRow" style="text-align: center;" id="1422486861">
					  <div class="dmRespColsWrapper" id="1640410774">
						 <div class="u_1084594921 dmRespCol small-12 large-12 medium-12" id="1084594921">
							<div class="widget-85b0db u_1855841291 dmCustomWidget">
							   <div class="wrapper">
								  <div class="spacer spacer-right"></div>
								  <?php while(have_posts()):the_post();the_content();endwhile; ?>
								  <div style="clear:both"></div>
							   </div>
							</div>
							
						</div>
                     </div>
				   </div>
				</div>
			 </div>
		  </div>
		</div>
	</div>
</div>
<!--- Page css -->
<link rel="preload" href="<?php echo get_stylesheet_directory_uri()?>/css/51716a39_1.min.css" id="siteGlobalCss" as="style" importance="low" onload="loadCSS(this)" />
<link rel="preload" onload="loadCSS(this)" href="<?php echo get_template_directory_uri(); ?>/css/about-min.css" />
<link rel="preload" href="<?php echo get_template_directory_uri(); ?>/css/about.css" id="widgetCSS" as="style" importance="low" onload="loadCSS(this)" />
<?php get_footer(); ?>	
<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
<script src="<?php echo get_stylesheet_directory_uri()?>/js/jquery.validate.js"></script>  
<script>
	$(document).ready(function(){
		$("#commentForm").validate();
		$('body').on('submit','#commentForm',function(e){
			var base_url = "<?php echo home_url();?>";
			var formData= $('#commentForm').serialize();
			console.log(formData);
			e.preventDefault();
			 jQuery.ajax({ 
				type:"post",
				dataType : 'json',
				url: base_url+"/wp-admin/admin-ajax.php",
				data: formData,
				success:function(response){ 
					$(".spinner-border").hide(); 
					if(response.status==true){
						$(".dmform-error").hide();
						$(".dmform-success").show();
						$("#commentForm")[0].reset();
					}else if(response.status=='recaptcha_error' ){	
						$(".dmform-error").show();
						$(".dmform-error").html(response.msg);
					}else{
						$(".dmform-error").show();						
						$("#commentForm")[0].reset();
						$(".dmform-error").html(response.msg);
						grecaptcha.reset();
					}
					
				},
				beforeSend:function(response){
					$(".spinner-border").show();
				}						
			});
		});	
	});

</script> 

