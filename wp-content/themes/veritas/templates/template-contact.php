<?php
/*
* Template Name:Contact
*/
get_header()
?>
<style>
.dmformsubmit { float:left !important;}
.dmform-error {
    color: #f00909b0 !important;;
    float: left !important;;
}
</style>
<div id="site_content">
		<div class="dmRespRow dmRespRowStable dmRespRowNoPadding dmPageTitleRow dmInnerBarRow">
		   <div class="dmRespColsWrapper">
			  <div class="large-12 dmRespCol">
				 <div id="innerBar" class="innerBar lineInnerBar dmDisplay_None">
					<div class="titleLine display_None">
					   <hr/>
					</div>
					<!-- Page title is hidden in css for new responsive sites. It is left here only so we don't break old sites. Don't copy it to new layouts --> 
					<div id="pageTitleText">
					   <div class="innerPageTitle"><?php the_title() ?></div>
					</div>
					<div class="titleLine display_None">
					   <hr/>
					</div>
				 </div>
			  </div>
		   </div>
		</div>
		<div dmwrapped="true" id="1635567076" themewaschanged="true" class="dmBody u_dmStyle_template_contact">
		   <div id="allWrapper" class="allWrapper">
			  <!-- navigation placeholders --> 
			  <div id="dm_content" class="dmContent">
				 <div dm:templateorder="111" class="dmContactUs2RespTmpl dmContactTitleTmpl" id="1253157095">
					<!-- this template has changed in 26/6/14 --> 
					<div class="innerPageTmplBox dmRespRowsWrapper" id="1247080059">
					   <section class="u_1307214832 dmRespRow dmHomeSection1 dmHomeSectionNoBtn dmSectionNoParallax hasBackgroundOverlay" id="1307214832">
						  <div class="dmRespColsWrapper" id="1110724283">
							 <div class="dmRespCol large-12 medium-12 small-12" id="1476720603">
								<h3 class="u_1969991425 dmNewParagraph" id="1969991425" data-element-type="paragraph" data-uialign="left" style="top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px auto 8px 0px; float: none; width: 960px; height: auto;">
								   <div style="text-align: center;"><span style="color: rgb(255, 255, 255); font-weight: 600;">CONTACT US</span></div>
								</h3>
							 </div>
						  </div>
					   </section>
					   <div class="dmRespRow u_1068790658" id="1068790658">
						  <div class="dmRespColsWrapper" id="1980225027">
							 <div class="dmRespCol large-12 medium-12 small-12" id="1775750777">
								<h3 class="u_1907609979 dmNewParagraph" id="1907609979" data-element-type="paragraph" data-uialign="center" style="top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px auto 8px 0px; float: none; width: 960px; height: auto;">
								   <div style="text-align: center;"><font color="#000000"><b><span style="font-weight: 400;" class="lh-1 font-size-28"><?php echo get_field('heading')?></span></b></font></div>
								</h3>
							 </div>
						  </div>
					   </div>
					   <div class="u_1253345738 dmRespRow contact_mobile" style="text-align: center;" id="1253345738">
						  <div class="dmRespColsWrapper" id="1445166595">
							 <div class="u_1727328452 dmRespCol small-12 large-3 medium-3" id="1727328452">

								<div class="u_1384753443 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1384753443" data-uialign="left" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 16px 0px 0px; float: none; width: 367px; height: auto; position: relative; max-width: 166px !important; min-width: 166px !important;"><font style="color: rgb(0, 0, 0);" class="lh-1 font-size-22">Contact Details</font></div>
								<div class="dmNewParagraph u_1665393511" data-dmtmpl="true" data-element-type="paragraph" id="1665393511" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 16px 0px 0px; float: none; width: 207px; height: 166px; position: relative; max-width: 207px !important; min-width: 207px !important;" data-uialign="left">
								   <div><?php  echo get_theme_mod('contact_details');?><br></div>
								   <div>
									  <div>
										 <div><font style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><a href="tel:(561) 731-3155" runtime_url="tel:(561) 731-3155" style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><?php  echo get_theme_mod('contact_phone_no');?></a></font></div>
										 <div><br/></div>
										 <div><font style="color: rgb(46, 176, 246);"><a href="mailto:<?php  echo get_theme_mod('contact_email');?>" runtime_url="mailto:admin@veritasnursecare.com" style="color: rgb(46, 176, 246);"><?php  echo get_theme_mod('contact_email');?></a></font></div>
										 <div><br/></div>
										 <div><font style="color: rgb(0, 0, 0);" class="font-size-16 lh-1"><?php  echo get_theme_mod('office_hours');?></font>
										 </div>
									  </div>
								   </div>
								</div>
								<div class="u_1718609389 dmDividerWrapper clearfix" data-element-type="dDividerId" data-layout="divider-style-1" data-widget-version="2" id="1718609389">
								   <hr class="dmDivider" style="border-width:2px; border-top-style:solid; color:grey;" id="1490358540"/>
								</div>
								<div class="u_1485000922 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1485000922" data-uialign="left" data-styletopreserve="{" background-image":""}"="" data-editor-state="closed"> 
								<div><span style="background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_address1');?></span><br/></div>
								<div>
								   <div>
									  <div>
										 <span style="" class="font-size-14 lh-1">
											
											<span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_address2');?></span><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><br/></span>
										 </span>
									  </div>
									  <div>
										 
									  </div>
								   </div>
								</div>
							 </div>
						  </div>
						  <div class="u_1346210134 dmRespCol small-12 large-9 medium-9" id="1346210134">
							 <div class="dmRespRow u_1944701691" id="1944701691">
								<div class="dmRespColsWrapper" id="1058833254">
								   <div class="dmRespCol small-12 medium-6 large-6" id="1151013822">
									  <div class="dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1862990277" style="transition: opacity 1s ease-in-out 0s;" data-uialign="center">
										 <p class="m-size-11 size-16"><span class="m-font-size-11 font-size-16" style="color: rgb(0, 0, 0); display: initial; font-weight: bold;"><span class="m-font-size-11 font-size-16" style="color: rgb(0, 0, 0); display: initial; font-weight: bold;">Veritas Home Care Inc. Accepted Insurance</span> 
											</span>
										 </p>
										 <p><br/></p>
										 <ul class="defaultList">
										 	<?php 
										 	if( have_rows('veritas_home_care_inc_accepted_insurance') ):
										 		while( have_rows('veritas_home_care_inc_accepted_insurance') ) : the_row();
										 	?>
											<li class="m-size-11 size-16" style="color: rgb(0, 0, 0);"><span class="m-font-size-11 font-size-16" style="color: rgb(0, 0, 0); display: initial;"><?php echo get_sub_field('veritas_home_care_'); ?></span></li>
											<?php endwhile; 
											endif;
											?>
										 </ul>
									  </div>
								   </div>
								   <div class="dmRespCol small-12 medium-6 large-6" id="1056679887">
									  <div class="u_1156329986 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1156329986" style="transition: opacity 1s ease-in-out 0s;" data-uialign="right">
										 <p class="m-size-11 size-16"><span class="font-size-16 m-font-size-11" style="font-weight: 700; display: initial; color: rgb(0, 0, 0);"><span class="font-size-16 m-font-size-11" style="font-weight: 700; display: initial; color: rgb(0, 0, 0);">Veritas Nurse Registry Inc. Accepted Insurance</span> 
											</span>
										 </p>
										 <p><br/></p>
										 <ul class="defaultList">
										 	<?php 
										 	if( have_rows('veritas_nurse_registry_inc_accepted_insurance') ):
										 		while( have_rows('veritas_nurse_registry_inc_accepted_insurance') ) : the_row();
										 	?>
											<li class="m-size-11 size-16" style="color: rgb(0, 0, 0);"><span class="font-size-16 m-font-size-11" style="display: initial; color: rgb(0, 0, 0);"><?php echo get_sub_field('veritas_nurse_registry'); ?></span></li>
											<?php endwhile; 
											endif;
											?>
										 </ul>
									  </div>
								   </div>
								</div>
							 </div>
							 <?php get_template_part('partials/contact/contact'); ?>
						  </div>
					   </div>
					</div>
					<section class="u_1867437211 dmRespRow dmHomeSection1 dmHomeSectionNoBtn fullBleedChanged fullBleedMode" id="1867437211">
					   <div class="dmRespColsWrapper" id="1465391158">
						  <div class="dmRespCol large-12 medium-12 small-12" id="1759144300">
							<iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" id="gmap_canvas" src="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=Veritas%20Home%20Care%20Inc.%20Palm%20Beach%20County%20License%20+(map)&amp;t=&amp;z=12&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe> <a href='https://add-map.com/'>how to add a google map to your website</a> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=2452023c5515de43a12fbecdfb2cad7ff7be6930'></script>
						  </div>
					   </div>
					</section>
				 </div>
			  </div>
		   </div>
		</div>
	 </div>
</div>  
                     
<link rel="preload" href="<?php echo get_stylesheet_directory_uri()?>/css/51716a39_1.min.css" id="siteGlobalCss" as="style" importance="low" onload="loadCSS(this)" />
<link rel="preload" as="style" importance="low" onload="loadCSS(this)" href="<?php echo get_template_directory_uri(); ?>/css/contact.css">
<link rel="preload" as="style" importance="low" onload="loadCSS(this)" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/css/media.css' ?>" />
<!--script src="<?php echo get_template_directory_uri(); ?>/js/contact.js"></script-->
<?php get_footer(); ?>
<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
<script src="<?php echo get_stylesheet_directory_uri()?>/js/jquery.validate.js"></script>  
<script>
	$(document).ready(function(){
		$("#commentForm").validate();
		$('body').on('submit','#commentForm',function(e){
			var base_url = "<?php echo home_url();?>";
			var formData= $('#commentForm').serialize();
			console.log(formData);
			e.preventDefault();
			 jQuery.ajax({ 
				type:"post",
				dataType : 'json',
				url: base_url+"/wp-admin/admin-ajax.php",
				data: formData,
				success:function(response){ 
					$(".spinner-border").hide(); 
					if(response.status==true){
						$(".dmform-error").hide();
						$(".dmform-success").show();
						$("#commentForm")[0].reset();
					}else if(response.status=='recaptcha_error' ){	
						$(".dmform-error").show();
						$(".dmform-error").html(response.msg);
					}else{
						$(".dmform-error").show();						
						$("#commentForm")[0].reset();
						$(".dmform-error").html(response.msg);
						grecaptcha.reset();
					}
					
				},
				beforeSend:function(response){
					$(".spinner-border").show();
				}						
			});
		});	
	});
</script> 