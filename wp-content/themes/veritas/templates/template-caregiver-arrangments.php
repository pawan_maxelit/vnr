<?php
/**
 * Template Name: Caregiver arrangements
 * Template Post Type: services
 * The template for displaying Home page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>
<!-- RT CSS Include d-css-runtime-desktop-one-package-new-->
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri()?>/mnlt/production/1903/_dm/s/rt/dist/css/d-css-runtime-desktop-one-package-new.min.css" />
<!-- End of RT CSS Include -->
       <link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/widget_css/production_1903/004b5bdd245110b6c6336267aa0e53b8.css" id="widgetCSS" />
<!-- Support `img` size attributes -->
<style>img[width][height] {height: auto;}</style>
<!-- This is populated in Ajax navigation -->
<style id="pageAdditionalWidgetsCss" type="text/css">
</style>
<!-- Site CSS -->
<link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/51716a39/files/51716a39_1.min.css" id="siteGlobalCss" />
<style>
.dmformsubmit { float:left !important;}
.dmform-error {
    color: #f00909b0 !important;;
    float: left !important;;
}
</style>
<style id="pagestyle" type="text/css">
    *#dm *.dmBody div.u_1422486861
{
	background-color:rgba(0,0,0,0) !important;
}
*#dm *.dmBody section.u_1897767439
{
	background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-2880w.jpg) !important;
	background-color:rgba(181,49,44,1) !important;
	background-repeat:no-repeat !important;
	background-size:cover !important;
}
*#dm *.dmBody *.u_1897767439:before
{
	opacity:0.5 !important;
	background-color:rgb(255,255,255) !important;
}
*#dm *.dmBody *.u_1897767439.before
{
	opacity:0.5 !important;
	background-color:rgb(255,255,255) !important;
}
*#dm *.dmBody *.u_1897767439>.bgExtraLayerOverlay
{
	opacity:0.5 !important;
	background-color:rgb(255,255,255) !important;
}
*#dm *.dmBody section.u_1897767439:before
{
	background-color:rgba(181,49,44,1) !important;
	opacity:0.92 !important;
}
*#dm *.dmBody section.u_1897767439.before
{
	background-color:rgba(181,49,44,1) !important;
	opacity:0.92 !important;
}
*#dm *.dmBody section.u_1897767439>.bgExtraLayerOverlay
{
	background-color:rgba(181,49,44,1) !important;
	opacity:0.92 !important;
}
*#dm *.dmBody div.u_1505275905 input:not([type="submit"])
{
	border-style:solid !important;
	border-width:0 !important;
	background-color:rgba(227,227,227,1) !important;
}
*#dm *.dmBody div.u_1505275905 textarea
{
	border-style:solid !important;
	border-width:0 !important;
	background-color:rgba(227,227,227,1) !important;
}
*#dm *.dmBody div.u_1505275905 select
{
	border-style:solid !important;
	border-width:0 !important;
	background-color:rgba(227,227,227,1) !important;
}
*#dm *.dmBody *.u_1505275905 .dmformsubmit
{
	float:LEFT !important;
}
*#dm *.dmBody div.u_1505275905 .dmform-success
{
	color:rgba(102,102,102,1) !important;
}
*#dm *.dmBody div.dmform-error
{
	color:rgba(102,102,102,1) !important;
}
*#dm *.dmBody div.u_1505275905 .dmform-success .rteBlock
{
	color:rgba(102,102,102,1) !important;
}
*#dm *.dmBody div.dmform-error .rteBlock
{
	color:rgba(102,102,102,1) !important;
}
*#dm *.dmBody div.u_1505275905 .dmwidget-title
{
	color:rgba(102,102,102,1) !important;
}

*#dm *.dmBody div.u_1505275905 .m-recaptcha
{
	color:rgba(102,102,102,1) !important;
}
*#dm *.dmBody div.u_1703643831
{
	color:rgba(0,0,0,1) !important;
}
*#dm *.dmBody nav.u_1382060198
{
	color:black !important;
}
*#dm *.dmBody nav.u_1382060198.main-navigation.unifiednav .unifiednav__container:not([data-depth])>.unifiednav__item-wrap>.unifiednav__item
{
	color:rgba(181,49,44,1) !important;
}
*#dm *.dmBody *.u_1382060198.main-navigation.unifiednav:not([data-nav-structure='VERTICAL']) .unifiednav__container:not([data-depth])>.unifiednav__item-wrap:not(:last-child)::before
{
	font-size:12px !important;
}
*#dm *.dmBody *.u_1382060198.main-navigation.unifiednav:not([data-nav-structure='VERTICAL']) .unifiednav__container:not([data-depth])>.unifiednav__item-wrap:not(:last-child)::after
{
	font-size:12px !important;
}
</style>
<style id="pagestyleDevice" type="text/css">
*#dm *.dmBody section.u_1897767439
{
	background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-1920w.jpg) !important;
	background-color:rgba(181,49,44,1) !important;
	background-repeat:no-repeat !important;
	background-size:cover !important;
}@media all and (min-width:1920px), all and (-webkit-min-device-pixel-ratio: 1.5), all and (min--moz-device-pixel-ratio: 1.5), all and (min-device-pixel-ratio: 1.5) {
*#dm *.dmBody section.u_1897767439
{
	background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-2880w.jpg) !important;
	background-color:rgba(181,49,44,1) !important;
	background-repeat:no-repeat !important;
	background-size:cover !important;
}
}
*#dm *.dmBody h1.u_1453001980
{
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	float:none !important;
	margin-right:157.5px !important;
	margin-left:157.5px !important;
	top:0 !important;
	max-width:calc(100% - 157px) !important;
	left:0 !important;
	width:645px !important;
	margin-top:66px !important;
	position:relative !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	height:auto !important;
	display:block !important;
}
*#dm *.dmBody h3.u_1453001980
{
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:auto !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody section.u_1897767439
{
	padding-top:13.5px !important;
	padding-left:40px !important;
	padding-bottom:13.5px !important;
	float:none !important;
	top:0 !important;
	max-width:100% !important;
	left:0 !important;
	width:auto !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	background-attachment:initial !important;
	background-position:0 50% !important;
}
*#dm *.dmBody div.u_1505275905 .dmwidget-title
{
	font-size:18px !important;
}
*#dm *.dmBody div.u_1422486861
{
	padding-top:36px !important;
	padding-left:40px !important;
	padding-bottom:36.5px !important;
	float:none !important;
	top:auto !important;
	max-width:none !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
}
*#dm *.dmBody div.u_1097261032
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	margin-right:auto !important;
	margin-left:auto !important;
	max-width:483px !important;
	margin-top:9px !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	height:auto !important;
}
*#dm *.dmBody div.u_1262511931
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:483px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
}
*#dm *.dmBody div.u_1262511931
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:483px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
}
*#dm *.dmBody div.u_1703643831
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:207px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1703643831
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:207px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1330770612
{
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:166px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0px !important;
	padding-bottom:2px !important;
	margin-right:auto !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0px !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody div.u_1505275905
{
	padding-top:0px !important;
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-left:0px !important;
	padding-bottom:0px !important;
	margin-right:0px !important;
	margin-left:0px !important;
	max-width:100% !important;
	margin-top:0px !important;
	margin-bottom:0px !important;
	padding-right:0px !important;
	min-width:25px !important;
	text-align:center !important;
}
*#dm *.dmBody div.u_1932266037
{
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:225.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0px !important;
	min-width:0 !important;
	display:block !important;
}
*#dm *.dmBody div.u_1932266037
{
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:225.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0px !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0px !important;
	min-width:0 !important;
	display:block !important;
}
*#dm *.dmBody nav.u_1382060198.main-navigation.unifiednav .unifiednav__container:not([data-depth])>.unifiednav__item-wrap>.unifiednav__item
{
	font-size:12px !important;
}
*#dm *.dmBody nav.u_1382060198.main-navigation.unifiednav .unifiednav__container:not([data-depth])>.unifiednav__item-wrap>.unifiednav__item
{
	font-size:12px !important;
}
*#dm *.dmBody nav.u_1382060198
{
	padding-top:0px !important;
	padding-left:0px !important;
	padding-bottom:0px !important;
	padding-right:0px !important;
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:225.594px !important;
	position:relative !important;
	height:auto !important;
	max-width:100% !important;
	justify-content:flex-start !important;
	align-items:stretch !important;
	min-width:0 !important;
	text-align:left !important;
	margin-right:auto !important;
	margin-left:0 !important;
	margin-top:10px !important;
	margin-bottom:0px !important;
}
*#dm *.dmBody nav.u_1382060198
{
	padding-top:0px !important;
	padding-left:0px !important;
	padding-bottom:0px !important;
	padding-right:0px !important;
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:225.594px !important;
	position:relative !important;
	height:auto !important;
	max-width:100% !important;
	justify-content:flex-start !important;
	align-items:stretch !important;
	min-width:0 !important;
	text-align:left !important;
	margin-right:auto !important;
	margin-left:0 !important;
	margin-top:10px !important;
	margin-bottom:0px !important;
}
</style>
<!--[if IE 7]><style>.fw-head.fw-logo img{max-width: 290px;}.dm_header .logo-div img{max-width: 290px;}</style><![endif]-->
<!--[if IE 8]><style>.fw-head .fw-logo img{max-width: 290px;}.dm_header .logo-div img{max-width: 290px;}*#dm div.dmHeader{_height:90px;min-height:0px;}</style><![endif]-->
    <style id="globalFontSizeStyle" type="text/css">
        .font-size-26, .size-26, .size-26 > font { font-size: 26px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-13, .size-13, .size-13 > font { font-size: 13px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-13, .size-13, .size-13 > font { font-size: 13px !important; }
    </style>
	
	<link rel="preload" as="style" importance="low" onload="loadCSS(this)" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/css/media.css' ?>" />
 <div id="desktopBodyBox"> <div id="iscrollBody"> <div id="site_content"> <div class="dmRespRow dmRespRowStable dmRespRowNoPadding dmPageTitleRow dmInnerBarRow"> <div class="dmRespColsWrapper"> <div class="large-12 dmRespCol"> <div id="innerBar" class="innerBar lineInnerBar dmDisplay_None"> <div class="titleLine display_None"><hr/></div> 
<!-- Page title is hidden in css for new responsive sites. It is left here only so we don't break old sites. Don't copy it to new layouts --> <div id="pageTitleText"> <div class="innerPageTitle">Caregiver Arrangements</div> 
</div> 
 <div class="titleLine display_None"><hr/></div> 
</div> 
</div> 
</div> 
</div> 
 <div dmwrapped="true" id="1275460582" class="dmBody u_dmStyle_template_caregiver-arrangments">
   <div id="allWrapper" class="allWrapper">
      <!-- navigation placeholders --> 
      <div id="dm_content" class="dmContent">
         <div dm:templateorder="112" class="dmDefaultRespTmpl mainBorder" id="1479330477">
            <div class="innerPageTmplBox dmDefaultPage dmAboutListPage dmRespRowsWrapper" id="1736103396">
               <section class="u_1897767439 dmRespRow dmHomeSection1 dmHomeSectionNoBtn dmSectionNoParallax hasBackgroundOverlay" id="1897767439">
                  <div class="dmRespColsWrapper" id="1010790795">
                     <div class="dmRespCol large-12 medium-12 small-12" id="1139052171">
                        <h3 class="u_1453001980 dmNewParagraph" id="1453001980" data-element-type="paragraph" data-uialign="left" style="top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px auto 8px 0px; float: none; width: 960px; height: auto;">
                           <div style="text-align: center;"><font color="#ffffff"><b>CAREGIVER ARRANGEMENTS</b></font></div>
                        </h3>
                     </div>
                  </div>
               </section>
               <div class="u_1422486861 dmRespRow" style="text-align: center;" id="1422486861">
                  <div class="dmRespColsWrapper" id="1640410774">
                     <div class="u_1084594921 dmRespCol small-12 large-9 medium-9" id="1084594921">
                        <div class="dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1489246653" style="transition: opacity 1s ease-in-out 0s;" data-uialign="left">
                           <b style=""><font style="color: rgb(102, 102, 102);">PART TIME CARE: (1-4 days per week and 3-4 hours per day)</font></b> 
                           <div><font style="color: rgb(102, 102, 102);"><br/></font></div>
                           <div><font style="color: rgb(102, 102, 102);">For seniors who are still quite independent. They are still able to walk and use the restroom by themselves and only needs light assistance with bathing and dressing. They will need help with more difficult tasks such as housekeeping or cooking or meal preparation. They are only occasionally forgetful. They are no longer allowed to drive and they need help to go to their doctor&rsquo;s appointments and to do groceries, shopping, etc.</font></div>
                        </div>
                        <div class="imageWidget align-center u_1097261032" editablewidget="true" data-element-type="image" data-widget-type="image" id="1097261032"> <a id="1852373220"><img src="<?php echo get_field('part_time_image'); ?>" alt="An elderly couple sits in the park while a home health aide takes the man's blood pressure." id="1512807956" class="" data-dm-image-path="https://irp-cdn.multiscreensite.com/51716a39/dms3rep/multi/type2.jpg" title="An elderly couple sits in the park while a home health aide takes the man's blood pressure." onerror="handleImageLoadError(this)"/></a> </div>
                        <div class="dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1284609858" style="transition: opacity 1s ease-in-out 0s;" data-uialign="left">
                           <br/>  
                           <div><font color="#666666"><b>FULL TIME CARE: (5-7 days per week; 5-8 hours per day)</b></font></div>
                           <div><font color="#666666"><b><br/></b></font></div>
                           <div><font color="#666666">For seniors who are somewhat dependent. They have some trouble walking and using the restroom independently, need help with bathing and dressing, and forget important things such as turning off appliances or electronics, location of house or car keys, and medication schedules. These seniors are also unable to go to doctor&rsquo;s appointments by themselves and need help with doing grocery, shopping, etc.</font></div>
                        </div>
                        <div class="imageWidget align-center u_1262511931" editablewidget="true" data-element-type="image" data-widget-type="image" id="1262511931"> <a id="1521830866"><img src="<?php echo get_field('full_time_image'); ?>" alt="A home health aide helps an elderly man out of bed." id="1942064060" class="" data-dm-image-path="https://irp-cdn.multiscreensite.com/51716a39/dms3rep/multi/type1.jpg" title="A home health aide helps an elderly man out of bed." onerror="handleImageLoadError(this)"/></a> </div>
                        <div class="dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1325451452" data-uialign="left" data-styletopreserve="{" background-image":""}"=""><font style="color: rgb(102, 102, 102);"><br/> </font> 
                        <div><font style="color: rgb(102, 102, 102);"><b>ROUND-THE-CLOCK CARE: (7 days per week; 24 hours per day)</b></font></div>
                        <div><font style="color: rgb(102, 102, 102);"><br/></font></div>
                        <div><font style="color: rgb(102, 102, 102);">For seniors who are physically incapacitated or bedridden, cannot use the restroom independently, cannot bathe or dress on his or her own, or is mentally incapacitated as in later stages of Alzheimer&rsquo;s or Dementia. This type of patient is considered dependent and cannot be left alone by themselves. Depending to the condition of the patient, 24 hours of care can be provided by a live-in caregiver or by 2 caregivers with 12-hour shifts.</font></div>
                        <div><font style="color: rgb(102, 102, 102);"><br/></font></div>
                        <div><font style="color: rgb(102, 102, 102);">The above descriptions are used as guidelines only.</font> 
                           <font style="color: rgb(181, 49, 44);">Veritas</font> <font style="color: rgb(102, 102, 102);">will be able to tailor fit a care plan for whatever type of care will be needed by the patients.</font>
                        </div>
                     </div>
                     <?php //get_template_part('partials/contact/contact'); ?>
                  </div>
                   <div class="u_1972778160 dmRespCol small-12 large-3 medium-3" id="1972778160">
                        <div class="u_1101791350 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1101791350" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px 0px; float: none; width: 166px; height: auto; position: relative;" data-uialign="right"><font style="color: rgb(102, 102, 102);" class="font-size-18 lh-1">Contact Us Today!</font></div>
                        <div class="u_1610263383 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1610263383" data-uialign="left" data-styletopreserve="{" background-image":""}"="" data-editor-state="closed"> 
                        <div><span style="" class="lh-1 font-size-16"><font style=""><span style=""><?php  echo get_theme_mod('contact_details');?></span></font></span></div>
                        <div>
                           <div>
                              <span style="" class="lh-1 font-size-16"><font style="color: rgb(181, 49, 44);"><span style=""><br/></span></font> 
                              </span> 
                              <div><font style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><a href="tel:(561) 731-3155" runtime_url="tel:(561) 731-3155" style="color: rgb(181, 49, 44);" class="lh-1 font-size-16">(561) 731-3155</a></font></div>
                              <div><span style="" class="font-size-14 lh-1"><br/></span></div>
                              <div><a href="mailto:<?php  echo get_theme_mod('contact_email');?>" runtime_url="mailto:<?php  echo get_theme_mod('contact_email');?>" style="color: rgb(46, 176, 246);"><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_email');?></font></a></div>
                              <div><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><br/></font></div>
                              <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('office_hours');?></font></div>
                              <div><br></br></div>
                              <div><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_address1');?></span></div>
                              <div>
                                 <span style="" class="font-size-14 lh-1">
                                    <span style="color: rgb(0, 0, 0);">
                                    </span>
                                    <span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_address2');?></span><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><br/></span>
                                 </span>
                              </div>
                              <div>
                                 </div>
                           </div>
                        </div>
                     </div>
                  <div class="u_1932266037 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1932266037" style="text-align: left;" data-uialign="left">
                     <h4 class="m-size-13 m-text-align-center size-18 text-align-left"><span class="font-size-18 m-font-size-13" style="color: rgb(181, 49, 44); display: initial;">Our Other Services</span></h4>
                  </div>
                  <nav class="u_1382060198 effect-bottom2 other_services main-navigation unifiednav dmLinksMenu" >
                      <ul class="unifiednav__container  " data-auto="navigation-pages">
						<?php 
							$args = array(  
										'post_type' 		=> 'services',
										'post_status' 		=> 'publish',
										'posts_per_page' 	=> 100, 
										'orderby' 			=> 'title', 
										'order' 			=> 'time'	
									);
									$service = new WP_Query($args);	
											
									if($service->have_posts()){
										while($service->have_posts()){
											$service->the_post();
											$post_id= $post->ID;
											
								?>

                        <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="<?php echo the_permalink($post->ID); ?>" class="unifiednav__item    " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                           Skilled Nursing & Therapy Services at Home
                           " data-auto="page-text-style"><?php echo get_the_title(); ?><span class="icon icon-angle-down"></span> 
                           </span> 
                           </a> 
                        </li>
						<?php }
								}
								?>
                     </ul>
                  </nav>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div> 
</div> 
<?php
//get_sidebar();
get_footer();
?>
<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
<script src="<?php echo get_stylesheet_directory_uri()?>/js/jquery.validate.js"></script>  
<script>
	$(document).ready(function(){
		$("#commentForm").validate();
		$('body').on('submit','#commentForm',function(e){
			var base_url = "<?php echo home_url();?>";
			var formData= $('#commentForm').serialize();
			console.log(formData);
			e.preventDefault();
			 jQuery.ajax({ 
				type:"post",
				dataType : 'json',
				url: base_url+"/wp-admin/admin-ajax.php",
				data: formData,
				success:function(response){ 
					$(".spinner-border").hide(); 
					if(response.status==true){
						$(".dmform-error").hide();
						$(".dmform-success").show();
						$("#commentForm")[0].reset();
					}else if(response.status=='recaptcha_error' ){	
						$(".dmform-error").show();
						$(".dmform-error").html(response.msg);
					}else{
						$(".dmform-error").show();						
						$("#commentForm")[0].reset();
						$(".dmform-error").html(response.msg);
						grecaptcha.reset();
					}
					
				},
				beforeSend:function(response){
					$(".spinner-border").show();
				}						
			});
		});	
	});
</script> 