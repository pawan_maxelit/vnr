<?php
/**
 * Template Name: Caregiver services
 * Template Post Type: services
 * The template for displaying Home page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>
<!-- RT CSS Include d-css-runtime-desktop-one-package-new-->
<link rel="preload" onload="loadCSS(this)" type="text/css" href="<?php echo get_stylesheet_directory_uri()?>/mnlt/production/1917/_dm/s/rt/dist/css/d-css-runtime-desktop-one-package-new.min.css" />
<!-- End of RT CSS Include -->
<link rel="preload" onload="loadCSS(this)" href="<?php echo get_stylesheet_directory_uri()?>/widget_css/production_1917/004b5bdd245110b6c6336267aa0e53b8.css" id="widgetCSS" />
<!-- Support `img` size attributes -->
<style>img[width][height] {height: auto;}</style>
<!-- This is populated in Ajax navigation -->
<style id="pageAdditionalWidgetsCss" type="text/css">
</style>
<!-- Site CSS -->
<link type="text/css" rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/51716a39/files/51716a39_1.min.css" id="siteGlobalCss" />
<style id="customWidgetStyle" type="text/css">
.u_1154840544.dmNewParagraph{
    transition: none 0s ease 0s !important;
    text-align: left !important;
    display: inline-block !important;
    margin: -20px !important;
	
}
.u_1783616978 a.unifiednav__item {
font-size: 12px !important;
}

*#dm *.dmBody nav.u_1783616978
{
	padding-top:0px !important;
	padding-left:0px !important;
	padding-bottom:0px !important;
	padding-right:0px !important;
	float:none !important;
	top:0px !important;
	left:0px !important;
	width:225.594px !important;
	position:relative !important;
	height:auto !important;
	max-width:100% !important;
	justify-content:flex-start !important;
	align-items:stretch !important;
	min-width:0 !important;
	text-align:left !important;
	margin-right:auto !important;
	margin-left:0 !important;
	margin-top:0px !important;
	margin-bottom:0px !important;
}
.large-2{     
	position: relative !important;
	width: 16.66667% !important;
}
.u_1972778160.dmRespCol.large-3{
	    padding-left: 1.5% !important;
}
.u_1972778160.dmRespCol.large-9{
	padding-right: .75% !important;
}
    .widget-85b0db .text, .widget-85b0db .image{
    display:inline;
}
.widget-85b0db .image{
    max-width:100%;
    width:200px;
}
.widget-85b0db .left{
    clear:left;
    float:left;
    margin-right: 10px;
}
.widget-85b0db .right{
    clear:right;
    float:right;
    margin-left: 10px;
}
.widget-85b0db .spacer-left{
    float:left;
}
.widget-85b0db .spacer-right{
    float:right;
}
#dm .widget-85b0db .wrapper, #dm .widget-85b0db .rteBlock {
    text-align: justify;
}
#dm .widget-85b0db ul, #dm .widget-85b0db ol {
    padding: 0;
    list-style-position: inside;
}
</style>
<style id="innerPagesStyle" type="text/css">
</style>
<style id="additionalGlobalCss" type="text/css">
</style>
<style id="pagestyle" type="text/css">
    *#dm *.dmBody section.u_1897767439{
background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-2880w.jpg) !important;
background-color:rgba(181,49,44,1) !important;
background-repeat:no-repeat !important;
background-size:cover !important}
*#dm *.dmBody *.u_1897767439:before{
opacity:0.5 !important;
background-color:rgb(255,255,255) !important}
*#dm *.dmBody *.u_1897767439.before{
opacity:0.5 !important;
background-color:rgb(255,255,255) !important}
*#dm *.dmBody *.u_1897767439>.bgExtraLayerOverlay{
opacity:0.5 !important;
background-color:rgb(255,255,255) !important}
*#dm *.dmBody section.u_1897767439:before{
background-color:rgba(181,49,44,1) !important;
opacity:0.92 !important}
*#dm *.dmBody section.u_1897767439.before{
background-color:rgba(181,49,44,1) !important;
opacity:0.92 !important}
*#dm *.dmBody section.u_1897767439>.bgExtraLayerOverlay{
background-color:rgba(181,49,44,1) !important;
opacity:0.92 !important}
*#dm *.dmBody div.dmform-error{
color:rgba(102,102,102,1) !important}
*#dm *.dmBody div.dmform-error .rteBlock{
color:rgba(102,102,102,1) !important}
div.u_1194927944{
background-color:rgba(0,0,0,0) !important}
*.u_1858495681{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1760164592{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1804392154 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1804392154{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
*.u_1049091114{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1969843266{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1184770522 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1184770522{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
*.u_1226347711{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1694173537{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1995579479 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1995579479{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
*.u_1053693692{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1772721273{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1788285898 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1788285898{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
div.u_1117060476{
background-color:rgba(0,0,0,0) !important}
div.u_1668271240 input:not([type="submit"]){
border-style:solid !important;
border-width:0 !important;
background-color:rgba(227,227,227,1) !important}
div.u_1668271240 textarea{
border-style:solid !important;
border-width:0 !important;
background-color:rgba(227,227,227,1) !important}
div.u_1668271240 select{
border-style:solid !important;
border-width:0 !important;
background-color:rgba(227,227,227,1) !important}
*#dm *.dmBody *.u_1668271240 .dmformsubmit{
float:LEFT !important}
div.u_1668271240 .dmform-success{
color:rgba(102,102,102,1) !important}
div.u_1668271240 .dmform-success .rteBlock{
color:rgba(102,102,102,1) !important}
div.u_1668271240 .dmwidget-title{
color:rgba(102,102,102,1) !important}
div.u_1668271240 .dmforminput label:not(.for-checkable):not(.custom-contact-checkable){
color:rgba(102,102,102,1) !important}
div.u_1668271240 .m-recaptcha{
color:rgba(102,102,102,1) !important}
*#dm *.dmBody *.u_1113016322 .wrapper{
text-align:LEFT !important}
*#dm *.dmBody *.u_1113016322 .rteBlock{
text-align:LEFT !important}
*.u_1307311406{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1699642385{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1308725135 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1308725135{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
*.u_1624102420{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1692328791{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1618714878 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1618714878{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
*.u_1342672865{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1202457526{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1746248514 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1746248514{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
*.u_1919969033{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1198151991{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1140287132 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1140287132{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
*.u_1277129686{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1407934513{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1008745840 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1008745840{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
*.u_1664620184{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:0 !important;
padding-left:4.53125px !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*.u_1381633090{
background-image:none !important;
background-color:rgba(0,0,0,0) !important;
background-position:0 0 !important;
background-size:auto !important;
background-repeat:repeat !important;
padding-top:0 !important;
padding-bottom:0 !important;
padding-right:4.53125px !important;
padding-left:0 !important;
margin:0 !important;
text-align:start !important;
color:rgb(0,0,0) !important}
*#dm *.dmBody div.u_1141729247 *.svg{
color:rgba(181,49,44,1) !important;
fill:rgba(181,49,44,1) !important;
width:50% !important}
*#dm *.dmBody div.u_1141729247{
border-radius:50% !important;
-moz-border-radius:50% !important;
border-color:rgba(181,49,44,1) !important;
border-width:0 !important;
-webkit-border-radius:50% !important;
border-style:solid !important;
background-color:rgba(184,184,184,1) !important}
*#dm *.dmBody div.u_1952797172{
background-color:rgba(0,0,0,0) !important}
*#dm *.dmBody div.u_1827529567{
color:rgba(0,0,0,1) !important}
*#dm *.dmBody div.u_1578434203{
background-color:rgba(255,255,255,1) !important}
*#dm *.dmBody nav.u_1569154096{
color:black !important}
*#dm *.dmBody nav.u_1569154096.main-navigation.unifiednav .unifiednav__container:not([data-depth])>.unifiednav__item-wrap>.unifiednav__item{
color:rgba(181,49,44,1) !important}
*#dm *.dmBody *.u_1569154096.main-navigation.unifiednav:not([data-nav-structure='VERTICAL']) .unifiednav__container:not([data-depth])>.unifiednav__item-wrap:not(:last-child)::before{
font-size:11px !important}
*#dm *.dmBody *.u_1569154096.main-navigation.unifiednav:not([data-nav-structure='VERTICAL']) .unifiednav__container:not([data-depth])>.unifiednav__item-wrap:not(:last-child)::after{
font-size:11px !important}
*#dm *.dmBody div.u_1284076404{
background-color:rgba(0,0,0,0) !important}
</style>
<style id="pagestyleDevice" type="text/css">
    *#dm *.dmBody section.u_1897767439{
background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-1920w.jpg) !important;
background-color:rgba(181,49,44,1) !important;
background-repeat:no-repeat !important;
background-size:cover !important}@media all and (min-width:1920px), all and (-webkit-min-device-pixel-ratio: 1.5), all and (min--moz-device-pixel-ratio: 1.5), all and (min-device-pixel-ratio: 1.5) {
*#dm *.dmBody section.u_1897767439{
background-image:url(md/unsplash/dms3rep/multi/opt/photo-1496938461470-aaa34930e2d7-2880w.jpg) !important;
background-color:rgba(181,49,44,1) !important;
background-repeat:no-repeat !important;
background-size:cover !important}
}

*#dm *.dmBody section.u_1897767439
{
	padding-top:13.5px !important;
	padding-left:40px !important;
	padding-bottom:13.5px !important;
	float:none !important;
	top:0 !important;
	max-width:100% !important;
	left:0 !important;
	width:auto !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	background-attachment:initial !important;
	background-position:0 50% !important;
}
*#dm *.dmBody nav.u_1179213709
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:12px !important;
	padding-left:15px !important;
	padding-bottom:12px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:415px !important;
	margin-top:-8px !important;
	justify-content:flex-end !important;
	align-items:stretch !important;
	margin-bottom:0 !important;
	padding-right:15px !important;
	min-width:25px !important;
	text-align:start !important;
}

div.u_1194927944
{
	padding-top:36.5px !important;
	padding-left:40px !important;
	padding-bottom:36.5px !important;
	float:none !important;
	top:auto !important;
	max-width:none !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
}
div.u_1194927944
{
	padding-top:36.5px !important;
	padding-left:40px !important;
	padding-bottom:36.5px !important;
	float:none !important;
	top:auto !important;
	max-width:none !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
}
*#dm *.dmBody div.u_1952797172
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1627594036
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1627594036
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1463623757
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1804392154
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1952797172
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1627594036
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1627594036
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1463623757
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1804392154
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1578434203
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1254868365
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1254868365
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1421252513
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1184770522
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1578434203
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1254868365
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1254868365
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1421252513
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1184770522
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1469827934
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1768594451
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1768594451
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1681643897
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1995579479
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1469827934
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1768594451
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1768594451
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1681643897
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1995579479
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1975153444
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1402562688
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1402562688
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1368210831
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1788285898
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1975153444
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1402562688
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1402562688
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1368210831
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1788285898
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
div.u_1117060476
{
	padding-top:36.5px ;
	padding-left:40px;
	padding-bottom:36.5px;
	float:none !important;
	top:auto !important;
	max-width:none !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	padding-right:40px ;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
}
div.u_1668271240 .dmwidget-title
{
	font-size:18px !important;
}
div.u_1668271240
{
	padding-top:0 !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:16px !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
}
div.u_1134821834
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 13px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:13px !important;
	max-width:166px !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
div.u_1113016322 .wrapper
{
	font-size:14px !important;
}
div.u_1113016322 .rteBlock
{
	font-size:14px !important;
}
div.u_1113016322 .image
{
	width:251px !important;
}
*#dm *.dmBody div.u_1113016322 .image
{
	width:298px !important;
}
*#dm *.dmBody div.u_1489594340
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1500403144
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1500403144
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1726319794
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1978950052
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:583.455px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:0 !important;
}
*#dm *.dmBody div.u_1308725135
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1066666359
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1164255197
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1164255197
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1704889787
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1618714878
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1183833395
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1818703641
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1818703641
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1915421570
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1746248514
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1138374214
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1535385292
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1535385292
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1970228934
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1140287132
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1096712365
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1259714783
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1259714783
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1401424956
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1008745840
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1566607482
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1533141419
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1533141419
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1832482087
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1141729247
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
div.u_1117060476
{
	padding-top:36.5px !important;
	padding-left:40px !important;
	padding-bottom:36.5px !important;
	float:none !important;
	top:auto !important;
	max-width:none !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	padding-right:40px !important;
	min-width:0 !important;
	text-align:center !important;
	height:auto !important;
}
div.u_1668271240 .dmwidget-title
{
	font-size:18px !important;
}
div.u_1668271240
{
	padding-top:0 !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:16px !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
}
div.u_1134821834
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 13px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:13px !important;
	max-width:166px !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
div.u_1113016322 .wrapper
{
	font-size:14px !important;
}
div.u_1113016322 .rteBlock
{
	font-size:14px !important;
}
div.u_1113016322 .image
{
	width:251px !important;
}
*#dm *.dmBody div.u_1113016322 .image
{
	width:298px !important;
}
*#dm *.dmBody div.u_1489594340
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1500403144
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1500403144
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1726319794
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1978950052
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:583.455px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:0 !important;
}
*#dm *.dmBody div.u_1308725135
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1066666359
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1164255197
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1164255197
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1704889787
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1618714878
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1183833395
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1818703641
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1818703641
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1915421570
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1746248514
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1138374214
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1535385292
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1535385292
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1970228934
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1140287132
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1096712365
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1259714783
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1259714783
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1401424956
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1008745840
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1566607482
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
}
*#dm *.dmBody h3.u_1533141419
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:300px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
}
*#dm *.dmBody h4.u_1533141419
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:calc(100% - 0px) !important;
	margin-top:10px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1832482087
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:100% !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1141729247
{
	width:calc(100% - 24px) !important;
	height:61.9965px !important;
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	position:relative !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	max-width:63.9931px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
	margin-right:auto !important;
	margin-left:auto !important;
	margin-top:20px !important;
	margin-bottom:10px !important;
}
*#dm *.dmBody div.u_1134821834
{
	display:block !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:166px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	margin-right:auto !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:8px !important;
	padding-right:0 !important;
	min-width:0 !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1827529567
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:207px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1827529567
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:207px !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:left !important;
}
*#dm *.dmBody div.u_1668271240
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	margin-right:0 !important;
	margin-left:0 !important;
	max-width:100% !important;
	margin-top:26px !important;
	margin-bottom:0 !important;
	padding-right:0 !important;
	min-width:25px !important;
	text-align:center !important;
}
*#dm *.dmBody div.u_1194927944
{
	margin-left:0 !important;
	padding-top:0 !important;
	padding-left:40px !important;
	padding-bottom:36.5px !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
	margin-right:0 !important;
	padding-right:40px !important;
	width:auto !important;
}
*#dm *.dmBody nav.u_1569154096.main-navigation.unifiednav .unifiednav__container:not([data-depth])>.unifiednav__item-wrap>.unifiednav__item
{
	font-size:12px !important;
}
*#dm *.dmBody nav.u_1569154096.main-navigation.unifiednav .unifiednav__container:not([data-depth])>.unifiednav__item-wrap>.unifiednav__item
{
	font-size:12px !important;
}
*#dm *.dmBody div.u_1858081827
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:225.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody div.u_1858081827
{
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:225.594px !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0 !important;
	padding-bottom:2px !important;
	max-width:100% !important;
	padding-right:0 !important;
	min-width:0 !important;
	display:block !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody nav.u_1569154096
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:225.594px !important;
	position:relative !important;
	height:auto !important;
	max-width:100% !important;
	justify-content:flex-start !important;
	align-items:stretch !important;
	min-width:0 !important;
	text-align:start !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody nav.u_1569154096
{
	padding-top:0 !important;
	padding-left:0 !important;
	padding-bottom:0 !important;
	padding-right:0 !important;
	float:none !important;
	top:0 !important;
	left:0 !important;
	width:225.594px !important;
	position:relative !important;
	height:auto !important;
	max-width:100% !important;
	justify-content:flex-start !important;
	align-items:stretch !important;
	min-width:0 !important;
	text-align:start !important;
	margin-right:0 !important;
	margin-left:0 !important;
	margin-top:0 !important;
	margin-bottom:0 !important;
}
*#dm *.dmBody a.u_1817066200
{
	display:block !important;
	float:none !important;
	top:0px !important;
	left:0 !important;
	width:280px !important;
	position:relative !important;
	height:43px !important;
	padding-top:0px !important;
	padding-left:0px !important;
	padding-bottom:0px !important;
	margin-right:auto !important;
	margin-left:auto !important;
	max-width:100% !important;
	margin-top:0px !important;
	margin-bottom:0px !important;
	padding-right:0px !important;
	min-width:0 !important;
	text-align:center !important;
}
*#dm *.dmBody div.u_1870509386
{
	float:none !important;
	top:0px !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0px !important;
	padding-bottom:2px !important;
	margin-right:auto !important;
	margin-left:auto !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:0px !important;
	padding-right:0px !important;
	min-width:25px !important;
	display:block !important;
}
*#dm *.dmBody div.u_1870509386
{
	float:none !important;
	top:0px !important;
	left:0 !important;
	width:calc(100% - 0px) !important;
	position:relative !important;
	height:auto !important;
	padding-top:2px !important;
	padding-left:0px !important;
	padding-bottom:2px !important;
	margin-right:auto !important;
	margin-left:auto !important;
	max-width:100% !important;
	margin-top:8px !important;
	margin-bottom:0px !important;
	padding-right:0px !important;
	min-width:25px !important;
	display:block !important;
}
</style>
<!--[if IE 7]><style>.fw-head.fw-logo img{max-width: 290px;}.dm_header .logo-div img{max-width: 290px;}</style><![endif]-->
<!--[if IE 8]><style>.fw-head .fw-logo img{max-width: 290px;}.dm_header .logo-div img{max-width: 290px;}*#dm div.dmHeader{_height:90px;min-height:0px;}</style><![endif]-->
    <style id="globalFontSizeStyle" type="text/css">
        .font-size-26, .size-26, .size-26 > font { font-size: 26px !important; }.font-size-36, .size-36, .size-36 > font { font-size: 36px !important; }.font-size-21, .size-21, .size-21 > font { font-size: 21px !important; }.font-size-30, .size-30, .size-30 > font { font-size: 30px !important; }.font-size-21, .size-21, .size-21 > font { font-size: 21px !important; }.font-size-30, .size-30, .size-30 > font { font-size: 30px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-20, .size-20, .size-20 > font { font-size: 20px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-20, .size-20, .size-20 > font { font-size: 20px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-16, .size-16, .size-16 > font { font-size: 16px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-14, .size-14, .size-14 > font { font-size: 14px !important; }.font-size-13, .size-13, .size-13 > font { font-size: 13px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-18, .size-18, .size-18 > font { font-size: 18px !important; }.font-size-13, .size-13, .size-13 > font { font-size: 13px !important; }
    </style>
	
	<link rel="preload" as="style" importance="low" onload="loadCSS(this)" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/css/media.css' ?>" />
 <div id="desktopBodyBox">
<div id="iscrollBody">
<div id="site_content">
   <div class="dmRespRow dmRespRowStable dmRespRowNoPadding dmPageTitleRow dmInnerBarRow">
      <div class="dmRespColsWrapper">
         <div class="large-12 dmRespCol">
            <div id="innerBar" class="innerBar lineInnerBar dmDisplay_None">
               <div class="titleLine display_None">
                  <hr/>
               </div>
               <!-- Page title is hidden in css for new responsive sites. It is left here only so we don't break old sites. Don't copy it to new layouts --> 
               <div id="pageTitleText">
                  <div class="innerPageTitle">Private Duty Caregiver Services</div>
               </div>
               <div class="titleLine display_None">
                  <hr/>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div dmwrapped="true" id="1275460582" class="dmBody u_dmStyle_template_caregiver-services">
      <div id="allWrapper" class="allWrapper">
         <!-- navigation placeholders --> 
         <div id="dm_content" class="dmContent">
            <div dm:templateorder="112" class="dmDefaultRespTmpl mainBorder" id="1479330477">
               <div class="innerPageTmplBox dmDefaultPage dmAboutListPage dmRespRowsWrapper" id="1736103396">
                  <section class="u_1897767439 dmRespRow dmHomeSection1 dmHomeSectionNoBtn dmSectionNoParallax hasBackgroundOverlay" id="1897767439">
                     <div class="dmRespColsWrapper template-heading" id="1010790795">
                        <div class="dmRespCol large-12 medium-12 small-12" id="1139052171">
                           <h3 class="u_1453001980 dmNewParagraph" id="1453001980" data-element-type="paragraph" data-uialign="center" style="display: block; transition: none 0s ease 0s;">
                              <div><font color="#ffffff"><b style="" class="font--36 lh-1">Private Duty Caregiver Services</b></font></div>
                           </h3>
                        </div>
                     </div>
                  </section>
                  <div class="u_1117060476 dmRespRow padding-0" style="text-align: center;" id="1117060476">
                     <div class="dmRespColsWrapper template_service" id="1814335476">
                        <div class="u_1730517860 dmRespCol small-12 large-9 medium-9" id="1730517860">
                           <div class="widget-85b0db u_1113016322 dmCustomWidget" data-lazy-load="" id="1113016322" dmle_extension="custom_extension" data-element-type="custom_extension" icon="false" surround="false" data-widget-id="85b0db3860664a28b3031e6cd70abf8c" data-widget-version="37" data-widget-config="eyJkaXJlY3Rpb24iOiJyaWdodCIsImltYWdlIjoiaHR0cHM6Ly9pcnAtY2RuLm11bHRpc2NyZWVuc2l0ZS5jb20vNTE3MTZhMzkvZG1zM3JlcC9tdWx0aS9tZWRpY2F0aW9uLmpwZyIsInRleHQiOiI8cCBjbGFzcz1cInJ0ZUJsb2NrXCI+PHNwYW4gc3R5bGU9XCJjb2xvcjpyZ2IoMTgxLCA0OSwgNDQpXCI+VkVSSVRBUzwvc3Bhbj48c3BhbiBzdHlsZT1cImNvbG9yOnJnYigwLCAwLCAwKVwiPsKgaGFzIHRyYWluZWQgYW5kIGV4cGVyaWVuY2VkIENhcmVnaXZlcnMgdGhhdCB3aWxsIHByb3ZpZGUgbm9uLW1lZGljYWwgc2VydmljZXMgdG8gdGhlIGVsZGVybHkuIMKgT3VyIENhcmVnaXZlcnMgaGVscCB0aGUgZWxkZXJseSB3aG8gc3VmZmVyIGZyb20gYSB2YXJpZXR5IG9mIGNvbmRpdGlvbnMgdGhhdCBwcmV2ZW50IHRoZW0gZnJvbSBwZXJmb3JtaW5nIHRoZWlyIGFjdGl2aXRpZXMgb2YgZGFpbHkgbGl2aW5nIGJ5IHRoZW1zZWx2ZXMuIMKgSW4gbW9zdCBjYXNlcywgdGhlIHByZXNlbmNlIG9mIDwvc3Bhbj48c3BhbiBzdHlsZT1cImNvbG9yOnJnYigxODEsIDQ5LCA0NClcIj5WRVJJVEFTPC9zcGFuPjxzcGFuIHN0eWxlPVwiY29sb3I6cmdiKDAsIDAsIDApXCI+IENhcmVnaXZlcnMgaXMgd2hhdCBtYWtlcyBpdCBwb3NzaWJsZSBmb3IgdGhlIGVsZGVybHkgdG8gc3RheSBhdCB0aGVpciBvd24gaG9tZXMgaW5zdGVhZCBvZiBtb3ZpbmcgdG8gYSBudXJzaW5nIGhvbWUgb3IgYXNzaXN0ZWQgbGl2aW5nIGZhY2lsaXR5Ljwvc3Bhbj48L3A+PGJyPjxwIGNsYXNzPVwicnRlQmxvY2tcIj48c3BhbiBzdHlsZT1cImNvbG9yOnJnYigwLCAwLCAwKVwiPjxzdHJvbmc+V2hhdCBBcmUgQWN0aXZpdGllcyBPZiBEYWlseSBMaXZpbmcgKEFETHMpIGFuZCBJbnN0cnVtZW50YWwgQWN0aXZpdGllcyBPZiBEYWlseSBMaXZpbmcgKElBRExzKT88L3N0cm9uZz48L3NwYW4+PC9wPjxicj48cCBjbGFzcz1cInJ0ZUJsb2NrXCI+PHNwYW4gc3R5bGU9XCJjb2xvcjpyZ2IoMCwgMCwgMClcIj5UaGUgbGV2ZWwgb2YgY2FyZSB0aGF0IGFuIGVsZGVybHkgbmVlZHMgZGVwZW5kcyBvbiBoaXMvaGVyIGFiaWxpdHkgdG8gcGVyZm9ybSBBY3Rpdml0aWVzIG9mIERhaWx5IExpdmluZyAoQURMKSBhbmQgSW5zdHJ1bWVudGFsIEFjdGl2aXRpZXMgb2YgRGFpbHkgTGl2aW5nIChJQURMKS4gPHN0cm9uZz7CoEFjdGl2aXRpZXMgb2YgRGFpbHkgTGl2aW5nIChBRExzKcKgPC9zdHJvbmc+YXJlIGRheSB0byBkYXkgYWN0aXZpdGllcyB0aGF0IGFyZSBmdW5kYW1lbnRhbCBmb3IgYmFzaWMgZnVuY3Rpb25hbCBsaXZpbmcuIMKgSW5hYmlsaXR5IHRvIHBlcmZvcm0gQURMcyBtYXkgaW5kaWNhdGUgdGhlIG5lZWQgZm9yIHNvbWUgbGV2ZWwgb2YgY2FyZWdpdmVyIHNlcnZpY2VzIGF0IGhvbWUuIDxzdHJvbmc+wqBJbnN0cnVtZW50YWwgQWN0aXZpdGllcyBvZiBEYWlseSBMaXZpbmcgKElBRExzKTwvc3Ryb25nPsKgYXJlIHRob3NlIGFjdGl2aXRpZXMgdGhhdCBhcmUgbm90IG5lY2Vzc2FyeSBmb3IgZnVuY3Rpb25hbCBsaXZpbmcgYnV0IGFsbG93cyB0aGUgZWxkZXJseSB0byBsaXZlIGluZGVwZW5kZW50bHkuIEFuIGVsZGVybHkgcGVyc29u4oCZcyBhYmlsaXR5IG9yIGluYWJpbGl0eSB0byBwZXJmb3JtIElBRExzIGFyZSB1c2VkIHRvIGV2YWx1YXRlIHRoZWlyIGNvZ25pdGl2ZSBmdW5jdGlvbiBhbmQgaGVscCBkZXRlcm1pbmUgdGhlIGxldmVsIG9mIGFzc2lzdGFuY2UgdGhleSBuZWVkLjwvc3Bhbj48L3A+IiwiYWx0IjoiIn0=">
                              <div class="wrapper">
                                 <div class="spacer spacer-right"></div>
                                 <img alt="" class="image right" src="<?php echo get_the_post_thumbnail_url();?>" onerror="handleImageLoadError(this)"/> 
                                 <div class="text">
                                    <?php echo get_the_content(); ?>
                                 </div>
                                 <div style="clear:both"></div>
                              </div>
                           </div>
                           <div class="dmRespRow u_1284076404" id="1284076404">
                              <div class="dmRespColsWrapper" id="1692351140">
                                 <div class="dmRespCol large-6 medium-6 small-12" id="1612176311">
                                    <div class="u_1870509386 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1870509386" style="transition: opacity 1s ease-in-out 0s; text-align: left;" data-uialign="center">
                                       <h4 class="text-align-center m-size-21 size-30 mt-0"><span class="m-font-size-21 font-size-30" style="display: initial; color: rgb(181, 49, 44);"><?php the_field('button_title'); ?></span></h4>
                                    </div>
                                    <a data-display-type="block" class="u_1817066200 align-center dmButtonLink dmWidget dmWwr default dmOnlyButton dmDefaultGradient" file="false" href="<?php the_field('button_link'); ?>" data-element-type="dButtonLinkId" id="1817066200" dm_dont_rewrite_url="false"> <span class="iconBg" id="1148030681"> <span class="icon hasFontIcon icon-star" id="1065081968"></span> 
                                    </span> 
                                    <span class="text" id="1196871185"><?php echo the_field('button_text'); ?></span> 
                                    </a> 
                                 </div>
                                 <div class="dmRespCol large-6 medium-6 small-12" id="1887532610">
                                    <div class="imageWidget align-center u_1904117514" editablewidget="true" data-element-type="image" data-widget-type="image" id="1904117514"> <a id="1876364005"><img src="<?php echo the_field('image'); ?>" alt="" id="1243151832" class="" data-dm-image-path="https://irp-cdn.multiscreensite.com/md/unsplash/dms3rep/multi/photo-1567067974934-75a3e4534c14.jpg" onerror="handleImageLoadError(this)"/></a> </div>
                                 </div>
                              </div>
                           </div>
                           <div class="dmNewParagraph u_1978950052" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1978950052" style="transition: opacity 1s ease-in-out 0s; text-align: left;" data-uialign="center">
                              <p class="size-18 m-size-16 m-text-align-center"><span class="font-size-18 m-font-size-16" style="display: initial; font-weight: bold; color: rgb(181, 49, 44);" m-font-size-set="true">Help In Activities Of Daily Living (ADLs)</span></p>
                           </div>
						   <?php $rows = get_field('category'); //print_r($rows); 
									if( have_rows('category') ):
										while ( have_rows('category') ) : the_row();
							?>
                           <div class="dmRespRow u_1489594340" id="1489594340">
                              <div class="dmRespColsWrapper" id="1192480823">
                                 <div class="u_1699642385 dmRespCol small-12 large-2 medium-2" id="1699642385">
                                    <div class="u_1308725135 graphicWidget" editablewidget="true" data-widget-type="graphic" id="1308725135" data-element-type="graphic" data-layout="graphic-style-2">
                                       <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 100 100" id="1031716174" class="svg u_1031716174" data-icon-name="general_bathtub_old_fashioned">
                                          <path d="M54.55,79.51h-9.1c-10.39,0-19.9-5.36-21.21-11.95l-2.19-11a1.12,1.12,0,1,1,2.2-.44l2.19,11c1.09,5.5,9.79,10.14,19,10.14h9.1c9.21,0,17.92-4.64,19-10.14l2.19-11a1.12,1.12,0,0,1,2.2.44l-2.19,11C74.45,74.16,64.94,79.51,54.55,79.51Z"></path>
                                          <path d="M74.83,57.43H25.17c-3,0-5.29-1.73-5.29-3.94s2.32-3.94,5.29-3.94H74.83c3,0,5.29,1.73,5.29,3.94S77.8,57.43,74.83,57.43ZM25.17,51.79c-1.74,0-3,.9-3,1.7s1.3,1.7,3,1.7H74.83c1.74,0,3-.9,3-1.7s-1.3-1.7-3-1.7Z"></path>
                                          <path d="M31,85.86h0a2,2,0,0,1-.69-.12l-.83-.31A2.05,2.05,0,0,1,28.23,83l1.89-7.15a1.87,1.87,0,0,1,1.8-1.4,1.91,1.91,0,0,1,.66.12l2.43.9a1.87,1.87,0,0,1,1,2.58l-3.2,6.66A2.06,2.06,0,0,1,31,85.86Zm-.57-2.47.48.18,2.95-6.14-1.69-.62Z"></path>
                                          <path d="M69,85.86a2.06,2.06,0,0,1-1.85-1.15L63.93,78a1.86,1.86,0,0,1,1-2.58l2.43-.9a1.91,1.91,0,0,1,.66-.12,1.87,1.87,0,0,1,1.8,1.4L71.77,83a2.05,2.05,0,0,1-1.26,2.43l-.83.31A2,2,0,0,1,69,85.86Zm-2.86-8.43,2.95,6.14.48-.18-1.74-6.58Z"></path>
                                          <path d="M67.9,36.3H51.3a1.12,1.12,0,0,1-1.12-1.12V33A9.42,9.42,0,0,1,69,33v2.17A1.12,1.12,0,0,1,67.9,36.3ZM52.42,34.06H66.78V33a7.18,7.18,0,0,0-14.35,0Z"></path>
                                          <path d="M76.19,51.79a1.12,1.12,0,0,1-1.12-1.12V23.56a7.18,7.18,0,0,0-14.35,0v1.16a1.12,1.12,0,1,1-2.24,0V23.56a9.42,9.42,0,0,1,18.83,0V50.67A1.12,1.12,0,0,1,76.19,51.79Z"></path>
                                       </svg>
                                    </div>
                                 </div>
                                 <div class="u_1307311406 dmRespCol small-12 large-10 medium-10" id="1307311406">
                                    <h5 class="u_1500403144 dmNewParagraph" id="1500403144" data-uialign="right" style="display: block;" data-element-type="paragraph">
                                       <div><span style="" class="font-size-20 lh-1"><b><span style="font-weight: 700;" class="lh-1 font-size-16"><font style="color: rgb(0, 0, 0);"><?php echo  the_sub_field('service_heading'); ?></font></span></b></span></div>
                                    </h5>
                                    <div class="u_1726319794 dmNewParagraph" data-dmtmpl="true" id="1726319794" data-uialign="right" style="transition: opacity 1s ease-in-out;">
                                       <div style=""><?php echo  the_sub_field('service_content'); ?></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
						   <?php
								endwhile;
								endif; 
							?>
                        </div>
                        <div class="u_1972778160 dmRespCol small-12 large-3 medium-3" id="1972778160">
                     <div class="u_1101791350 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1101791350" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px 0px; float: none; width: 166px; height: auto; position: relative;" data-uialign="right"><font style="color: rgb(102, 102, 102);" class="font-size-18 lh-1">Contact Us Today!</font></div>
                     <div class="u_1507611713 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1507611713" data-uialign="left" data-styletopreserve="{" background-image":""}"="" data-editor-state="closed"> 
                     <div><span style="" class="lh-1 font-size-16"><font style=""><span style=""><?php  echo get_theme_mod('contact_details');?></span></font> 
                        </span> 
                     </div>
                     <div>
                        <div>
                           <span style="" class="lh-1 font-size-16"><font style="color: rgb(181, 49, 44);"><span style=""><br/></span></font> 
                           </span> 
                           <div><font style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><a href="tel:<?php  echo get_theme_mod('contact_phone_no');?>" runtime_url="tel:(561) 731-3155" style="color: rgb(181, 49, 44);" class="lh-1 font-size-16"><?php  echo get_theme_mod('contact_phone_no');?></a></font></div>
                           <div><span style="" class="font-size-14 lh-1"><br/></span></div>
                           <div><a href="mailto:<?php  echo get_theme_mod('contact_email');?>" runtime_url="mailto:admin@veritasnursecare.com" style="color: rgb(46, 176, 246);"><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_email');?></font></a></div>
                           <div><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><br/></font></div>
                          <div><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><br/></font></div>
							  <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('office_hours');?></font></div>
							  <div><br></br></div>
                           <div><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_address1');?></span></div>
                           <div>
                              <span style="" class="font-size-14 lh-1">
                                 <span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_address2');?></span><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><br/></span>
                              </span>
                           </div>
                       </div>
                     </div>
                  </div>
                  <div class="u_1154840544 dmNewParagraph " data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1154840544" style="transition: none 0s ease 0s; text-align: left; display: block;" data-uialign="center">
                     <h4 class="m-size-13 size-18 m-text-align-center"><span class="font-size-18 m-font-size-13" style="color: rgb(181, 49, 44); display: initial;">Our Other Services</span></h4>
                  </div>
                  <nav class="u_1783616978 other_services effect-bottom2 main-navigation unifiednav dmLinksMenu">
                     <ul class="unifiednav__container  " data-auto="navigation-pages">
						<?php 
							$args = array(  
										'post_type' 		=> 'services',
										'post_status' 		=> 'publish',
										'posts_per_page' 	=> 100, 
										'orderby' 			=> 'title', 
										'order' 			=> 'time'	
									);
									$service = new WP_Query($args);	
											
									if($service->have_posts()){
										while($service->have_posts()){
											$service->the_post();
											$post_id= $post->ID;
											
								?>

                        <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="<?php echo the_permalink($post->ID); ?>" class="unifiednav__item    " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                           Skilled Nursing & Therapy Services at Home
                           " data-auto="page-text-style"><?php echo get_the_title(); ?><span class="icon icon-angle-down"></span> 
                           </span> 
                           </a> 
                        </li>
						<?php }
								}
								?>
                     </ul>
                  </nav>
               </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
   </div>
</div> 
<?php
//get_sidebar();
get_footer();