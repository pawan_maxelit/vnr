<?php
/*
* Template Name: Job opportunities
*/

get_header();
?>
<style>

</style> 
 <div id="site_content">
	<div class="dmRespRow dmRespRowStable dmRespRowNoPadding dmPageTitleRow dmInnerBarRow">
	   <div class="dmRespColsWrapper">
		  <div class="large-12 dmRespCol">
			 <div id="innerBar" class="innerBar lineInnerBar dmDisplay_None">
				<div class="titleLine display_None">
				   <hr/>
				</div>
				<!-- Page title is hidden in css for new responsive sites. It is left here only so we don't break old sites. Don't copy it to new layouts --> 
				<div id="pageTitleText">
				   <div class="innerPageTitle">Job Opportunities</div>
				</div>
				<div class="titleLine display_None">
				   <hr/>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
	<div dmwrapped="true" id="1275460582" class="dmBody u_dmStyle_template_job-opportunities">
	   <div id="allWrapper" class="allWrapper">
		  <!-- navigation placeholders --> 
		  <div id="dm_content" class="dmContent">
			 <div dm:templateorder="112" class="dmDefaultRespTmpl mainBorder" id="1479330477">
				<div class="innerPageTmplBox dmDefaultPage dmAboutListPage dmRespRowsWrapper" id="1736103396">
				   <section class="u_1897767439 dmRespRow dmHomeSection1 dmHomeSectionNoBtn dmSectionNoParallax hasBackgroundOverlay" id="1897767439">
					  <div class="dmRespColsWrapper" id="1010790795">
						 <div class="dmRespCol large-12 medium-12 small-12" id="1139052171">
							<h3 class="u_1453001980 dmNewParagraph" id="1453001980" data-element-type="paragraph" data-uialign="left" style="top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px auto 8px 0px; float: none; width: 960px; height: auto;">
							   <div style="text-align: center;"><font color="#ffffff"><b>JOB OPPORTUNITIES</b></font></div>
							</h3>
						 </div>
					  </div>
				   </section>
				   <div class="u_1422486861 dmRespRow" style="text-align: center;" id="1422486861">
					  <div class="dmRespColsWrapper" id="1640410774">
						 <div class="u_1084594921 dmRespCol small-12 large-9 medium-9" id="1084594921">
							<div class="dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1489246653" style="transition: opacity 1s ease-in-out 0s;" data-uialign="left"><?php echo get_the_content();?> 
							   <a href="mailto:HR@veritasnursecare.com" runtime_url="mailto:HR@veritasnursecare.com" style=""><font style="color: rgb(181, 49, 44);"><?php echo get_theme_mod('hr_email');?></font></a><font style="color: rgb(102, 102, 102);">.</font></font>
							</div>
							<div class="imageWidget align-center u_1097261032" editablewidget="true" data-element-type="image" data-widget-type="image" id="1097261032"> <a id="1852373220"><img src="<?php echo get_the_post_thumbnail_url();?>" alt="A nurse in blue scrubs helps an elderly man out of bed." id="1512807956" class="" data-dm-image-path="https://irp-cdn.multiscreensite.com/51716a39/dms3rep/multi/job2.jpg" title="A nurse in blue scrubs helps an elderly man out of bed." onerror="handleImageLoadError(this)"/></a> </div>
							<div class="u_1505275905 dmform default" preserve_css="true" data-element-type="dContactUsRespId" data-captcha-position="checkbox" id="1505275905" data-layout="layout-1" captcha="true" data-captcha-message="VGhpcyBzaXRlIGlzIHByb3RlY3RlZCBieSByZUNBUFRDSEEuIEdvb2dsZSdzIDxhIHR5cGU9InVybCIgZGF0YS1ydW50aW1lLXVybD0iaHR0cHM6Ly9wb2xpY2llcy5nb29nbGUuY29tL3ByaXZhY3kiIGhyZWY9Imh0dHBzOi8vcG9saWNpZXMuZ29vZ2xlLmNvbS9wcml2YWN5IiB0YXJnZXQ9Il9ibGFuayI+UHJpdmFjeSBQb2xpY3k8L2E+IGFuZCA8YSB0eXBlPSJ1cmwiIGRhdGEtcnVudGltZS11cmw9Imh0dHBzOi8vcG9saWNpZXMuZ29vZ2xlLmNvbS90ZXJtcyIgaHJlZj0iaHR0cHM6Ly9wb2xpY2llcy5nb29nbGUuY29tL3Rlcm1zIiB0YXJnZXQ9Il9ibGFuayI+VGVybXMgb2YgU2VydmljZTwvYT4gYXBwbHkuIA==">
							   <h3 class="dmform-title dmwidget-title" id="1943791538">Job Opportunities</h3>
							   <div class="dmform-wrapper" preserve_css="true" id="1540438115 job_form" captcha-lang="en">
								 <form class="cmxform" id="commentForm" method="get" action="">
									<input type="hidden" name="action" value="job_ajax" >
									 <div class="dmforminput required  small-12 dmRespDesignCol medium-12 large-12" id="1091241145"> <label for="dmform-0" id="1127067165">Name:</label> 
										<input type="text" class="" name="name" required/>
									 </div>
									 <div class="dmforminput required  small-12 dmRespDesignCol medium-12 large-12" id="1766385549"> 
									 <label for="dmform-1" id="1969739341">Email:</label> 
										<input type="email" class="email" name="email" id="1508160829"/>
									 </div>
									 <div class="dmforminput required  small-12 dmRespDesignCol medium-12 large-12" id="1876320445"> <label for="dmform-2" id="1083995350">Phone:</label> 
										<input type="tel" class="" name="phone" id="1168524067"/>
									 </div>
									 <div class="dmforminput large-12 medium-12 dmRespDesignCol small-12" id="1891306863"> <label for="dmform-3" id="1232776556">Message:</label> 
										<textarea name="message" id="1221373922"></textarea> 
										
									 </div>
									 <div class="dmforminput small-12 dmRespDesignCol required medium-12 large-12" id="1161565959"> <label for="dmform-4" id="1564016355">What job are your applying for?</label> 
										<input type="text" name="job_name" id="1660003474"/>
										<div class="g-recaptcha" data-sitekey="<?php echo GOOGLE_RECAPTCH_KEY; ?>"></div>
									 </div>
									
									 <div class="dmformsubmit dmWidget R" preserve_css="true" id="1553211185">
									 <input class="" name="submit" type="submit" value="Send Message" id="1233818972"/></div>
								
									<div class="spinner-border" role="status" style="display:none;">  
									</div>
								  </form>
							   </div>
							   <div class="dmform-success" style="display:none" preserve_css="true" id="1065913156">Thank you for contacting us.<br id="1224248818"/>We will get back to you as soon as possible.</div>
							   <div class="dmform-error" style="display:none" preserve_css="true" id="1656980343">Oops, there was an error sending your message.<br id="1741285276"/>Please try again later.</div>
							</div>
						 </div>
						 <div class="u_1371840841 dmRespCol small-12 large-3 medium-3" id="1371840841">
							<div class="u_1330770612 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1330770612" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 8px 0px; float: none; width: 166px; height: auto; position: relative;" data-uialign="right"><font style="color: rgb(102, 102, 102);" class="font-size-18 lh-1">Contact Us Today!</font></div>
							<div class="u_1020826578 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1020826578" data-uialign="left" data-styletopreserve="{" background-image":""}"="" data-editor-state="closed"> 
							<div><span style="font-size: 16px; background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_details');?></span><br/></div>
							<div>
							   <div>
								  <span style="" class="lh-1 font-size-16"><font style="color: rgb(181, 49, 44);"><span style=""><br/></span></font> 
								  </span> 
								  <div><font style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><a href="tel:(561) 731-3155" runtime_url="tel:(561) 731-3155" style="color: rgb(181, 49, 44);" class="lh-1 font-size-16"><?php  echo get_theme_mod('contact_phone_no');?></a></font></div>
								  <div><br/></div>
								  <div>For job opportunities contact:</div>
								  <div><font style="background-color: rgba(0, 0, 0, 0); color: rgb(181, 49, 44);"><a href="mailto:HR@veritasnursecare.com" runtime_url="mailto:HR@veritasnursecare.com" style="background-color: rgba(0, 0, 0, 0);"><?php echo get_theme_mod('hr_email');?></a>&nbsp;</font><font style="background-color: rgba(0, 0, 0, 0);">o</font><span style="background-color: rgba(0, 0, 0, 0);">r apply below.</span></div>
								  <div><br/></div>
								  <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('office_hours');?></font></div>
								  
								  <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><br/></font></div>
								  <div><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_address1');?></span></div>
								  <div>
									 <span style="" class="font-size-14 lh-1">
										<span style="color: rgb(0, 0, 0);">
										   <ul class="innerList defaultList">
											  <li><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_address2');?></span></li>
										   </ul>
										</span>
									 </span>
								  </div>
							   </div>  
							</div>
						 </div>
					  </div>
				   </div>
				</div>
			 </div>
		  </div>
	   </div>
	</div>
 </div>
</div>
<link rel="preload" href="<?php echo get_stylesheet_directory_uri()?>/css/51716a39_1.min.css" id="siteGlobalCss" as="style" importance="low" onload="loadCSS(this)" />  
<link rel="preload" href="<?php echo get_stylesheet_directory_uri()?>/css/jquery-validator.css" id="siteGlobalCss" as="style" importance="low" onload="loadCSS(this)" />
<link rel="preload" href="<?php echo get_stylesheet_directory_uri()?>/css/job.css" id="siteGlobalCss" as="style" importance="low" onload="loadCSS(this)" />
<link rel="preload" as="style" importance="low" onload="loadCSS(this)" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/css/media.css' ?>" />
           
<?php get_footer(); ?>
<style>
	.dmform-error {color:red!important;font-size: 24px!important;}
    .dmform-error {color:green!important;font-size: 24px!important;}
</style>
<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
<script src="<?php echo get_stylesheet_directory_uri()?>/js/jquery.validate.js"></script>  
<script>
	$(document).ready(function(){
		$("#commentForm").validate();
		$('body').on('submit','#commentForm',function(e){
			var base_url = "<?php echo home_url();?>";
			var formData= $('#commentForm').serialize();
			console.log(formData);
			e.preventDefault();
			 jQuery.ajax({ 
				type:"post",
				dataType : 'json',
				url: base_url+"/wp-admin/admin-ajax.php",
				data: formData,
				success:function(response){ 
					$(".spinner-border").hide(); 
					if(response.status==true){
						$(".dmform-error").hide();
						$(".dmform-success").show();
						$("#commentForm")[0].reset();
					}else if(response.status=='recaptcha_error' ){	
						$(".dmform-error").show();
						$(".dmform-error").html(response.msg);
					}else{
						
						$("#commentForm")[0].reset();
						$(".dmform-error").html(response.msg);
						grecaptcha.reset();
					}
					
				},
				beforeSend:function(response){
					$(".spinner-border").show();
				}						
			});
		});	
	});

</script> 