<?php
/**
 * Template Name: Home page 
 * The template for displaying Home page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
get_header();
?>
<style>
.dmWidget.R {
    float: left !important;
}
</style>
<!-- End of RT CSS Include -->
<link rel="preload" href="<?php echo get_stylesheet_directory_uri()?>/css/51716a39_home_1.min.css" id="homeCssLink" as="style" importance="low" onload="loadCSS(this)" />
<link rel="preload" href="<?php echo get_stylesheet_directory_uri()?>/css/51716a39_1.min.css" id="siteGlobalCss" as="style" importance="low" onload="loadCSS(this)" />
 <link rel="preload" as="style" importance="low" onload="loadCSS(this)" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/css/media.css' ?>" />
<div id="site_content" class="home_page">
   <div class="dmRespRow dmRespRowStable dmRespRowNoPadding dmPageTitleRow dmInnerBarRow">
      <div class="dmRespColsWrapper">
         <div class="large-12 dmRespCol">
            <div id="innerBar" class="innerBar lineInnerBar dmDisplay_None">
               <div class="titleLine display_None">
                  <hr/>
               </div>
               <!-- Page title is hidden in css for new responsive sites. It is left here only so we don't break old sites. Don't copy it to new layouts --> 
               <div id="pageTitleText"></div>
               <div class="titleLine display_None">
                  <hr/>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php echo do_shortcode( '[slick-slider fade="true" dots="false" arrows="true"]' );?>

   <div dmwrapped="true" id="1554514795" class="dmBody u_dmStyle_template_home dm-home-page" themewaschanged="true">
      <div id="allWrapper" class="allWrapper">

         <!-- navigation placeholders --> 
         <div id="dm_content" class="dmContent">
            <div dm:templateorder="117" class="dmFullRowRespTmpl mainBorder dmRespRowsWrapper" id="1734971351">
               <div class="u_1514154341 dmRespRow fullBleedChanged fullBleedMode" id="1514154341">
                  <div class="dmRespColsWrapper" id="1662282693">
                     <div class="dmRespCol large-12 medium-12 small-12 content-removed" id="1384233177"></div>
                  </div>
               </div>
               <div class="u_1825294111 dmRespRow hide-for-medium hide-for-small" id="1825294111">
                  <div class="dmRespColsWrapper" id="1123498717">
                     <div class="dmRespCol empty-column large-12 medium-12 small-12" id="1532882722"></div>
                  </div>
               </div>
               <div class="dmRespRow u_1044344646" id="1044344646">
                  <div class="dmRespColsWrapper" id="1601323143">
                     <div class="dmRespCol large-12 medium-12 small-12" id="1884139051">
                        <h1 class="dmNewParagraph u_1482106349" data-element-type="paragraph" id="1482106349" style="text-align: center; transition: opacity 1s ease-in-out 0s;" data-uialign="center"><span style="" class="font--36 lh-1"><font style="color: rgb(255, 255, 255);"><b><span style="font-weight: 400;">
                        <?php if(get_field('section2_heading')) :
                        the_field('section2_heading');
                      endif; ?>
                     </span></b></font></span></h1>
                        <div class="u_1784180854 dmNewParagraph ql-disabled" data-dmtmpl="true" data-element-type="paragraph" data-version="5" id="1784180854" style="transition: opacity 1s ease-in-out 0s;" data-uialign="center">
                           <p class="text-align-center m-size-17 size-24"><span class="font--24 m-font-size-17" style="display: initial; font-style: italic; color: rgb(255, 254, 41);"><?php the_field('section2_content'); ?></span></p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="u_1454046630 dmRespRow" style="text-align: center;" id="1454046630">
                  <div class="dmRespColsWrapper" id="1792872438">
                     <div class="dmRespCol small-12 medium-12 large-12" id="1568837381">
                        <h3 class="dmNewParagraph u_1277082927" id="1277082927" style="display: block;" data-element-type="paragraph" data-uialign="center"><span style="" class="font-size-30 lh-1"><font style="color: rgb(0, 0, 0);"><?php echo the_field('section3_heading'); ?></font></span></h3>
                        <div class="dmNewParagraph u_1409032146" data-dmtmpl="true" id="1409032146" style="display: block; transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: center; margin: 20px auto 8px; float: none; width: 960px; height: auto; position: relative;" data-uialign="center">
                           
                           <div style="text-align: center;"><span style="color: rgb(102, 102, 102); font-size: 18px;"><?php  the_field('section3_content'); ?></span></div>
                        </div>
                        <a data-display-type="block" class="u_1920629359 align-center dmButtonLink dmWidget dmWwr default dmOnlyButton dmDefaultGradient" file="false" href="<?php echo get_field('button_link'); ?>" data-element-type="dButtonLinkId" id="1920629359" dm_dont_rewrite_url="false"> <span class="iconBg" id="1017066023"> <span class="icon hasFontIcon icon-star" id="1709150978"></span> 
                        </span> 
                        <span class="text" id="1520383026"><?php the_field('button_text'); ?></span> 
                        </a> 
                     </div>
                  </div>
               </div>
               <div class="u_1222876846 dmRespRow" style="text-align: center;" id="1222876846">
                  <div class="dmRespColsWrapper" id="1950769346">
                     <div class="dmRespCol small-12 medium-12 large-12" id="1827323789">
                        <span id="1675918617"></span> 
                        <h4 class="u_1215781291 dmNewParagraph" id="1215781291" style="transition: none 0s ease 0s; display: block;" data-uialign="center" data-element-type="paragraph">
                           <div style="text-align: center;"><font color="#0c0b0b"><span style="" class="m-font--36 lh-1 font-48"><?php the_field('service_section_heading'); ?></span></font></div>
                        </h4>
                     </div>
                  </div>
               </div>
               <div class="dmRespRow u_1995702180" id="1995702180">
                  <div class="dmRespColsWrapper" id="1493851970">
                     <div class="u_1215195264 dmRespCol small-12 large-4 medium-4" id="1215195264">
                        <h3 class="u_1086057936 dmNewParagraph" id="1086057936" data-uialign="center" data-element-type="paragraph" style="transition: none 0s ease 0s;">
                           <div style="text-align: center;"><font style="color: rgb(255, 255, 255);" class="font--24 lh-1"><?php the_field('service1_heading'); ?></font></div>
                        </h3>
                        <div class="u_1706571238 dmPhotoGallery newPhotoGallery dmPhotoGalleryResp" galleryoptionsparams="{thumbnailsPerRow: 3, rowsToShow: 3, imageScaleMethod: true}" data-desktop-layout="square" data-desktop-columns="1" data-element-type="dPhotoGalleryId" data-desktop-text-layout="bottom" id="1706571238" data-desktop-caption-alignment="top_left" data-rows-to-show="100" data-placeholder="false" data-mobile-columns="1">
                           <ul class="dmPhotoGalleryHolder clearfix gallery shadowEffectToChildren gallery4inArow" id="1014223541" dont_set_id="true">
                              <li index="0" class="photoGalleryThumbs" id="1532512152">
                                 <div class="image-container" id="1826765996"> <a data-dm-multisize-attr="href" data-image-url="https://irp-cdn.multiscreensite.com/51716a39/dms3rep/multi/clean.jpg" id="1995597004" class="u_1995597004"><img data-src="51716a39/dms3rep/multi/opt/clean-1920w.jpg" irh="1" irw="2" alt="" id="1713249163" class="" onerror="handleImageLoadError(this)"/></a> </div>
                                 <div class="caption-container  u_1676849420" id="1676849420">
                                    <span class="caption-inner" id="1644575040">
                                       <h3 class="caption-title u_1233548333" id="1233548333">Skilled Nursing and Therapy Services</h3>
                                       <div class="caption-text u_1552232719" id="1552232719" style="">
                                          <br/>
                                          <p class="rteBlock">Services include:</p>
                                          <ul class="rteBlock defaultList">
                                             <li>Skilled Nursing</li>
                                             <li>Physical Therapy</li>
                                             <li>Occupational Therapy</li>
                                             <li>Speech Therapy</li>
                                             <li>Medical Social Work</li>
                                             <li>Caregiver Services</li>
                                          </ul>
                                          <br/><br/><br/><br/><br/>
                                       </div>
                                       <a class="caption-button dmWidget   clearfix u_1630501251" href="skilled-nursing.html" id="1630501251" dm_dont_rewrite_url="false" file="false" link-modified="true"> <span class="iconBg" id="1335506318"> <span class="icon hasFontIcon icon-star" id="1789825659"></span> 
                                       </span> 
                                       <span class="text" id="1441721161">Learn More</span> 
                                       </a> 
                                    </span>
                                 </div>
                              </li>
                           </ul>
                           <div class="photoGalleryViewAll link" isall="true" data-viewall="View more" data-viewless="View less" style="display:none;" id="1302606859"></div>
                        </div>
                     </div>
                     <div class="u_1783517396 dmRespCol small-12 large-8 medium-8" id="1783517396">
                        <h3 class="u_1119679030 dmNewParagraph" id="1119679030" style="transition: none 0s ease 0s;" data-uialign="center" data-element-type="paragraph">
                           <div style="text-align: center;"><font style="color: rgb(255, 255, 255);" class="font--24 lh-1"><?php the_field('service2_heading'); ?></font></div>
                        </h3>
                        <div class="u_1150064675 dmPhotoGallery newPhotoGallery dmPhotoGalleryResp" galleryoptionsparams="{thumbnailsPerRow: 3, rowsToShow: 3, imageScaleMethod: true}" data-desktop-layout="square" data-desktop-columns="2" data-element-type="dPhotoGalleryId" data-desktop-text-layout="bottom" id="1150064675" data-desktop-caption-alignment="top_center" data-rows-to-show="100" data-placeholder="false" data-mobile-columns="1">
                           <ul class="dmPhotoGalleryHolder clearfix gallery shadowEffectToChildren gallery4inArow" id="1877976114" dont_set_id="true">
                              <li index="1" class="photoGalleryThumbs" id="1439834173">
                                 <div class="image-container" id="1118156093"> <a data-dm-multisize-attr="href" data-image-url="https://irp-cdn.multiscreensite.com/51716a39/dms3rep/multi/meal.jpg" id="1044435878" class="u_1044435878"><img data-src="51716a39/dms3rep/multi/opt/meal-1920w.jpg" irh="2" irw="3" alt="" id="1542617606" class="" onerror="handleImageLoadError(this)"/></a> </div>
                                 <div class="caption-container  u_1033138019" id="1033138019">
                                    <span class="caption-inner" id="1672191322">
                                       <h3 class="caption-title u_1354985574" id="1354985574">Skilled Nursing</h3>
                                       <div class="caption-text u_1021990998" id="1021990998" style="">
                                          <br/>
                                          <p class="rteBlock">Services Include:&nbsp;</p>
                                          <ul class="rteBlock defaultList">
                                             <li>High Tech</li>
                                             <li>Injections</li>
                                             <li>Wound Care</li>
                                             <li>Medication Administration & Instruction</li>
                                             <li>Catheter Insertion & Care</li>
                                             <li>Diabetic Care</li>
                                             <li>Other Nursing Care</li>
                                          </ul>
                                          <br/><br/><br/><br/><br/>
                                       </div>
                                       <a class="caption-button dmWidget   clearfix u_1976221343" href="private-duty-skilled-nursing-care.html" id="1976221343" dm_dont_rewrite_url="false" file="false" link-modified="true"> <span class="iconBg" id="1941697385"> <span class="icon hasFontIcon icon-star" id="1353321428"></span> 
                                       </span> 
                                       <span class="text" id="1214769868">Learn More</span> 
                                       </a> 
                                    </span>
                                 </div>
                              </li>
                              <li class="photoGalleryThumbs" id="1997377981">
                                 <div class="image-container" id="1590436289"> <a data-dm-multisize-attr="href" data-image-url="https://irp-cdn.multiscreensite.com/51716a39/dms3rep/multi/skilled1.jpg" id="1370597937"><img irh="" irw="" alt="" data-src="51716a39/dms3rep/multi/opt/skilled1-1920w.jpg" id="1853130231" onerror="handleImageLoadError(this)"/></a> </div>
                                 <div class="caption-container u_1515445955" style="display:none" id="1515445955">
                                    <span class="caption-inner" id="1433785074">
                                       <h3 class="caption-title u_1324034589" id="1324034589">Caregiver Services</h3>
                                       <div class="caption-text u_1795711615" id="1795711615">
                                          <br/>
                                          <p class="rteBlock">Services Include:</p>
                                          <ul class="rteBlock defaultList">
                                             <li>Bathing/Showering</li>
                                             <li>Dressing-Up/Grooming</li>
                                             <li>Feeding/Eating</li>
                                             <li>Incontinent Care</li>
                                             <li>Toileting</li>
                                             <li>Ambulation/Transfers</li>
                                             <li>Medication Reminder/ Monitoring</li>
                                             <li>Grocery Shopping/ Running Errands</li>
                                             <li>Light Housekeeping, Cleaning, and Laundry</li>
                                             <li>Paying Bills / Etc.</li>
                                          </ul>
                                       </div>
                                       <a class="caption-button dmWidget clearfix u_1836663795" id="1836663795" dm_dont_rewrite_url="false" href="caregiver-services.html" file="false" link-modified="true"> <span class="iconBg" id="1004550617"> <span class="icon hasFontIcon icon-star" id="1548283246"></span> 
                                       </span> 
                                       <span class="text" id="1563350888">Learn More</span> 
                                       </a> 
                                    </span>
                                 </div>
                              </li>
                           </ul>
                           <div class="photoGalleryViewAll link" isall="true" data-viewall="View more" data-viewless="View less" style="display:none;" id="1393241799"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="u_1245415772 dmRespRow hide-for-small" style="text-align: center;" id="1245415772">
                  <div class="dmRespColsWrapper" id="1898983389">
                     <div class="dmRespCol small-12 medium-12 large-12" id="1930068185">
                        <span id="1943675762"></span> 
                        <h3 class="dmNewParagraph u_1074520291" id="1074520291" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: -52px auto 8px; float: none; width: 960px; height: 7px; position: relative; max-width: 960px !important;" data-uialign="center" data-element-type="paragraph"><font color="#0c0b0b"><span style="font-size: 30px;">TESTIMONIALS</span></font></h3>
                     </div>
                  </div>
               </div>
               <div class="dmRespRow" id="1669008857">
                  <div class="dmRespColsWrapper" id="1671174463">
                     <?php $i=0;
                      $args = array(  
                          'post_type' => 'testimonials',
                          'post_status' => 'publish',
                          'posts_per_page' => 4, 
                          'orderby' => 'date', 
                          'order' => 'ASC', 
                      );

                      $loop = new WP_Query( $args ); 
                          
                      while ( $loop->have_posts() ) : $loop->the_post(); 
                     ?>
                     <div class="dmRespCol large-3 medium-3 small-12 u_1338005344" id="1338005344">
                        <div class="u_1362415960 graphicWidget hide-for-small" editablewidget="true" data-element-type="graphic" data-widget-type="graphic" id="1362415960" data-layout="graphic-style-2" title="Quotation Mark">
                           <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 1664 1792" id="1971274868" class="svg u_1971274868" data-icon-name="fa-quote-right" alt="Quotation Mark">
                              <path fill="inherit" d="M768 320v704q0 104-40.5 198.5t-109.5 163.5-163.5 109.5-198.5 40.5h-64q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h64q106 0 181-75t75-181v-32q0-40-28-68t-68-28h-224q-80 0-136-56t-56-136v-384q0-80 56-136t136-56h384q80 0 136 56t56 136zM1664 320v704q0 104-40.5 198.5t-109.5 163.5-163.5 109.5-198.5 40.5h-64q-26 0-45-19t-19-45v-128q0-26 19-45t45-19h64q106 0 181-75t75-181v-32q0-40-28-68t-68-28h-224q-80 0-136-56t-56-136v-384q0-80 56-136t136-56h384q80 0 136 56t56 136z"></path>
                           </svg>
                        </div>
                        <div class="dmNewParagraph u_1960583667" data-dmtmpl="true" data-element-type="paragraph" id="1960583667" style="transition: opacity 1s ease-in-out 0s;" data-uialign="center">
                           <div><br/></div>
                           <div><font style="color: rgb(0, 0, 0);"><?php 
                           $content = get_the_content();
                           $content = strip_tags($content);
                           echo substr($content, 0, 100).'...'; ?></font></div>
                        </div>
                        <div class="dmNewParagraph u_1224828773" data-dmtmpl="true" data-element-type="paragraph" id="1224828773" style="transition: opacity 1s ease-in-out 0s;" data-uialign="center" data-editor-state="closed">
                           <div><br/></div>
                           <div><font style="color: rgb(0, 0, 0);"><b><?php echo get_field('testimonial_name'); ?></b></font></div>
                           <div><font style="color: rgb(46, 176, 246);"><a href="<?php echo home_url() ;?>/testimonials" runtime_url="/testimonials" style="color: rgb(46, 176, 246);">Read More</a></font></div> 
                        </div>
                     </div>
                  <?php 
                  $i++;
               endwhile; 
               wp_reset_postdata(); 
               ?>
                  </div>
               </div>
               <div class="u_1446740145 dmRespRow" style="text-align: center;" id="1446740145">
                  <div class="dmRespColsWrapper" id="1678338290">
                     <div class="u_1085644380 dmRespCol small-12 large-3 medium-3" id="1085644380">
                        <div class="dmNewParagraph u_1006168195" data-dmtmpl="true" data-element-type="paragraph" id="1006168195" data-uialign="left" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 16px 0px 0px; float: none; width: 367px; height: auto; position: relative; max-width: 166px !important; min-width: 166px !important;"><font style="color: rgb(0, 0, 0);" class="lh-1 font-size-22">Contact Details</font></div>
                        <div class="u_1806587324 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1806587324" data-uialign="left" data-styletopreserve="{" background-image":""}"="" data-editor-state="closed"> 
                        <div><span style="" class="lh-1 font-size-16"><font style=""><span style=""><?php  echo get_theme_mod('contact_details');?></div>
                        <div>
                           <div>
                              <span style="" class="lh-1 font-size-16"><font style="color: rgb(181, 49, 44);"><span style=""><br/></span></font> 
                              </span> 
                              <div><font style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><a href="tel:<?php  echo get_theme_mod('contact_phone_no');?>" runtime_url="tel:<?php  echo get_theme_mod('contact_phone_no');?>" style="color: rgb(181, 49, 44);" class="lh-1 font-size-16"><?php  echo get_theme_mod('contact_phone_no');?></a></font></div>
                              <div><span style="" class="font-size-14 lh-1"><br/></span></div>
                              <div><a href="mailto:<?php  echo get_theme_mod('contact_email');?>" runtime_url="mailto:<?php  echo get_theme_mod('contact_email');?>" style="color: rgb(46, 176, 246);"><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_email');?></font></a></div>
                              <div><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><br/></font></div>
                              <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('office_hours');?></font></div>
                              <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><br/></font></div>
                              <div><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);" class="font-size-14 lh-1">
                                 <?php  echo get_theme_mod('contact_address1');?>
                                    </span>
                                    <br></br>
                                    <span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);">
                                       <?php  echo get_theme_mod('contact_address2');?>
                                    </span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="u_1468308291 dmRespCol small-12 large-9 medium-9" id="1468308291">
                    <?php get_template_part('partials/contact/contact'); ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
//get_sidebar();
get_footer();
?>
<style>
	.dmform-error {color:red!important;font-size: 24px!important;}
    .dmform-error {color:green!important;font-size: 24px!important;}
</style>
<script src='https://www.google.com/recaptcha/api.js' async defer ></script>
<script src="<?php echo get_stylesheet_directory_uri()?>/js/jquery.validate.js"></script>  
<script>
	$(document).ready(function(){
		jQuery("#commentForm").validate();
		$('body').on('submit','#commentForm',function(e){
			var base_url = "<?php echo home_url();?>";
			var formData= $('#commentForm').serialize();
			console.log(formData);
			e.preventDefault();
			 jQuery.ajax({ 
				type:"post",
				dataType : 'json',
				url: base_url+"/wp-admin/admin-ajax.php",
				data: formData,
				success:function(response){ 
					$(".spinner-border").hide(); 
					if(response.status==true){
						$(".dmform-error").hide();
						$(".dmform-success").show();
						$("#commentForm")[0].reset();
					}else if(response.status=='recaptcha_error' ){	
						$(".dmform-error").show();
						$(".dmform-error").html(response.msg);
					}else{	
						$(".dmform-error").show();
						$("#commentForm")[0].reset();
						$(".dmform-error").html(response.msg);
						grecaptcha.reset();
					}
					
				},
				beforeSend:function(response){
					$(".spinner-border").show();
				}						
			});
		});	
	});

</script> 