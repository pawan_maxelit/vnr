<style>
.error {
    color: #e61313 !important;
}
</style>
<div class="u_1505275905 dmform default" >
   <h3 class="dmform-title dmwidget-title" id="1943791538">Contact Us <?php if(is_home() || is_front_page()){ echo 'Today!'; } ?></h3>
   <div class="dmform-wrapper" preserve_css="true" id="1540438115" captcha-lang="en">
	  <form method="post" class="cmxform" id="commentForm" action="">
		 <div class="dmforminput required  small-12 dmRespDesignCol medium-12 large-12" id="1091241145"> <label for="dmform-0" id="1127067165">Name:</label> 
			<input type="text" class="" name="name" id="1509887643" required /><input type="hidden" name="action" value="contact_form_inquery" id="1832524367"/>
		 </div>
		 <div class="dmforminput required  small-12 dmRespDesignCol medium-12 large-12" id="1766385549"> <label for="dmform-1" id="1969739341">Email:</label> 
			<input type="email" class="" name="email" id="1508160829" required />
		 </div>
		 <div class="dmforminput required  small-12 dmRespDesignCol medium-12 large-12" id="1876320445"> <label for="dmform-2" id="1083995350">Phone:</label> 
			<input type="tel" class="" name="phone" id="1168524067"/>
		 </div>
		 <div class="dmforminput large-12 medium-12 dmRespDesignCol small-12" id="1891306863"> <label for="dmform-3" id="1232776556">Message:</label> 
			<textarea name="message" id="1221373922"></textarea> 
		 <div class="g-recaptcha" data-sitekey="<?php echo GOOGLE_RECAPTCH_KEY; ?>"></div>
		 </div>
		 <span id="1891306863_clear" class="dmWidgetClear"></span> 
		 <div class="dmformsubmit dmWidget R" preserve_css="true" id="1553211185"><input class="" name="submit" type="submit" value="Send Message" id="1233818972"/></div>
		
		 <div class="spinner-border" role="status" style="display:none;">  </div>
	  </form>
   </div>
   <div class="dmform-success" style="display:none" preserve_css="true" id="1065913156">Thank you for contacting us.<br id="1224248818"/>We will get back to you as soon as possible.</div>
   <div class="dmform-error" style="display:none" preserve_css="true" id="1656980343">Oops, there was an error sending your message.<br id="1741285276"/>Please try again later.</div>
</div>
