 <div class="u_1371840841 dmRespCol small-12 large-3 medium-3" id="1371840841">
	<div class="dmNewParagraph u_1330770612" data-dmtmpl="true" data-element-type="paragraph" id="1330770612" style="transition: none 0s ease 0s;" data-uialign="left"><font style="color: rgb(102, 102, 102);" class="font-size-18 lh-1">Contact Us Today!</font></div>
	<div class="u_1833944401 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1833944401" data-uialign="left" data-styletopreserve="{" background-image":""}"="" data-editor-state="closed"> 
	<div><span style="font-size: 16px; background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_details');?></span><br/></div>
	<div>
	   <div>
		  <span style="" class="lh-1 font-size-16"><font style="color: rgb(181, 49, 44);"><span style=""><br/></span></font> 
		  </span> 
		  <div><font style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><a href="tel:(561) 731-3155" runtime_url="tel:<?php  echo get_theme_mod('contact_phone_no');?>" style="color: rgb(181, 49, 44);" class="lh-1 font-size-16"><?php  echo get_theme_mod('contact_phone_no');?></a></font></div>
		  <div><span style="" class="font-size-14 lh-1"><br/></span></div>
		  <div><a href="mailto:<?php  echo get_theme_mod('contact_email');?>" runtime_url="mailto:<?php  echo get_theme_mod('contact_email');?>" style="color: rgb(46, 176, 246);"><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_email');?></font></a></div>
		  <div><font style="color: rgb(46, 176, 246);" class="font-size-14 lh-1"><br/></font></div>
		  <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('office_hours');?></font></div>
		  <div><font style="color: rgb(0, 0, 0);" class="font-size-14 lh-1"><br/></font></div>
		  <div><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);" class="font-size-14 lh-1"><?php  echo get_theme_mod('contact_address1');?></span></div>
		  <div>
			 <span style="" class="font-size-14 lh-1">
				<span style="color: rgb(0, 0, 0);">
				   <ul class="innerList defaultList">
					  <li><span style="color: rgb(0, 0, 0); background-color: rgba(0, 0, 0, 0);"><?php  echo get_theme_mod('contact_address2');?></span></li>
				   </ul>
				</span>
			 </span>
		  </div>
	   </div>
	</div>
 </div>