<div class="u_1384753443 dmNewParagraph" data-dmtmpl="true" data-element-type="paragraph" id="1384753443" data-uialign="left" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 16px 0px 0px; float: none; width: 367px; height: auto; position: relative; max-width: 166px !important; min-width: 166px !important;"><font style="color: rgb(0, 0, 0);" class="lh-1 font-size-22">Contact Details</font></div>
								<div class="dmNewParagraph u_1665393511" data-dmtmpl="true" data-element-type="paragraph" id="1665393511" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: left; display: block; margin: 16px 0px 0px; float: none; width: 207px; height: 166px; position: relative; max-width: 207px !important; min-width: 207px !important;" data-uialign="left">
								   <div><span style="" class="font-size-16 lh-1"><font style="color: rgb(0, 0, 0);"><font style=""><span style="">Veritas Home Care Inc.</span></font></font></span></div>
								   <div><font style="color: rgb(0, 0, 0); font-size: 16px; background-color: rgba(0, 0, 0, 0);"><span>Veritas Nurse Registry, Inc.&nbsp;</span></font><span style="color: rgb(0, 0, 0); font-size: 16px; background-color: rgba(0, 0, 0, 0);">&nbsp; &nbsp;</span><br/></div>
								   <div>
									  <div>
										 <div><font style="color: rgb(181, 49, 44);" class="font-size-16 lh-1"><a href="tel:(561) 731-3155" runtime_url="tel:(561) 731-3155" style="color: rgb(181, 49, 44);" class="font-size-16 lh-1">(561) 731-3155</a></font></div>
										 <div><br/></div>
										 <div><font style="color: rgb(46, 176, 246);"><a href="mailto:admin@veritasnursecare.com" runtime_url="mailto:admin@veritasnursecare.com" style="color: rgb(46, 176, 246);">admin@veritasnursecare.com</a></font></div>
										 <div><br/></div>
										 <div><font style="color: rgb(0, 0, 0);" class="font-size-16 lh-1">Office Hours:</font></div>
										 <div><font style="color: rgb(0, 0, 0);" class="font-size-16 lh-1">Monday &ndash; Friday</font></div>
										 <div><font style="color: rgb(0, 0, 0);" class="font-size-16 lh-1">9:00 AM &ndash; 5:00 PM</font></div>
