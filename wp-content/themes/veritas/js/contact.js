!(function (e, n, s) {
    function o(e, n) {
        return typeof e === n;
    }
    function t() {
        var e, n, s, t, a, r, l;
        for (var c in i)
            if (i.hasOwnProperty(c)) {
                if (((e = []), (n = i[c]), n.name && (e.push(n.name.toLowerCase()), n.options && n.options.aliases && n.options.aliases.length))) for (s = 0; s < n.options.aliases.length; s++) e.push(n.options.aliases[s].toLowerCase());
                for (t = o(n.fn, "function") ? n.fn() : n.fn, a = 0; a < e.length; a++)
                    (r = e[a]),
                        (l = r.split(".")),
                        1 === l.length ? (Modernizr[l[0]] = t) : (!Modernizr[l[0]] || Modernizr[l[0]] instanceof Boolean || (Modernizr[l[0]] = new Boolean(Modernizr[l[0]])), (Modernizr[l[0]][l[1]] = t)),
                        f.push((t ? "" : "no-") + l.join("-"));
            }
    }
    function a(e) {
        var n = l.className,
            s = Modernizr._config.classPrefix || "";
        if ((c && (n = n.baseVal), Modernizr._config.enableJSClass)) {
            var o = new RegExp("(^|\\s)" + s + "no-js(\\s|$)");
            n = n.replace(o, "$1" + s + "js$2");
        }
        Modernizr._config.enableClasses && ((n += " " + s + e.join(" " + s)), c ? (l.className.baseVal = n) : (l.className = n));
    }
    var i = [],
        r = {
            _version: "3.5.0",
            _config: { classPrefix: "dm-", enableClasses: !0, enableJSClass: !0, usePrefixes: !0 },
            _q: [],
            on: function (e, n) {
                var s = this;
                setTimeout(function () {
                    n(s[e]);
                }, 0);
            },
            addTest: function (e, n, s) {
                i.push({ name: e, fn: n, options: s });
            },
            addAsyncTest: function (e) {
                i.push({ name: null, fn: e });
            },
        },
        Modernizr = function () {};
    (Modernizr.prototype = r), (Modernizr = new Modernizr());
    var f = [],
        l = n.documentElement,
        c = "svg" === l.nodeName.toLowerCase(),
        u = "CSS" in e && "supports" in e.CSS,
        p = "supportsCSS" in e;
    Modernizr.addTest("supports", u || p),
        Modernizr.addTest("passiveeventlisteners", function () {
            var n = !1;
            try {
                var s = Object.defineProperty({}, "passive", {
                    get: function () {
                        n = !0;
                    },
                });
                e.addEventListener("test", null, s);
            } catch (o) {}
            return n;
        }),
        t(),
        a(f),
        delete r.addTest,
        delete r.addAsyncTest;
    for (var d = 0; d < Modernizr._q.length; d++) Modernizr._q[d]();
    e.Modernizr = Modernizr;
})(window, document);
var isWLR = true;
window.customWidgetsFunctions = {};
window.customWidgetsStrings = {};
window.collections = {};
window.currentLanguage = "ENGLISH";


/**** Scond ****/
 var d_version = "8.9.9";
         var build = "2021-09-23T12_53_56";
         window['v'+'ersion'] = d_version;
         function buildEditorParent() {
             window.isMultiScreen = true;
             window.editorParent = {};
             window.previewParent = {};
             window.assetsCacheQueryParam = "?version=2021-09-23T12_53_56";
             try {
                 var _p = window.parent;
                 if (_p && _p.document && _p.$ && _p.$.dmfw) {
                     window.editorParent = _p;
                 }
                 else if (_p.isSitePreview) {
                     window.previewParent = _p;
                 }
             } catch (e) {
             }
         }
buildEditorParent();

/*** Third ***/
 var jquery = window.$;
 Object.defineProperty(window, '$', {
	 get() {
		 return jquery;
	 },
	 set() {
		 console.warn("Trying to over-write the global jquery object!");
	 }
 });
  window.cookiesNotificationMarkupPreview = 'null';
  
/*** Fourth **/
 var shouldMonitorImages = false;
 var numOfImageLoadErrorsReported = 0;
 if(shouldMonitorImages) {
	 window.addEventListener('error', function(event) {
		 // log secured resources failures
		 if(event.target && event.target.src && event.target.src.includes
			 && event.target.src.includes('/s/')
			 && !event.target.src.includes('/_dm/s/rt/')) {
			 if (!window.fetch || numOfImageLoadErrorsReported > 4){
				 return;
			 }
			 try {
				 numOfImageLoadErrorsReported++;
				 return fetch('/_dm/s/rt/actions/log/INFO', {
					 method: 'POST',
					 headers: {
						 'Content-Type': 'application/json'
					 },
					 body: JSON.stringify({ data: {
							 tags: 'imageFailedToLoad',
							 eventType: event.type,
							 image: event.target.src,
							 href: window.location.href
						 } })
				 });
			 } catch (error) {
				 console.log(error);
			 }
		 }
	 }, { capture: true });
 }  
 
  window.INSITE = window.INSITE || {};
         window.INSITE.device = "desktop";
         window.rtCommonProps = {};
         rtCommonProps["rt.ajax.ajaxScriptsFix"] =true;
         rtCommonProps["rt.pushnotifs.sslframe.encoded"] = 'aHR0cHM6Ly97c3ViZG9tYWlufS5wdXNoLW5vdGlmcy5jb20=';
         rtCommonProps["runtimecollector.url"] = 'https://rtc.multiscreensite.com';
         rtCommonProps["facebook.accessToken"] = '126515034112906|8vv7JhnEegS8qz43fIOZjxGZReA';
         rtCommonProps["performance.tabletPreview.removeScroll"] = 'false';
         rtCommonProps["feature.flag.contactUsNewForm"] = 'true';
         rtCommonProps["inlineEditGrid.snap"] =true;
         rtCommonProps["popup.insite.cookie.ttl"] = '0.5';
         rtCommonProps["rt.pushnotifs.force.button"] =true;
         rtCommonProps["google.places.key"] = 'AIzaSyBAwUOqPUB1CU31yDztoZYaUE7sPv4ktEI';
         rtCommonProps["common.mapbox.token"] = 'pk.eyJ1IjoiZGFubnliMTIzIiwiYSI6ImNqMGljZ256dzAwMDAycXBkdWxwbDgzeXYifQ.Ck5P-0NKPVKAZ6SH98gxxw';
         rtCommonProps["common.mapbox.js.override"] =false;
         rtCommonProps["common.opencage.token"] = '319e14f32bcce967ba55cd263478796d';
         rtCommonProps["common.here.appId"] = 'iYvDjIQ2quyEu0rg0hLo';
         rtCommonProps["common.here.appCode"] = '1hcIxLJcbybmtBYTD9Z1UA';
         rtCommonProps["isCoverage.test"] =false;
         rtCommonProps["ecommerce.ecwid.script"] = 'https://app.multiscreenstore.com/script.js';
         rtCommonProps["feature.flag.mappy.kml"] =false;
         rtCommonProps["common.resources.dist.cdn"]=true;
         rtCommonProps["common.build.dist.folder"]='production/1914';
         rtCommonProps["common.resources.cdn.host"]='https://static.cdn-website.com';
         rtCommonProps["common.resources.folder"]='https://static.cdn-website.com/mnlt/production/1914';
         rtCommonProps["import.images.storage.useImageCDN"]=true;
         rtCommonProps["feature.flag.runtime.backgroundSlider.preload.slowly"]=true;
         rtCommonProps["feature.flag.runtime.photoswipe.fix"]=true;
         rtCommonProps["feature.flag.runtime.newAnimation.enabled"]=true;
         rtCommonProps["feature.flag.runtime.newAnimation.respectCssAnimationProps.enabled"]=true;
         rtCommonProps["feature.flag.runtime.newAnimation.jitAnimation.enabled"]=true;
         rtCommonProps["feature.flag.sites.google.analytics.gtag"]=true;
         rtCommonProps['common.mapsProvider'] = 'mapbox';
         rtCommonProps['common.mapsProvider.version'] = '0.52.0';
         rtCommonProps['common.geocodeProvider'] = 'here';
         rtCommonProps['common.map.defaults.radiusSize'] = '1500';
         rtCommonProps['common.map.defaults.radiusBg'] = 'rgba(255, 255, 255, 0.4)';
         rtCommonProps['common.map.defaults.strokeColor'] = 'rgba(255, 255, 255, 1)';
         rtCommonProps['common.map.defaults.strokeSize'] = '2';
         rtCommonProps['server.for.resources'] = '';
         rtCommonProps['feature.flag.lazy.widgets'] = true;
         rtCommonProps['editor.infra.noPrefixedChanges'] = true;
         rtCommonProps['feature.flag.single.wow'] = false;
         rtCommonProps['feature.flag.mark.anchors'] = true;
         rtCommonProps['captcha.public.key'] = '6LffcBsUAAAAAMU-MYacU-6QHY4iDtUEYv_Ppwlz';
         rtCommonProps['captcha.invisible.public.key'] = '6LeiWB8UAAAAAHYnVJM7_-7ap6bXCUNGiv7bBPME';
         rtCommonProps["images.sizes.small"] =160;
         rtCommonProps["images.sizes.mobile"] =640;
         rtCommonProps["images.sizes.tablet"] =1280;
         rtCommonProps["images.sizes.desktop"] =1920;
         rtCommonProps["modules.resources.cdn"] =true;
         rtCommonProps["import.images.storage.imageCDN"] = 'https://lirp.cdn-website.com/';
         rtCommonProps["facebook.api.version"] = '7.0';
         rtCommonProps["runtime.save.restore.function.bind"] =true;
         rtCommonProps["enable.extended.collections.js.api"] =true;
         rtCommonProps["feature.flag.photo.gallery.exact.size"] =false;
         rtCommonProps["feature.flag.photo.gallery.lazy"] =true;
         rtCommonProps["store.handleItemId.enabled"] =true;
         rtCommonProps["new.store.fix.ecwid.back.bug"] =true;
         rtCommonProps["new.store.accountPage.ecwid.signIn.fix"] =true;
         rtCommonProps["fix.ecwid.sign.in.page.with.return.url"] =true;
         rtCommonProps["site.runtime.video.background.ssr"] =true;
		 
		 jQuery.DM.updateWidthAndHeight();
         $(window).resize(function () {
         });
         $(window).bind("orientationchange", function (e) {
             $.layoutManager.initLayout();
         });
         $(document).resize(function () {
         });
		 
		 (function() {
          	var campaign = (/utm_campaign=([^&]*)/).exec(window.location.search);
          	if (campaign && campaign != null && campaign.length > 1) {
          		campaign = campaign[1];
          		document.cookie = "_dm_rt_campaign=" + campaign + ";expires=" + new Date().getTime() + 24*60*60*1000 + ";domain=" + window.location.hostname + ";path=/";
          	}
         }());