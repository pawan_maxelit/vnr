<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Veritas
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script src="<?php echo get_stylesheet_directory_uri(). '/js/header.js' ?>"></script>
      <!-- Injecting site-wide to the head -->
      <!-- End Injecting site-wide to the head -->
      <!-- Inject secured cdn script -->
      <!-- ========= Meta Tags ========= -->
      <!-- PWA settings -->
      <script>
         function toHash(str) {
             var hash = 5381, i = str.length;
             while (i) {
                 hash = hash * 33 ^ str.charCodeAt(--i)
             }
             return hash >>> 0
         }
      </script>
      <script>
         (function(global) {
         //const cacheKey = global.cacheKey;
         const isOffline = 'onLine' in navigator && navigator.onLine === false;
         const hasServiceWorkerSupport = 'serviceWorker' in navigator;
         if (isOffline) {
             console.log('offline mode');
         }
         if (!hasServiceWorkerSupport) {
             console.log('service worker is not supported');
         }
         if (hasServiceWorkerSupport && !isOffline) {
             window.addEventListener('load', function() {
                 const serviceWorkerPath = 'runtime-service-worker.js';
                 navigator.serviceWorker
                     .register(serviceWorkerPath, { scope: './' })
                     .then(
                         function(registration) {
                             // Registration was successful
                             console.log('ServiceWorker registration successful with scope: ', registration.scope);
                         },
                         function(err) {
                             // registration failed :(
                             console.log('ServiceWorker registration failed: ', err);
                         }
                     )
                     .catch(function(err) {
                         console.log(err);
                     });
             });
             // helper function to refresh the page
             var refreshPage = (function() {
                 var refreshing;
                 return function() {
                     if (refreshing) return;
                     // prevent multiple refreshes
                     var refreshkey = 'refreshed' + location.href;
                     var prevRefresh = localStorage.getItem(refreshkey);
                     if (prevRefresh) {
                         localStorage.removeItem(refreshkey);
                         if (Date.now() - prevRefresh < 30000) {
                             return; // dont go into a refresh loop
                         }
                     }
                     refreshing = true;
                     localStorage.setItem(refreshkey, Date.now());
                     console.log('refereshing page');
                     window.location.reload();
                 };
             })();
             function messageServiceWorker(data) {
                 return new Promise(function(resolve, reject) {
                     if (navigator.serviceWorker.controller) {
                         var worker = navigator.serviceWorker.controller;
                         var messageChannel = new MessageChannel();
                         messageChannel.port1.onmessage = replyHandler;
                         worker.postMessage(data, [messageChannel.port2]);
                         function replyHandler(event) {
                             resolve(event.data);
                         }
                     } else {
                         resolve();
                     }
                 });
             }
         }
         })(window);
      </script>
      <!-- Add manifest -->
      <!-- End PWA settings -->
	 
      <link rel="canonical" href="https://www.veritashomecare.com/">
      <meta id="view" name="viewport"
         content=", initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0, viewport-fit=cover"/>
      <meta name="apple-mobile-web-app-capable" content="yes">
      <!--Add favorites icons-->
      <link rel="apple-touch-icon" href="51716a39/dms3rep/multi/6543251.png"/>
      <link rel="icon" type="image/x-icon" href="51716a39/site_favicon_16_1590674892864.ico"/>
      <!-- End favorite icons -->
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
      <link rel="preconnect" href="https://lirp.cdn-website.com/" crossorigin />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <!-- render the required CSS and JS in the head section -->
      <script>
         window.SystemID = 'US_DIRECT_PRODUCTION';
         if(!window.dmAPI) {
             window.dmAPI = {
                 registerExternalRuntimeComponent: function() {
                 },
                 getCurrentDeviceType: function() {
                     return window._currentDevice;
                 }
             };
         }
         if (!window.requestIdleCallback) {
             window.requestIdleCallback = function (fn) {
                 setTimeout(fn, 0);
             }
         }
         function loadCSS(link) {
             try {
                 var urlParams = new URLSearchParams(window.location.search);
                 var noCSS = !!urlParams.get('nocss');
                 var cssTimeout = urlParams.get('cssTimeout') || 0;
                 if (noCSS) {
                     return;
                 }
                 requestIdleCallback(function () {
                     window.setTimeout(function () {
                         link.onload = null;
                         link.rel = 'stylesheet';
                         link.type = 'text/css'
                     }, parseInt(cssTimeout, 10));
                 });
             } catch (e) {/* Never fail - this is just a tool for measurements */}
         }
      </script>
      <script>
         (function(n){"use strict";if(!n.l){n.l=function(){}}var o=loadCSS.t={};o.o=function(){var l;try{l=n.document.createElement("link").relList.supports("preload")}catch(e){l=false}return function(){return l}}();o.i=function(e){var l=e.media||"all";function enableStylesheet(){if(e.addEventListener){e.removeEventListener("load",enableStylesheet)}else if(e.attachEvent){e.detachEvent("onload",enableStylesheet)}e.setAttribute("onload",null);e.media=l}if(e.addEventListener){e.addEventListener("load",enableStylesheet)}else if(e.attachEvent){e.attachEvent("onload",enableStylesheet)}setTimeout(function(){e.rel="stylesheet";e.media="only x"});setTimeout(enableStylesheet,3e3)};o.s=function(){if(o.o()){return}var e=n.document.getElementsByTagName("link");for(var l=0;l<e.length;l++){var t=e[l];if(t.rel==="preload"&&t.getAttribute("as")==="style"&&!t.getAttribute("data-loadcss")){t.setAttribute("data-loadcss",true);o.i(t)}}};if(!o.o()){o.s();var e=n.setInterval(o.s,500);if(n.addEventListener){n.addEventListener("load",function(){o.s();n.clearInterval(e)})}else if(n.attachEvent){n.attachEvent("onload",function(){o.s();n.clearInterval(e)})}}if(typeof exports!=="undefined"){exports.l=loadCSS}else{n.l=loadCSS}})(typeof global!=="undefined"?global:this);
      </script>
       <link rel="preload" as="style" importance="low" onload="loadCSS(this)" href="<?php echo get_stylesheet_directory_uri()?>/mnlt/production/1903/_dm/s/rt/dist/css/d-css-runtime-desktop-one-package-new.min.css" />
      <style id="forceCssIncludes">
	  li#menu-item-17 a {
			font-family: 'Open Sans';
		}
	  .icon-angle-down:after {
			content: '\f107' !important;
			
			    transition: 0.5s all ease;
		}
		.icon-angle-down:hover:after{  transform: rotate(
180deg);}
.other_services .icon-angle-down{
	display:none !important
}
		.u_1505275905 .dmwidget-title{
			    font-size: 30px!important;
				color: #000!important;
		}
		#dm .dmBody div.u_1505275905 .dmforminput label{
			color: #666!important;
		}
		#dm .dmBody div.u_1505275905 .dmforminput label.error{
			color: #cc1212!important;
		}
         /* This file is auto-generated from a `scss` file with the same name */
         .videobgwrapper{overflow:hidden;position:absolute;z-index:0;width:100%;height:100%;top:0;left:0;pointer-events:none;border-radius:inherit}.videobgframe{position:absolute;width:101%;height:100%;top:50%;left:50%;transform:translateY(-50%) translateX(-50%);object-fit:fill}#dm video.videobgframe{margin:0}
      </style>
      <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(). '/css/custom.css' ?>" />
      <link rel="preload" href="//fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic|Josefin+Sans:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic|Oswald:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic|Open+Sans:100,200,300,400,500,600,700,800,900,100italic,200italic,300italic,400italic,500italic,600italic,700italic,800italic,900italic&amp;subset=latin-ext&amp;display=swap"  as="style" importance="low" onload="loadCSS(this)" />
     
      <!-- End render the required css and JS in the head section -->
      <title>
         Veritas Home Care | Serving Palm Beach and Broward Counties
      </title>
      <meta name="description" content="Veritas Home Care provides personal care to those in need. We are passionate about taking care of your loved one as if they are our own."/>
      <meta name="twitter:card" content="summary"/>
      <meta name="twitter:title" content="Veritas Home Care | Serving Palm Beach and Broward Counties"/>
      <meta name="twitter:description" content="Veritas Home Care provides personal care to those in need. We are passionate about taking care of your loved one as if they are our own."/>
      <meta name="twitter:image" content="51716a39/dms3rep/multi/opt/6543251-1920w.png"/>
      <meta property="og:description" content="Veritas Home Care provides personal care to those in need. We are passionate about taking care of your loved one as if they are our own."/>
      <meta property="og:title" content="Veritas Home Care | Serving Palm Beach and Broward Counties"/>
      <meta property="og:image" content="51716a39/dms3rep/multi/opt/6543251-1920w.png"/>
      <!-- SYS- VVNfRElSRUNUX1BST0RVQ1RJT04= -->
	  
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
      <div id="disabledImageZone" style="display:none;z-index:-1">
         <style type="text/css">
            #imageZone {
            position: absolute;
            margin: auto;
            }
            .coloumns {
            border-radius: 3px;
            background-color: rgb(249, 152, 13); /*border:1px solid #999;*/
            height: 18px;
            width: 6px;
            -webkit-animation-name: loader;
            -webkit-animation-duration: 1s;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-direction: linear;
            -moz-animation-name: loader;
            -moz-animation-duration: 1s;
            -moz-animation-iteration-count: infinite;
            -moz-animation-direction: linear;
            opacity: .25;
            -webkit-transform: scale(0.7);
            -webkit-transform-origin: 50% 180%;
            -moz-transform: scale(0.7);
            -moz-transform-origin: 50% 180%;
            position: absolute;
            }
            #coloumn1 {
            -webkit-transform: rotate(0deg);
            -webkit-animation-delay: -.914s;
            -moz-transform: rotate(0deg);
            -moz-animation-delay: -.914s;
            }
            #coloumn2 {
            -webkit-transform: rotate(30deg);
            -webkit-animation-delay: -.831s;
            -moz-transform: rotate(30deg);
            -moz-animation-delay: -.831s;
            }
            #coloumn3 {
            -webkit-transform: rotate(60deg);
            -webkit-animation-delay: -.747s;
            -moz-transform: rotate(60deg);
            -moz-animation-delay: -.747s;
            }
            #coloumn4 {
            -webkit-transform: rotate(90deg);
            -webkit-animation-delay: -.664s;
            -moz-transform: rotate(90deg);
            -moz-animation-delay: -.664s;
            }
            #coloumn5 {
            -webkit-transform: rotate(120deg);
            -webkit-animation-delay: -.581s;
            -moz-transform: rotate(120deg);
            -moz-animation-delay: -.581s;
            }
            #coloumn6 {
            -webkit-transform: rotate(150deg);
            -webkit-animation-delay: -.498s;
            -moz-transform: rotate(150deg);
            -moz-animation-delay: -.498s;
            }
            #coloumn7 {
            -webkit-transform: rotate(180deg);
            -webkit-animation-delay: -.415s;
            -moz-transform: rotate(180deg);
            -moz-animation-delay: -.415s;
            }
            #coloumn8 {
            -webkit-transform: rotate(210deg);
            -webkit-animation-delay: -.332s;
            -moz-transform: rotate(210deg);
            -moz-animation-delay: -.332s;
            }
            #coloumn9 {
            -webkit-transform: rotate(240deg);
            -webkit-animation-delay: -.249s;
            -moz-transform: rotate(240deg);
            -moz-animation-delay: -.249s;
            }
            #coloumn10 {
            -webkit-transform: rotate(270deg);
            -webkit-animation-delay: -.166s;
            -moz-transform: rotate(270deg);
            -moz-animation-delay: -.166s;
            }
            #coloumn11 {
            -webkit-transform: rotate(300deg);
            -webkit-animation-delay: -.083s;
            -moz-transform: rotate(300deg);
            -moz-animation-delay: -.083s;
            }
            #coloumn12 {
            -webkit-transform: rotate(330deg);
            -moz-transform: rotate(330deg);
            }
            @-webkit-keyframes loader {
            0% {
            opacity: 1;
            }
            100% {
            opacity: .25;
            }
            }
            @-moz-keyframes loader {
            0% {
            opacity: 1;
            }
            100% {
            opacity: .25;
            }
            }
         </style>
         <div id='imageZone'>
            <div id='coloumn1' class='coloumns'></div>
            <div id='coloumn2' class='coloumns'></div>
            <div id='coloumn3' class='coloumns'></div>
            <div id='coloumn4' class='coloumns'></div>
            <div id='coloumn5' class='coloumns'></div>
            <div id='coloumn6' class='coloumns'></div>
            <div id='coloumn7' class='coloumns'></div>
            <div id='coloumn8' class='coloumns'></div>
            <div id='coloumn9' class='coloumns'></div>
            <div id='coloumn10' class='coloumns'></div>
            <div id='coloumn11' class='coloumns'></div>
            <div id='coloumn12' class='coloumns'></div>
         </div>
      </div>
      <!-- ========= Site Content ========= -->
      <div id="dm" class='dmwr'>
         <div class="dm_wrapper standard-var5 widgetStyle-3 standard">
            <div dmwrapped="true" id="1554514795" class="dm-home-page" themewaschanged="true">
               <div dmtemplateid="skinnyHeaderLayout" class="skinnyHeaderLayout dm-bfs dm-layout-home inMiniHeaderMode hasStickyHeader dmPageBody d-page-1734971351 dmFreeHeader" id="dm-outer-wrapper" data-page-class="1734971351" data-buttonstyle="FLAT" data-background-size="cover" data-background-attachment="fixed" data-background-repeat="no-repeat" data-background-position="50% 0%" data-background-image="url(https://dd-cdn.multiscreensite.com/themes/sky-patrol/sky-patrol-hp-main-img.jpg)" data-background-hide-inner="true" data-background-fullbleed="false" data-background-parallax-selector=".dmHomeSection1, .dmSectionParallex" data-background-parallax-speed="0.2">
                  <div id="dmStyle_outerContainer" class="dmOuter">
                     <div id="dmStyle_innerContainer" class="dmInner">
                        <div class="dmLayoutWrapper standard-var dmStandardDesktop">
                           <div class="dmHeaderContainer fHeader d-header-wrapper">
                              <div id="hcontainer" class="u_hcontainer dmHeader p_hfcontainer" freeheader="true" headerlayout="722" layout="f014fc9487554eb885e18628ee6309e9===header" data-scrollable-target="body" data-scrollable-target-threshold="1" data-scroll-responder-id="1" logo-size-target="100%">
                                 <div dm:templateorder="76" class="dmHeaderResp noSwitch" id="1261229135">
                                    <div class="dmRespRow fullBleedChanged fullBleedMode" style="text-align: center;" id="1975164890">
                                       <div class="dmRespColsWrapper" id="1504348909">
                                          <div class="u_1169575068 dmRespCol small-12 large-5 medium-5 mobile_app" id="1169575068">
                                             <div class="u_1193814124 imageWidget align-center" editablewidget="true" data-widget-type="image" id="1193814124" data-element-type="image"> 
                                              <?php the_custom_logo();?>
                                             </div>
											 <div class="mobile_menu_click"><i class="fa fa-bars" aria-hidden="true"></i></div>
                                          </div>
                                          <div class="u_1723742866 dmRespCol small-12 large-5 medium-5" id="1723742866">
                                             <div class="u_1941317625 dmNewParagraph" data-dmtmpl="true" id="1941317625" data-uialign="right" style="transition: none 0s ease 0s; top: 0px; padding: 2px 0px; text-align: right; display: block; margin: 0px 0px 0px auto; float: none; width: 395px; height: auto; position: relative;" data-editor-state="closed">
                                              <?php if( get_theme_mod( 'contact_text') != "" ):?>
                                                <div style="text-align: right;"><font style="color: rgb(181, 49, 44);" class="font-26 lh-1"><a href="tel:<?php echo get_theme_mod( 'contact_no'); ?>" runtime_url="tel:(561) 731-3155" style="color: rgb(181, 49, 44); text-decoration: none;"><?php echo get_theme_mod( 'contact_text'); ?> <?php echo get_theme_mod( 'contact_no'); ?></a></font></div>
                                                <?php endif; ?>
                                             </div>
                                             <div class="dmNewParagraph u_1985505645" data-dmtmpl="true" data-element-type="paragraph" id="1985505645" style="transition: opacity 1s ease-in-out 0s;" data-uialign="right"><font style="color: rgb(0, 0, 0);"><?php echo get_theme_mod( 'header_text'); ?></font></div>
                                             <nav class="u_1179213709 effect-text-color main-navigation unifiednav dmLinksMenu" role="navigation" layout-main="horizontal_nav_layout_2" layout-sub="submenu_horizontal_1" data-show-vertical-sub-items="HOVER" id="1179213709" dmle_extension="onelinksmenu" data-element-type="onelinksmenu" wr="true" icon="true" surround="true" adwords="" data-from-nav="true" data-sub-nav="true" data-nav-structure="HORIZONTAL" data-items="-1" custom-li-class="" custom-ul-class="" custom-a-class="" custom-inner-ul-class="" custom-nested-inner-ul-class="" custom-span-class="" navigation-id="unifiedNav" data-links-level="0" is-build-your-own="false">
                                              <?php
                                              wp_nav_menu( array(
                                                'menu'        => 'Home',
                                                'theme_location' => 'primary',
                                               'container'=> false,
                                                'menu_class'      => ' unifiednav__container',
                                                ) );
                                            ?>
                                                <!-- <ul class="unifiednav__container  " data-auto="navigation-pages">
                                                   <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="index.html" class="unifiednav__item  dmNavItemSelected  dmUDNavigationItem_00  " target="" data-target-page-alias="" data-auto="selected-page"> <span class="nav-item-text " data-link-text="
                                                      Home
                                                      " data-auto="page-text-style">Home<span class="icon icon-angle-down"></span> 
                                                      </span> 
                                                      </a> 
                                                   </li>
                                                   <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="about.html" class="unifiednav__item  dmUDNavigationItem_02  " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                      About Us
                                                      " data-auto="page-text-style">About Us<span class="icon icon-angle-down"></span> 
                                                      </span> 
                                                      </a> 
                                                   </li>
                                                   <li class=" unifiednav__item-wrap " data-auto="more-pages">
                                                      <a href="#" class="unifiednav__item  dmUDNavigationItem_010101244184  unifiednav__item_has-sub-nav" target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                         Services
                                                         " data-auto="page-text-style">Services<span class="icon icon-angle-down"></span> 
                                                      </span> 
                                                      </a> 
                                                      <ul class="unifiednav__container unifiednav__container_sub-nav" data-depth="0" data-auto="sub-pages">
                                                         <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="skilled-nursing.html" class="unifiednav__item  dmUDNavigationItem_010101640865  " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                            Skilled Nursing &amp; Therapy Services at Home
                                                            " data-auto="page-text-style">Skilled Nursing &amp; Therapy Services at Home<span class="icon icon-angle-right"></span> 
                                                            </span> 
                                                            </a> 
                                                         </li>
                                                         <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="physicians-assistance.html" class="unifiednav__item  dmUDNavigationItem_010101522431  " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                            Physicians Recommended Care
                                                            " data-auto="page-text-style">Physicians Recommended Care<span class="icon icon-angle-right"></span> 
                                                            </span> 
                                                            </a> 
                                                         </li>
                                                         <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="private-duty-skilled-nursing-care.html" class="unifiednav__item  dmUDNavigationItem_01010133939  " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                            Private Duty Skilled Nursing Services
                                                            " data-auto="page-text-style">Private Duty Skilled Nursing Services<span class="icon icon-angle-right"></span> 
                                                            </span> 
                                                            </a> 
                                                         </li>
                                                         <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="caregiver-services.html" class="unifiednav__item  dmUDNavigationItem_010101704032  " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                            Private Duty Caregiver Services
                                                            " data-auto="page-text-style">Private Duty Caregiver Services<span class="icon icon-angle-right"></span> 
                                                            </span> 
                                                            </a> 
                                                         </li>
                                                         <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="caregiver-arrangments.html" class="unifiednav__item  dmUDNavigationItem_010101312613  " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                            Caregiver Arrangements
                                                            " data-auto="page-text-style">Caregiver Arrangements<span class="icon icon-angle-right"></span> 
                                                            </span> 
                                                            </a> 
                                                         </li>
                                                      </ul>
                                                   </li>
                                                   <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="testimonials.html" class="unifiednav__item  dmUDNavigationItem_0101011126  " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                      Testimonials
                                                      " data-auto="page-text-style">Testimonials<span class="icon icon-angle-down"></span> 
                                                      </span> 
                                                      </a> 
                                                   </li>
                                                   <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="job-opportunities.html" class="unifiednav__item  dmUDNavigationItem_010101572143  " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                      Job Opportunities
                                                      " data-auto="page-text-style">Job Opportunities<span class="icon icon-angle-down"></span> 
                                                      </span> 
                                                      </a> 
                                                   </li>
                                                   <li class=" unifiednav__item-wrap " data-auto="more-pages"> <a href="contact.html" class="unifiednav__item  dmUDNavigationItem_03  " target="" data-target-page-alias=""> <span class="nav-item-text " data-link-text="
                                                      Contact
                                                      " data-auto="page-text-style">Contact<span class="icon icon-angle-down"></span> 
                                                      </span> 
                                                      </a> 
                                                   </li>
                                                </ul> -->
                                             </nav>
                                          </div>
                                          <div class="u_1864336756 dmRespCol empty-column small-12 medium-2" id="1864336756"></div>
                                       </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                             <div class="stickyHeaderSpacer" id="stickyHeaderSpacer" data-new="true"></div>
                           <div id="desktopBodyBox">
                              <div id="iscrollBody">
                                 <div id="site_content">
                                 </div>
                               </div>
<!-- <div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'veritas' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$veritas_description = get_bloginfo( 'description', 'display' );
			if ( $veritas_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $veritas_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<!--<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'veritas' ); ?></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				)
			);
			?>
		</nav> --><!-- #site-navigation -->
	</header><!-- #masthead -->
