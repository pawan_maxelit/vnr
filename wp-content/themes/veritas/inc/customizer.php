<?php
/**
 * Veritas Theme Customizer
 *
 * @package Veritas
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function veritas_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'veritas_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'veritas_customize_partial_blogdescription',
			)
		);
		$wp_customize->add_section( 
		'veritas_header_options', 
			array(
				'title'       => __( 'Header settings', 'mytheme' ),
				'priority'    => 100,
				'capability'  => 'edit_theme_options',
				'description' => __('Set Header options here.', 'mytheme'), 
			) 
		);
		$wp_customize->add_setting( 'contact_text',
			array(
				'default' => ''
			)
		);   
		$wp_customize->add_control( new WP_Customize_Control( 
			$wp_customize, 
			'contact_text',
			array(
				'label'    => __( 'Contact Text', 'mytheme' ), 
				'section'  => 'veritas_header_options',
				'settings' => 'contact_text',
				'priority' => 10,
				'type'     => 'text'
			) 
		));
		$wp_customize->add_setting( 'contact_no',
			array(
				'default' => ''
			)
		);   
		$wp_customize->add_control( new WP_Customize_Control( 
			$wp_customize, 
			'contact_no',
			array(
				'label'    => __( 'Contact No', 'mytheme' ), 
				'section'  => 'veritas_header_options',
				'settings' => 'contact_no',
				'priority' => 10,
				'type'     => 'text'
			) 
		));
		$wp_customize->add_setting( 'header_text',
			array(
				'default' => ''
			)
		);   
		$wp_customize->add_control( new WP_Customize_Control( 
			$wp_customize, 
			'header_text',
			array(
				'label'    => __( 'Header Text', 'mytheme' ), 
				'section'  => 'veritas_header_options',
				'settings' => 'header_text',
				'priority' => 10,
				'type'     => 'text'
			) 
		));
		$wp_customize->add_section( 
				'veritas_socialicons', 
				array(
					'title'       => __( 'Social Icons', 'mytheme' ),
					'priority'    => 100,
					'capability'  => 'edit_theme_options',
					'description' => __('Add social icon Url.', 'mytheme'), 
				) 
			);
			$wp_customize->add_setting( 'facebook_url',
				array(
					'default' => ''
				)
			);   
			$wp_customize->add_control( new WP_Customize_Control( 
				$wp_customize, 
				'facebook_url',
				array(
					'label'    => __( 'Facebook url', 'mytheme' ), 
					'section'  => 'veritas_socialicons',
					'settings' => 'facebook_url',
					'priority' => 10,
					'type'     => 'text',
					'description' => __('Social Icons', 'mytheme'), 
				) 
			));
			$wp_customize->add_setting( 'twitter_url',
				array(
					'default' => ''
				)
			);   
			$wp_customize->add_control( new WP_Customize_Control( 
				$wp_customize, 
				'twitter_url',
				array(
					'label'    => __( 'Twitter url', 'mytheme' ), 
					'section'  => 'veritas_socialicons',
					'settings' => 'twitter_url',
					'priority' => 10,
					'type'     => 'text',
					'description' => __('Social Icons', 'mytheme'),
				) 
			));
			$wp_customize->add_setting( 'instagram_url',
				array(
					'default' => ''
				)
			);   
			$wp_customize->add_control( new WP_Customize_Control( 
				$wp_customize, 
				'instagram_url',
				array(
					'label'    => __( 'Instagram url', 'mytheme' ), 
					'section'  => 'veritas_socialicons',
					'settings' => 'instagram_url',
					'priority' => 10,
					'type'     => 'text',
					'description' => __('Social Icons', 'mytheme'),
				) 
			));
			$wp_customize->add_section( 
				'veritas_footer_copyright_options', 
					array(
						'title'       => __( 'Footer Copyright Section', 'mytheme' ),
						'priority'    => 100,
						'capability'  => 'edit_theme_options',
						'description' => __('Set footer options here.', 'mytheme'), 
					) 
				);
				$wp_customize->add_setting( 'copyright_text',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'copyright_text',
					array(
						'label'    => __( 'Copyright Text', 'mytheme' ), 
						'section'  => 'veritas_footer_copyright_options',
						'settings' => 'copyright_text',
						'priority' => 10,
						'type'     => 'text'
					) 
				));
				$wp_customize->add_setting( 'private_policy_link',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'private_policy_link',
					array(
						'label'    => __( 'Private Policy link', 'mytheme' ), 
						'section'  => 'veritas_footer_copyright_options',
						'settings' => 'private_policy_link',
						'priority' => 10,
						'type'     => 'text'
					) 
				));
				$wp_customize->add_section( 
				'veritas_footer_sections', 
					array(
						'title'       => __( 'Footer Section', 'mytheme' ),
						'priority'    => 100,
						'capability'  => 'edit_theme_options',
						'description' => __('Footer Sections options here.', 'mytheme'), 
					) 
				);
				$wp_customize->add_setting( 'footer_section_text1',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'footer_section_text1',
					array(
						'label'    => __( 'Footer Section Text1', 'mytheme' ), 
						'section'  => 'veritas_footer_sections',
						'settings' => 'footer_section_text1',
						'priority' => 10,
						'type'     => 'textarea'
					) 
				));
				$wp_customize->add_setting( 'footer_section_text2',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'footer_section_text2',
					array(
						'label'    => __( 'Footer Section Text2', 'mytheme' ), 
						'section'  => 'veritas_footer_sections',
						'settings' => 'footer_section_text2',
						'priority' => 10,
						'type'     => 'textarea'
					) 
				));
				$wp_customize->add_setting( 'footer_section_text3',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'footer_section_text3',
					array(
						'label'    => __( 'Footer Section Text3', 'mytheme' ), 
						'section'  => 'veritas_footer_sections',
						'settings' => 'footer_section_text3',
						'priority' => 10,
						'type'     => 'textarea'
					) 
				));
				$wp_customize->add_section( 
				'veritas_contact_sections', 
					array(
						'title'       => __( 'Contact Sections', 'mytheme' ),
						'priority'    => 100,
						'capability'  => 'edit_theme_options',
						'description' => __('Contact details options here.', 'mytheme'), 
					) 
				);
				$wp_customize->add_setting( 'contact_details',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'contact_details',
					array(
						'label'    => __( 'Contact Details', 'mytheme' ), 
						'section'  => 'veritas_contact_sections',
						'settings' => 'contact_details',
						'priority' => 10,
						'type'     => 'textarea'
					) 
				));
				$wp_customize->add_setting( 'contact_phone_no',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'contact_phone_no',
					array(
						'label'    => __( 'Contact Phone No', 'mytheme' ), 
						'section'  => 'veritas_contact_sections',
						'settings' => 'contact_phone_no',
						'priority' => 10,
						'type'     => 'text'
					) 
				));
				$wp_customize->add_setting( 'contact_email',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'contact_email',
					array(
						'label'    => __( 'Contact Email', 'mytheme' ), 
						'section'  => 'veritas_contact_sections',
						'settings' => 'contact_email',
						'priority' => 10,
						'type'     => 'text'
					) 
				));
				$wp_customize->add_setting( 'office_hours',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'office_hours',
					array(
						'label'    => __( 'Office Hours', 'mytheme' ), 
						'section'  => 'veritas_contact_sections',
						'settings' => 'office_hours',
						'priority' => 10,
						'type'     => 'textarea'
					) 
				));
				$wp_customize->add_setting( 'contact_address1',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'contact_address1',
					array(
						'label'    => __( 'Contact Address1', 'mytheme' ), 
						'section'  => 'veritas_contact_sections',
						'settings' => 'contact_address1',
						'priority' => 10,
						'type'     => 'textarea'
					) 
				));
				$wp_customize->add_setting( 'contact_address2',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'contact_address2',
					array(
						'label'    => __( 'Contact Address2', 'mytheme' ), 
						'section'  => 'veritas_contact_sections',
						'settings' => 'contact_address2',
						'priority' => 10,
						'type'     => 'textarea'
					) 
				));
				$wp_customize->add_setting( 'hr_email',
					array(
						'default' => ''
					)
				);   
				$wp_customize->add_control( new WP_Customize_Control( 
					$wp_customize, 
					'hr_email',
					array(
						'label'    => __( 'HR Email', 'mytheme' ), 
						'section'  => 'veritas_contact_sections',
						'settings' => 'hr_email',
						'priority' => 10,
						'type'     => 'textarea'
					) 
				));
	}
}
add_action( 'customize_register', 'veritas_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function veritas_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function veritas_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function veritas_customize_preview_js() {
	wp_enqueue_script( 'veritas-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'veritas_customize_preview_js' );
